package in.innasoft.gofarmz.utilities;

public class AppUrls {

//    public static String BASE_URL = "http://goformz-new.learningslot.in/";
//    public static String IMAGE_URL = "http://goformz-new.learningslot.in/images/products/280x280/";
//    public static String PRODUCTS_IMAGE_URL = "http://goformz-new.learningslot.in/images/products/400x400/";
//    public static String SLIDER_IMAGE_URL = "http://goformz-new.learningslot.in/images/banners/";
//    public static String WISHLIST_IMAGE_URL = "http://goformz-new.learningslot.in/images/products/70x70/";
//    public static String GATEWAY_IMAGE_LOGO = "http://goformz-new.learningslot.in/images/gateway/";

//    public static String BASE_URL = "http://192.168.1.161:8088/gofarmz.com/";
    public static String BASE_URL = "https://www.gofarmz.com/";
    public static String IMAGE_URL = "https://www.gofarmz.com/images/products/280x280/";
    public static String PRODUCTS_IMAGE_URL = "https://www.gofarmz.com/images/products/400x400/";
    public static String SLIDER_IMAGE_URL = "https://www.gofarmz.com/images/banners/";
    public static String WISHLIST_IMAGE_URL = "https://www.gofarmz.com/images/products/70x70/";
    public static String GATEWAY_IMAGE_LOGO = "https://www.gofarmz.com/images/gateway/";

    public static String REGISTRATION = "registration";
    public static String LOGIN = "login_app";
    public static String ACCOUNT_VERIFICATION = "otp_verification";
    public static String RESEND_OTP = "otp_resend";
    public static String USER_PROFILE = "getUserProfile";
    public static String UPDATE_PROFILE = "editUserProfile";
    public static String LOGOUT = "logout_app";
    public static String FORGOT_PASSWORD = "forgot_password";
    public static String RESET_PASSWORD = "otp_verification_forgot_password";
    public static String CHANGE_PASSWORD = "change_password";
    public static String COUNTRY_LIST = "getCountry";
    public static String STATE_LIST = "getState";
    public static String CITY_LIST = "getCity";
    public static String AREA_LIST = "getArea";
    public static String LOCATION = "getLocation";
    public static String GET_WISHLIST = "getWishlist";
    public static String DELETE_WISHLIST = "deleteWishlist";
    public static String GET_ORDER_HISTORY = "getOrderHistory";
    public static String GET_ORDER_CANCEL = "getOrderCancel";
    public static String GET_ORDER_VIEW = "getOrderFullDetails";
    public static String GET_ORDER_TRACK = "getOrderTrack";
    public static String GET_MAIN_CATEGORY = "getMainCategory";
    public static String GET_MAIN_CATEGORY_PRODUCT = "getMainCategoryProducts";
    public static String GET_MAIN_SUB_CATEGORY_PRODUCT = "getSubCategoryProducts";
    public static String GET_MAIN_CHILD_CATEGORY_PRODUCT = "getChildCategoryProducts";
    public static String GET_NEW_PRODUCTLIST = "getNewProducts";
    public static String GET_MOST_SELLING_PRODUCTLIST = "getMostSellingProducts";
    public static String GET_POPULAR_PRODUCTS = "getPopularProducts";

    public static String GET_PRODUCT_DETAIL_VIEW = "getProductFullDetails";
    public static String ADD_FAVOURATE = "addFavourite";
    public static String REMOVE_FAVOURATE = "removeFavourite";
    public static String GET_RELATED_PRODUCT_LIST = "getRelatedProducts";
    public static String GET_TESTIMONIALS = "getTestimonials";
    public static String GET_BLOGS_LIST = "getBlogs";
    public static String GET_BLOGS_VIEW = "getBlogView";
    public static String ADD_CART_DATA = "addCartData";
    public static String DELETE_CART_DATA = "deleteCartData";
    public static String UPDATE_CART_DATA = "updateCartData";
    public static String EMPTY_CART_DATA = "emptyCartData";
    public static String GET_CART_DATA = "getCartData";
    public static String GET_BANNER = "getBanner";
    public static String GET_RELATED_WEIGHTS = "getRelatedWeights";
    public static String GET_REFINE_FILTER_PRODUCTS = "getRefineFilterProducts";
    public static String GET_CART_COUNT = "getCartDataCount";
    public static String GET_SUBCATEGORY = "getSubCategory";
    public static String GET_CHILD_CATEGORY = "getChildCategory";
    public static String GET_PRODUCT_WISHLIST = "getProductWishList";
    public static String ADD_CHECKOUT_DATA = "addCheckoutData";
    public static String POST_PRODUCT_REVIEW = "postProductReview";
    public static String USER_PRODUCT_REVIEW = "userProductReview";

    public static String PRODUCT_REVIEWS = "productReviews";

    public static String SHIPPING_PRODUCT = "getCheckoutData";
    public static String ADD_PAYMENT_DATA = "addPaymentData";
    public static String UPDATE_PAYMENT_DATA = "updatePaymentData";
    public static String PROCEED_CHECKOUT = "proceedCheckout";
    public static String PRODUCT_RECEIPE = "getProductRecipes";
    public static String PRODUCT_RECEIPE_SINGLEVIEW = "getProductRecipeSingleView";
    public static String PINCODES = "pincodes";


    public static String SENDING_PAYMENT_TO_SERVER = "order";


}

