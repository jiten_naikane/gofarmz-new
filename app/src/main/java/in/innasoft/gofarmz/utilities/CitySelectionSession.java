package in.innasoft.gofarmz.utilities;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by Devolper on 24-Jul-17.
 */



public class CitySelectionSession
{
    SharedPreferences pref,preferences;
    SharedPreferences.Editor editor,editor2;
    Context _context;
    int PRIVATE_MODE = 0;

    private static final String PREFER_NAME = "sricc_city";

    private static final String IS_CITY_CREATED = "is_city_created";
    public static final String CITY_ID = "city_id";
    public static final String CITY_NAME = "city_name";
    public static final String CITY_LAT = "city_lat";
    public static final String CITY_LNG = "city_lan";


    public CitySelectionSession(Context context){
        this._context = context;


        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createCity(String city_id, String city_name){

        editor.putBoolean(IS_CITY_CREATED, true);

        editor.putString(CITY_ID, city_id);
        editor.putString(CITY_NAME, city_name);
       // editor.putString(CITY_LAT, city_lat);
     //   editor.putString(CITY_LNG, city_lng);
        editor.commit();
    }

    public void clearCity(){
        editor.clear();
        editor.commit();

    }





    public boolean checkCityCreated(){

        if(!this.isCityGet()){


            return true;
        }
        return false;
    }

    public void updateProfileInfo(String id, String name, String lat, String lng){
        editor.putString(CITY_ID, id);
        editor.putString(CITY_NAME, name);
     //   editor.putString(CITY_LAT, lat);
    //    editor.putString(CITY_LNG, lng);

        editor.commit();
    }


    public HashMap<String, String> getCityDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(CITY_ID, pref.getString(CITY_ID, null));
        user.put(CITY_NAME, pref.getString(CITY_NAME, "0"));
      //  user.put(CITY_LAT, pref.getString(CITY_LAT, null));
     //   user.put(CITY_LNG, pref.getString(CITY_LNG, null));

        return user;
    }






    public boolean isCityGet(){
        return pref.getBoolean(IS_CITY_CREATED, false);
    }



}
