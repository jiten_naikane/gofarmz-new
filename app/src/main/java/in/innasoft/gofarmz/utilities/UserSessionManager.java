package in.innasoft.gofarmz.utilities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.activities.GetStartedActivity;


public class UserSessionManager {

    SharedPreferences pref,preferences;
    Editor editor,editor2;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREFER_NAME = "wellcarehealthcard";
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";


    public static final String KEY_ACCSES = "access_key";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_MOBILE = "user_mobile";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_ACCOUNT_STATUS = "account_status";
    public static final String PROFILE_PIC_URL = "user_profile_pic_url";
    public static final String INTRO_SLIDE = "intorslide";
    public static final String ISFIRTTIMEAPP = "isfirsttime";
    public static final String USER_REG_MOBILE= "user_reg_mobile";
    public static final String BROWSER_SESSION_ID= "browser_session_id";

    public UserSessionManager(Context context){
        this._context = context;
        preferences = context.getSharedPreferences(INTRO_SLIDE, PRIVATE_MODE);
        editor2 = preferences.edit();

        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createUserLoginSession(String key, String userId, String userName, String mobile, String email,String account_status,String browser_session_id)
    {

        editor.putBoolean(IS_USER_LOGIN, true);
        editor.putString(KEY_ACCSES, key);
        editor.putString(USER_ID,userId);
        editor.putString(USER_NAME, userName);
        editor.putString(USER_MOBILE, mobile);
        editor.putString(USER_EMAIL, email);
        editor.putString(USER_ACCOUNT_STATUS, account_status);
        editor.putString(BROWSER_SESSION_ID, browser_session_id);
      //  editor.putString(PROFILE_PIC_URL, profile_pic_url);
        editor.commit();
    }
    public void getUserMobileSession(String mobile){

        editor.putBoolean(IS_USER_LOGIN, true);
        editor.putString(USER_MOBILE, mobile);


        editor.commit();
    }
    public void createIsFirstTimeAppLunch()
    {
        editor2.putBoolean(ISFIRTTIMEAPP, true);
        editor2.commit();
    }

    public boolean checkIsFirstTime(){

        if(!this.isFirstTimeAppLunch()){

            Intent i = new Intent(_context, GetStartedActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);

            return true;
        }
        return false;
    }

    public void usermobileSession(String mobile)
    {
        editor.putString(USER_REG_MOBILE, mobile);

        editor.commit();
    }

    public boolean checkLogin(){

        if(!this.isUserLoggedIn()){

            Intent i = new Intent(_context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
            return true;
        }
        return false;
    }

    public HashMap<String, String> getUserDetails()
    {

        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_ACCSES, pref.getString(KEY_ACCSES, null));
        user.put(USER_ID, pref.getString(USER_ID, null));
        user.put(USER_NAME, pref.getString(USER_NAME, null));
        user.put(USER_MOBILE, pref.getString(USER_MOBILE, null));
        user.put(USER_EMAIL, pref.getString(USER_EMAIL, null));
        user.put(USER_ACCOUNT_STATUS, pref.getString(USER_ACCOUNT_STATUS, null));
        user.put(USER_REG_MOBILE, pref.getString(USER_REG_MOBILE, null));
        user.put(BROWSER_SESSION_ID, pref.getString(BROWSER_SESSION_ID, null));

       // user.put(PROFILE_PIC_URL, pref.getString(PROFILE_PIC_URL, null));
        return user;
    }

    public void logoutUser(){
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

    public boolean isUserLoggedIn(){
        return pref.getBoolean(IS_USER_LOGIN, false);
    }

    public boolean isFirstTimeAppLunch(){
        return preferences.getBoolean(ISFIRTTIMEAPP, false);
    }
}