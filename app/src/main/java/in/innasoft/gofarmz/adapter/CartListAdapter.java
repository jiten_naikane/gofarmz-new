package in.innasoft.gofarmz.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.CartProductListActivity;
import in.innasoft.gofarmz.dbhelper.LocationControlar;
import in.innasoft.gofarmz.holder.CartListHolder;
import in.innasoft.gofarmz.itemclicklistners.CartListItemClickListener;
import in.innasoft.gofarmz.models.CartListModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;


public class CartListAdapter extends RecyclerView.Adapter<CartListHolder> {

    private ArrayList<CartListModel> cartList;
    CartProductListActivity context;
    LayoutInflater li;
    int resource;
    int sum = 0;
    public static boolean add = true;
    String user_id, jwt_token, device_id;
    private boolean checkInternet;
    UserSessionManager session;
    ProgressDialog progressDialog;
    LocationControlar db;
    Typeface typeface, typeface2;


    public CartListAdapter(ArrayList<CartListModel> cartList, CartProductListActivity context, int resource) {
        this.cartList = cartList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_two));
        db = new LocationControlar(context);
        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        jwt_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("WhishLISt", user_id + "//" + jwt_token);
        device_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("CARTDETLETDEVICEID:", device_id);
        if (jwt_token == null || jwt_token.equals("")) {
            user_id = "0";

        } else {
            user_id = userDetails.get(UserSessionManager.USER_ID);
        }

    }

    @Override
    public int getItemCount() {
        return this.cartList.size();
    }


    @Override
    public CartListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutSeedCategory = li.inflate(resource, parent, false);
        CartListHolder sch = new CartListHolder(layoutSeedCategory);
        return sch;
    }

    @Override
    public void onBindViewHolder(final CartListHolder holder, final int position) {

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        // db.getCartProductId();
        String str = cartList.get(position).getProduct_name();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.cart_product_name.setText(converted_string);
        holder.cart_product_name.setTypeface(typeface2);

        holder.cart_product_skuid.setText(cartList.get(position).getProduct_sku_id());
        holder.cart_product_skuid.setTypeface(typeface);

        holder.cart_product_count.setText(cartList.get(position).getProduct_count());
        holder.cart_product_count.setTypeface(typeface);


        holder.cart_product_prize.setText("MRP : Rs." + cartList.get(position).getProduct_mrp_price() + " / " + cartList.get(position).getProduct_weight());
        holder.cart_product_prize.setTypeface(typeface);

        holder.cart_product_qty.setText("  " + cartList.get(position).getPurchase_quantity());
        holder.cart_product_qty.setTypeface(typeface);

        int qty = Integer.parseInt(cartList.get(position).getProduct_quantity());

        holder.total_item.setText("Total : \u20B9" + cartList.get(position).getTotal_price() + " /-");
        holder.total_item.setTypeface(typeface);

        holder.discount_price.setText("Discount : \u20B9"+ cartList.get(position).getDiscount() + " /-");
        holder.discount_price.setTypeface(typeface);

        //  double price = qty* Double.parseDouble(cartList.get(position).getProduct_mrp_price());
        // holder.total_item.setText("Total Amount : Rs." + price + " /-");


        holder.qty_minus_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(holder.cart_product_qty.getText().toString().trim());

                if (qty > 1) {
                    qty--;
                   /* qty--;
                    double finalPrice = qty* Double.parseDouble(cartList.get(position).getProduct_mrp_price());
                    String result = new DecimalFormat("##.##").format(finalPrice);
                    db.updateCart(cartList.get(position).getProduct_id(), String.valueOf(qty), result);
                   // context.refreshCartPage();*/


                    final String item_remove_id = cartList.get(position).getId();
                    final String item_product_id = cartList.get(position).getProduct_id();
                    final String item_product_quantity = String.valueOf(qty--);
                    if (jwt_token == null || jwt_token.equals("")) {
                        user_id = "0";
                        getUpdateQuantity(item_remove_id, item_product_id, item_product_quantity, user_id);
                    } else {
                        getUpdateQuantity(item_remove_id, item_product_id, item_product_quantity, user_id);
                    }
                } else {
                    Toast.makeText(context, "Minimum Quantity Required...!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.qty_add_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(holder.cart_product_qty.getText().toString().trim());

                if (qty != 0) {
                    qty++;
                  /*  double finalPrice = qty* Double.parseDouble(cartList.get(position).getProduct_mrp_price());
                    String result = new DecimalFormat("##.##").format(finalPrice);
                    db.updateCart(cartList.get(position).getProduct_id(), String.valueOf(qty), result);*/
                    final String item_remove_id = cartList.get(position).getId();
                    final String item_product_id = cartList.get(position).getProduct_id();
                    final String item_product_quantity = String.valueOf(qty++);
                    if (jwt_token == null || jwt_token.equals("")) {
                        user_id = "0";
                        getUpdateQuantity(item_remove_id, item_product_id, item_product_quantity, user_id);
                    } else {
                        getUpdateQuantity(item_remove_id, item_product_id, item_product_quantity, user_id);
                    }

                    // context.refreshCartPage();

                } else {
                    Toast.makeText(context, "Minimum Quantity Required...!", Toast.LENGTH_SHORT).show();
                }
            }
        });


       /* holder.cart_product_qty.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                final String[] listValue;
                listValue = context.getResources().getStringArray(R.array.qunatity_list);
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                LayoutInflater inflater = context.getLayoutInflater();
                View convertView = (View) inflater.inflate(R.layout.custom_quantity_layout, null);
                alertDialog.setCancelable(false);
                alertDialog.setView(convertView);
                //alertDialog.setTitle("Blood Group");
                final TextView quant_txt = (TextView) convertView.findViewById(R.id.quant_txt);
                final ListView lv_quant = (ListView) convertView.findViewById(R.id.lv_quant);
                final Button btn_quant_cancel = (Button) convertView.findViewById(R.id.btn_quant_cancel);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, listValue);
                lv_quant.setAdapter(adapter);
                final AlertDialog dialog = alertDialog.show();
                lv_quant.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                        holder.cart_product_qty.setText("Qantity  : "+listValue[arg2]);
                        final String  item_remove_id= cartList.get(position).getId();
                        final String  item_product_id= cartList.get(position).getProduct_id();
                        final String  item_product_quantity= listValue[arg2];

                        getUpdateQuantity(item_remove_id,item_product_id,item_product_quantity);




                        dialog.dismiss();

                    }
                });

                btn_quant_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view)
                    {
                        dialog.dismiss();
                    }
                });

            }
        });*/


        Picasso.with(context)
                .load(cartList.get(position).getImages())
                .placeholder(R.drawable.nodata_image)
                .into(holder.cart_product_img);


        holder.setItemClickListener(new CartListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {


            }
        });

        holder.remove_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // page_no="1";
                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                builder.setTitle("Alert");
                builder.setMessage("Are you sure?");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        /*if(db.deleteProduct(cartList.get(position).getProduct_id())){

                           // context.refreshCartPage();
                            Toast.makeText(context, "Successfully Removed...!", Toast.LENGTH_SHORT).show();

                        }else {
                            dialog.dismiss();
                        }*/


                        {
                            checkInternet = NetworkChecking.isConnected(context);
                            if (checkInternet) {
                                Log.d("CARTLISDELTTURL", AppUrls.BASE_URL + AppUrls.DELETE_CART_DATA);
                                final String item_remove_id = cartList.get(position).getId();
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.DELETE_CART_DATA,
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                progressDialog.dismiss();

                                                try {
                                                    JSONObject jsonObject = new JSONObject(response);
                                                    Log.d("CARTLISTDELTRESP", response);
                                                    String responceCode = jsonObject.getString("status");

                                                    if (responceCode.equalsIgnoreCase("10100")) {
                                                        Toast.makeText(context, "Successfully Deleted", Toast.LENGTH_SHORT).show();
                                                        progressDialog.dismiss();
                                                        //   db.deleteProduct(cartList.get(position).getProduct_id());
                                                       /* Intent intent = new Intent(context,CartProductListActivity.class);
                                                        context.startActivity(intent);
                                                        context.finish();
*/
                                                        ((CartProductListActivity) context).getCartListData(user_id);
                                                        //  ((WishListActivity)context).wishList(1);
                                                        //   ((WishListActivity) context).functionToRun();


                                                    }
                                                    if (responceCode.equals("12786")) {
                                                        Toast.makeText(context, "Sorry Try Again.", Toast.LENGTH_SHORT).show();
                                                    }
                                                    if (responceCode.equals("11786")) {
                                                        Toast.makeText(context, " All fields are required", Toast.LENGTH_SHORT).show();
                                                    }


                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                progressDialog.dismiss();

                                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                } else if (error instanceof AuthFailureError) {

                                                } else if (error instanceof ServerError) {

                                                } else if (error instanceof NetworkError) {

                                                } else if (error instanceof ParseError) {

                                                }
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("cartId", item_remove_id);
                                        params.put("user_id", user_id);
                                        params.put("browserId", device_id);
                                        Log.d("CARTPARAM:", params.toString());
                                        return params;
                                    }

                                    @Override
                                    public Map<String, String> getHeaders() throws AuthFailureError {
                                        Map<String, String> headers = new HashMap<>();

                                        //  headers.put("Content-Type", "application/json");
                                        headers.put("Authorization-Basic", jwt_token);
                                        Log.d("CARTListHEADER", headers.toString());
                                        return headers;
                                    }

                                };
                                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                RequestQueue requestQueue = Volley.newRequestQueue(context);
                                requestQueue.add(stringRequest);

                            } else {
                                progressDialog.cancel();
                                Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();


            }
        });
    }

    private void getUpdateQuantity(final String item_remove_id, final String item_product_id, final String item_product_quantity, final String user_id) {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            Log.d("UPDATECARTLISDELTTURL", AppUrls.BASE_URL + AppUrls.UPDATE_CART_DATA);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.UPDATE_CART_DATA,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("UPDATECARTLISTDELTRESP", response);
                                String responceCode = jsonObject.getString("status");

                                if (responceCode.equalsIgnoreCase("10100")) {
                                    //Toast.makeText(context, "Updated to cart successfully", Toast.LENGTH_SHORT).show();
                                    progressDialog.dismiss();

                                    ((CartProductListActivity) context).getCartListData(user_id);

                                }
                                if (responceCode.equals("10200")) {
                                    Toast.makeText(context, "Sorry Try Again.", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    Toast.makeText(context, " All fields are required", Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("productId", item_product_id);
                    params.put("browserId", device_id);
                    params.put("productQuantity", item_product_quantity);
                    params.put("cartId", item_remove_id);

                    Log.d("UPDATECARTPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    //  headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", jwt_token);
                    Log.d("UPDATECARTListHEADER", headers.toString());
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);

        } else {
            progressDialog.cancel();
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


}
