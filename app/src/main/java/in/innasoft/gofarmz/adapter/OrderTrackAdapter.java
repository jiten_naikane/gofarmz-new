package in.innasoft.gofarmz.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.OrderViewActivity;
import in.innasoft.gofarmz.holder.OrderTrackHolder;
import in.innasoft.gofarmz.itemclicklistners.OrderViewItemClickListener;
import in.innasoft.gofarmz.models.OrderTrackingModel;
import in.innasoft.gofarmz.utilities.UserSessionManager;

public class OrderTrackAdapter extends RecyclerView.Adapter<OrderTrackHolder> {

    public ArrayList<OrderTrackingModel> orderViewModels;
    public OrderViewActivity context;
    LayoutInflater li;
    int resource;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String user_id, token;
    UserSessionManager session;
    Typeface typeface;

    public OrderTrackAdapter(ArrayList<OrderTrackingModel> orderViewModels, OrderViewActivity context, int resource) {
        this.orderViewModels = orderViewModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);

        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
    }

    @Override
    public OrderTrackHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        OrderTrackHolder slh = new OrderTrackHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(OrderTrackHolder holder, final int position) {


        holder.order_status.setText(orderViewModels.get(position).getOrder_status());
        holder.order_status.setTypeface(typeface);
        if (orderViewModels.get(position).getOrder_status().equals("Cancelled")) {
            holder.order_status.setTextColor(Color.RED);
        } else if (orderViewModels.get(position).getOrder_status().equals("Pending")) {
            holder.order_status.setTextColor(Color.parseColor("#F1C40F"));
        } else {
            holder.order_status.setTextColor(Color.parseColor("#27AE60"));
        }

        holder.order_message.setText(orderViewModels.get(position).getMessage());
        holder.order_message.setTypeface(typeface);

        holder.order_date_and_time.setText(orderViewModels.get(position).getCreate_date_time());
        holder.order_date_and_time.setTypeface(typeface);

        holder.setItemClickListener(new OrderViewItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.orderViewModels.size();
    }
}
