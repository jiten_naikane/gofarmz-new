package in.innasoft.gofarmz.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.LocationDialogActivity;
import in.innasoft.gofarmz.filters.HomeFilterForCityList;
import in.innasoft.gofarmz.holder.HomeCityHolder;
import in.innasoft.gofarmz.itemclicklistners.HomeCityItemClickListener;
import in.innasoft.gofarmz.models.CityModel;
import in.innasoft.gofarmz.utilities.CitySelectionSession;


public class HomeCityAdapter extends RecyclerView.Adapter<HomeCityHolder> implements Filterable {

    public ArrayList<CityModel> homecityModels,filterList;
    public LocationDialogActivity context;
    HomeFilterForCityList filter;
    LayoutInflater li;
    int resource;
    Typeface typeface;
    CitySelectionSession citySelectionSession;
    public String disp_city_name = "", disp_city_id, disp_lat, disp_lng;

    public HomeCityAdapter(ArrayList<CityModel> homecityModels, LocationDialogActivity context, int resource) {
        this.homecityModels = homecityModels;
        this.filterList = homecityModels;
        this.context = context;
        this.resource = resource;
        citySelectionSession = new CitySelectionSession(context);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));


        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public HomeCityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,null);
        HomeCityHolder slh = new HomeCityHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final HomeCityHolder holder, final int position) {



        holder.row_home_city_txt.setTypeface(typeface);
       holder.row_home_city_txt.setText(homecityModels.get(position).getCityName());


        if(citySelectionSession.checkCityCreated() == false){
            HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
            disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
            disp_city_name = cityDetails.get(CitySelectionSession.CITY_NAME);
            disp_lng = cityDetails.get(CitySelectionSession.CITY_LAT);
            disp_lng = cityDetails.get(CitySelectionSession.CITY_LNG);
            try{
                Log.d("FIIINDCITYLENGTH", disp_city_name);
               if(disp_city_id.equals(homecityModels.get(position).getCityId())){
                   holder.city_row_img.setColorFilter(Color.argb(255, 2, 175, 238));
                   holder.row_home_city_txt.setTextColor(Color.argb(255, 2, 175, 238));
                   Picasso.with(context)
                           .load(homecityModels.get(position).getCity_image())
                           .placeholder(R.drawable.ic_city_grey600_24dp)
                           .into(holder.city_row_img);
               }else {
                   Picasso.with(context)
                           .load(homecityModels.get(position).getCity_image())
                           .placeholder(R.drawable.ic_city_grey600_24dp)
                           .into(holder.city_row_img);
               }
            }catch (NullPointerException e){
                String msg = (e.getMessage()==null)?"Login failed!":e.getMessage();
                Log.i("Login Error1",msg);
            }
        }else {
            Picasso.with(context)
                    .load(homecityModels.get(position).getCity_image())
                    .placeholder(R.drawable.ic_city_grey600_24dp)
                    .into(holder.city_row_img);
        }



        holder.setItemClickListener(new HomeCityItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {
                citySelectionSession.createCity(homecityModels.get(pos).getCityId(), homecityModels.get(pos).getCityName());
                Intent intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);
                context.finish();
               //context.setCityName(homecityModels.get(pos).getCityName(),homecityModels.get(pos).getCityId(),homecityModels.get(pos).getLatitute(),homecityModels.get(pos).getLognigtute());

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.homecityModels.size();

    }

    @Override
    public Filter getFilter()
    {
        if(filter==null)
        {
            filter=new HomeFilterForCityList(filterList,this);
        }

        return filter;
    }
}
