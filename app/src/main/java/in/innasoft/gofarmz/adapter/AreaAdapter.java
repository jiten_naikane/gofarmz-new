package in.innasoft.gofarmz.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.UserProfileActivity;
import in.innasoft.gofarmz.filters.CustomFilterForAreaList;
import in.innasoft.gofarmz.holder.AreaHolder;
import in.innasoft.gofarmz.itemclicklistners.AreaItemClickListener;
import in.innasoft.gofarmz.models.AreaModel;

public class AreaAdapter extends RecyclerView.Adapter<AreaHolder>implements Filterable {
    public ArrayList<AreaModel> areaModels,filterList;
    public UserProfileActivity context;
    CustomFilterForAreaList filter;
    LayoutInflater li;
    int resource;
    Typeface typeface;

    public AreaAdapter(ArrayList<AreaModel> areaModels, UserProfileActivity context, int resource) {
        this.areaModels = areaModels;
        this.context = context;
        this.resource = resource;
        this.filterList = areaModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterForAreaList(filterList,this);
        }

        return filter;
    }

    @Override
    public AreaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        AreaHolder slh = new AreaHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(AreaHolder holder, final int position) {

        holder.area_name_txt.setText(areaModels.get(position).getName());
        holder.area_name_txt.setTypeface(typeface);

        holder.setItemClickListener(new AreaItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                context.setAreaName(areaModels.get(pos).getName(), areaModels.get(pos).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.areaModels.size();
    }
}
