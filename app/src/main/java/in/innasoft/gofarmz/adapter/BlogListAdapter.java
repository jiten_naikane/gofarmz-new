package in.innasoft.gofarmz.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.BlogDescriptionActivity;
import in.innasoft.gofarmz.activities.BlogsListActivity;
import in.innasoft.gofarmz.holder.BlogListHolder;
import in.innasoft.gofarmz.itemclicklistners.BlogListItemClickListener;
import in.innasoft.gofarmz.models.BlogListModel;

public class BlogListAdapter extends RecyclerView.Adapter<BlogListHolder>{

    private ArrayList<BlogListModel> blogList;
    BlogsListActivity context;
    LayoutInflater li;
    int resource;
    String blog_id;
    Typeface typeface;

    public BlogListAdapter(ArrayList<BlogListModel> blogList, BlogsListActivity context, int resource) {
        this.blogList = blogList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
    }

    @Override
    public BlogListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutSeedCategory = li.inflate(resource,null);
        BlogListHolder sch = new BlogListHolder(layoutSeedCategory);
        return sch;
    }

    @Override
    public void onBindViewHolder(BlogListHolder holder, final int position) {




        String str = blogList.get(position).getName();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.blog_title_text.setText(converted_string);
        holder.blog_title_text.setTypeface(typeface);



        Picasso.with(context)
                .load(blogList.get(position).getImage())
                .placeholder(R.drawable.nodata_image)
                .into(holder.blog_image_iv);

        holder.setItemClickListener(new BlogListItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {
                blog_id=blogList.get(position).getId();

                Intent intent = new Intent(context,BlogDescriptionActivity.class);
                intent.putExtra("blog_id",blog_id);
                context.startActivity(intent);

            }
        });
    }


    @Override
    public int getItemCount() {
        return this.blogList.size();
    }


}
