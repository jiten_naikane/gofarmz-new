package in.innasoft.gofarmz.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.OrderViewActivity;
import in.innasoft.gofarmz.holder.OrderViewHolder;
import in.innasoft.gofarmz.itemclicklistners.OrderViewItemClickListener;
import in.innasoft.gofarmz.models.OrderViewModel;
import in.innasoft.gofarmz.utilities.UserSessionManager;

public class OrderViewAdapter extends RecyclerView.Adapter<OrderViewHolder>{

    public ArrayList<OrderViewModel> orderViewModels;
    public OrderViewActivity context;
    LayoutInflater li;
    int resource;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String user_id,token;
    UserSessionManager session;
    Typeface typeface,typeface2;

    public OrderViewAdapter(ArrayList<OrderViewModel> orderViewModels, OrderViewActivity context, int resource) {
        this.orderViewModels = orderViewModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_two));
        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);

        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        OrderViewHolder slh = new OrderViewHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(OrderViewHolder holder, final int position) {


        holder.product_name_txt.setText(orderViewModels.get(position).getProduct_name());
        holder.product_name_txt.setTypeface(typeface);

        holder.orderview_product_count.setText(orderViewModels.get(position).getCount_name());
        holder.orderview_product_count.setTypeface(typeface);

        holder.quantity_txt.setText("Quantity : "+orderViewModels.get(position).getPurchase_quantity());
        holder.quantity_txt.setTypeface(typeface);

        holder.total_price_txt.setText("\u20B9"+orderViewModels.get(position).getTotal_price());
        holder.total_price_txt.setTypeface(typeface);


        Picasso.with(context)
                .load(orderViewModels.get(position).getImages())
                .placeholder(R.drawable.nodata_image)
                .into(holder.order_view_product_img);


        holder.setItemClickListener(new OrderViewItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.orderViewModels.size();
    }
}
