package in.innasoft.gofarmz.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.UserProfileActivity;
import in.innasoft.gofarmz.filters.CustomFilterForCountryList;
import in.innasoft.gofarmz.holder.CountryHolder;
import in.innasoft.gofarmz.itemclicklistners.CountryItemClickListener;
import in.innasoft.gofarmz.models.CountriesModel;


public class CountryAdapter extends RecyclerView.Adapter<CountryHolder>implements Filterable {
    public ArrayList<CountriesModel> countryModels,filterList;
    public UserProfileActivity context;
    CustomFilterForCountryList filter;
    LayoutInflater li;
    int resource;
    Typeface typeface;

    public CountryAdapter(ArrayList<CountriesModel> countryModels, UserProfileActivity context, int resource) {
        this.countryModels = countryModels;
        this.context = context;
        this.resource = resource;
        this.filterList = countryModels;
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterForCountryList(filterList,this);
        }

        return filter;
    }

    @Override
    public CountryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        CountryHolder slh = new CountryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(CountryHolder holder, final int position) {

        holder.country_name_txt.setText(countryModels.get(position).getName());
        holder.country_name_txt.setTypeface(typeface);

        holder.setItemClickListener(new CountryItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                context.setCountryName(countryModels.get(pos).getName(),countryModels.get(pos).getId());

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.countryModels.size();
    }
}
