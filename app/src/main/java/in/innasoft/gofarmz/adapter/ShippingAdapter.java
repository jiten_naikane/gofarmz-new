package in.innasoft.gofarmz.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.ShippingProductActivity;
import in.innasoft.gofarmz.holder.ShippingHolder;
import in.innasoft.gofarmz.itemclicklistners.ShippingItemClickListener;
import in.innasoft.gofarmz.models.ShippingModel;

public class ShippingAdapter extends RecyclerView.Adapter<ShippingHolder> {

    public ArrayList<ShippingModel> shippingModels;
    public ShippingProductActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;

    public ShippingAdapter(ArrayList<ShippingModel> shippingModels, ShippingProductActivity context, int resource) {
        this.shippingModels = shippingModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_two));

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);

    }

    @Override
    public ShippingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ShippingHolder slh = new ShippingHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(ShippingHolder holder, final int position) {

        holder.product_name_txt.setText(shippingModels.get(position).getProduct_name());
        holder.product_name_txt.setTypeface(typeface2);


        holder.ship_product_skuid.setText(shippingModels.get(position).getSku_id());
        holder.ship_product_skuid.setTypeface(typeface);

        holder.ship_product_count.setText(shippingModels.get(position).getProduct_count());
        holder.ship_product_count.setTypeface(typeface);

        //

        holder.quantity_txt.setText("Quantity : " + shippingModels.get(position).getPurchase_quantity() + " / "+ shippingModels.get(position).getProduct_weight());
        holder.quantity_txt.setTypeface(typeface);

        holder.price_txt.setText("Price : \u20B9" + shippingModels.get(position).getTotal_price() + " /-");
        holder.price_txt.setTypeface(typeface);

        holder.discount_txt.setText("Discount : \u20B9" + shippingModels.get(position).getDiscount() + " /-");
        holder.discount_txt.setTypeface(typeface);


        Picasso.with(context)
                .load(shippingModels.get(position).getImages())
                .placeholder(R.drawable.nodata_image)
                .into(holder.ship_product_img);

        Log.d("SHIPADAPTER", shippingModels.get(position).getProduct_name() + "\n" + shippingModels.get(position).getPurchase_quantity() + "\n" +
                shippingModels.get(position).getTotal_price());

        holder.setItemClickListener(new ShippingItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.shippingModels.size();
    }
}
