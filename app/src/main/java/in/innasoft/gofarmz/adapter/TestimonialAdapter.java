package in.innasoft.gofarmz.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.TestimonialActivity;
import in.innasoft.gofarmz.filters.CustomFilterForTestimonialList;
import in.innasoft.gofarmz.holder.TestimonialHolder;
import in.innasoft.gofarmz.itemclicklistners.TestimonialItemClickListener;
import in.innasoft.gofarmz.models.TestimonialModel;

public class TestimonialAdapter extends RecyclerView.Adapter<TestimonialHolder>implements Filterable {
    public ArrayList<TestimonialModel> testimonialModels,filterList;
    public TestimonialActivity context;
    CustomFilterForTestimonialList filter;
    LayoutInflater li;
    int resource;
    Typeface typeface,typeface2;

    public TestimonialAdapter(ArrayList<TestimonialModel> testimonialModels, TestimonialActivity context, int resource) {
        this.testimonialModels = testimonialModels;
        this.context = context;
        this.resource = resource;
        this.filterList = testimonialModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_two));

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterForTestimonialList(filterList,this);
        }

        return filter;
    }

    @Override
    public TestimonialHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        TestimonialHolder slh = new TestimonialHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(TestimonialHolder holder, final int position) {

        holder.testimonial_desp.setText(testimonialModels.get(position).getDescription());
        holder.testimonial_desp.setTypeface(typeface);

        holder.testimonial_tittle.setText(" - "+testimonialModels.get(position).getName());
        holder.testimonial_tittle.setTypeface(typeface2);

        holder.setItemClickListener(new TestimonialItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.testimonialModels.size();
    }
}
