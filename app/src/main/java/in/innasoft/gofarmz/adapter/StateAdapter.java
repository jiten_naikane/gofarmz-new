package in.innasoft.gofarmz.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.UserProfileActivity;
import in.innasoft.gofarmz.filters.CustomFilterForStateList;
import in.innasoft.gofarmz.holder.StateHolder;
import in.innasoft.gofarmz.itemclicklistners.StateItemClickListener;
import in.innasoft.gofarmz.models.StateModel;


public class StateAdapter extends RecyclerView.Adapter<StateHolder>implements Filterable {
    public ArrayList<StateModel> stateModels,filterList;
    public UserProfileActivity context;
    CustomFilterForStateList filter;
    LayoutInflater li;
    int resource;
    Typeface typeface;


    public StateAdapter(ArrayList<StateModel> stateModels, UserProfileActivity context, int resource) {
        this.stateModels = stateModels;
        this.context = context;
        this.resource = resource;
        this.filterList = stateModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterForStateList(filterList,this);
        }

        return filter;
    }

    @Override
    public StateHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        StateHolder slh = new StateHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(StateHolder holder, final int position) {

        holder.state_name_txt.setText(stateModels.get(position).getName());
        holder.state_name_txt.setTypeface(typeface);

        holder.setItemClickListener(new StateItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                context.setStateName(stateModels.get(pos).getName(),stateModels.get(pos).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.stateModels.size();
    }
}
