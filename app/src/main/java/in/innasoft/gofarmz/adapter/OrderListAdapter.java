package in.innasoft.gofarmz.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.OrderViewActivity;
import in.innasoft.gofarmz.activities.OrdersListActivity;
import in.innasoft.gofarmz.holder.OrderListHolder;
import in.innasoft.gofarmz.itemclicklistners.OrderListItemClickListener;
import in.innasoft.gofarmz.models.OrderListModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;

public class OrderListAdapter extends RecyclerView.Adapter<OrderListHolder> {

    public ArrayList<OrderListModel> orderListModels;
    public OrdersListActivity context;
    LayoutInflater li;
    int resource;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String user_id, token;
    UserSessionManager session;
    Typeface typeface, typeface2;

    public OrderListAdapter(ArrayList<OrderListModel> orderListModels, OrdersListActivity context, int resource) {
        this.orderListModels = orderListModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_two));

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME", className);

        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
    }

    @Override
    public OrderListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        OrderListHolder slh = new OrderListHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(OrderListHolder holder, final int position) {


//        holder.time_text_title.setTypeface(typeface2);
//        holder.reference_id_text_title.setTypeface(typeface2);
//        holder.order_id_text_title.setTypeface(typeface2);


        holder.order_id_txt.setText("Order Id "+orderListModels.get(position).getOrder_id());
        holder.order_id_txt.setTypeface(typeface2);

        holder.ref_id_txt.setText("Ref. Id "+orderListModels.get(position).getReference_id());
        holder.ref_id_txt.setTypeface(typeface);

        holder.price_txt.setText("\u20B9" + orderListModels.get(position).getFinal_price() + " /-");
        holder.price_txt.setTypeface(typeface);

        holder.time_txt.setText(orderListModels.get(position).getOrder_date() + " " + orderListModels.get(position).getOrder_time());
        holder.time_txt.setTypeface(typeface);

        // convert "yyyy-MM-dd"  to  dd MMM yyyy
      /*  DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        String inputDateStr="2013-06-24";
        Date date = inputFormat.parse(inputDateStr);
        String outputDateStr = outputFormat.format(date);*/



        holder.order_status.setText(orderListModels.get(position).getOrder_status());

        if (orderListModels.get(position).getOrder_status().equals("Cancelled")) {
            holder.order_status.setTextColor(Color.parseColor("#E74C3C"));
//            holder.view_border.setBackgroundColor(Color.parseColor("#E74C3C"));

        } else if (orderListModels.get(position).getOrder_status().equals("Pending")) {
            holder.order_status.setTextColor(Color.parseColor("#F1C40F"));
//            holder.view_border.setBackgroundColor(Color.parseColor("#F1C40F"));
        } else {
            holder.order_status.setTextColor(Color.parseColor("#27AE60"));
//                holder.view_border.setBackgroundColor(Color.parseColor("#27AE60"));
        }


        Picasso.with(context)
                .load(orderListModels.get(position).getImage())
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.order_img);

        // holder.order_status.setText(orderListModels.get(position).getOrder_status());

        /*if (orderListModels.get(position).status.equals("3")){
            holder.cancel_btn.setVisibility(View.GONE);
        }

        holder.cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                builder.setTitle("Alert");
                builder.setMessage("Do you really want to cancel your order?");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        String order_id = orderListModels.get(position).getId();
                        cancelOrder(order_id);
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();}

                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        holder.view_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderViewActivity.class);
                intent.putExtra("orders_pid",orderListModels.get(position).getId());
                context.startActivity(intent);
            }
        });*/

        holder.setItemClickListener(new OrderListItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent intent = new Intent(context, OrderViewActivity.class);
                intent.putExtra("orders_pid", orderListModels.get(position).getId());
                context.startActivity(intent);
            }
        });
    }

    private void cancelOrder(final String order_id) {
        orderListModels.clear();
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_ORDER_CANCEL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("OrdersResponce", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    progressDialog.cancel();
                                    Toast.makeText(context, "Your order has been cancelled..!", Toast.LENGTH_SHORT).show();
                                    ((OrdersListActivity) context).getOrders(user_id);
                                }

                                if (responceCode.equals("10200")) {
                                    Toast.makeText(context, "Sorry, Try Again..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    Toast.makeText(context, "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10786")) {
                                    Toast.makeText(context, "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    Log.d("ORDERCANCELHEADER", headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("orders_pid", order_id);
                    Log.d("OrdersCancel", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(context.getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public int getItemCount() {
        return this.orderListModels.size();
    }

}
