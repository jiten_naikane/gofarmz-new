package in.innasoft.gofarmz.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.UserProfileActivity;
import in.innasoft.gofarmz.filters.CustomFilterForCityList;
import in.innasoft.gofarmz.holder.CityProfileHolder;
import in.innasoft.gofarmz.itemclicklistners.CityProfileItemClickListener;
import in.innasoft.gofarmz.models.CityProfileModel;


public class CityAdapter extends RecyclerView.Adapter<CityProfileHolder>implements Filterable {
    public ArrayList<CityProfileModel> cityModels,filterList;
    public UserProfileActivity context;
    CustomFilterForCityList filter;
    LayoutInflater li;
    int resource;
    Typeface typeface;

    public CityAdapter(ArrayList<CityProfileModel> cityModels, UserProfileActivity context, int resource) {
        this.cityModels = cityModels;
        this.context = context;
        this.resource = resource;
        this.filterList = cityModels;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterForCityList(filterList,this);
        }

        return filter;
    }

    @Override
    public CityProfileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        CityProfileHolder slh = new CityProfileHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(CityProfileHolder holder, final int position) {

        holder.city_name_txt.setText(cityModels.get(position).getName());
        holder.city_name_txt.setTypeface(typeface);

        holder.setItemClickListener(new CityProfileItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                context.setCityName(cityModels.get(pos).getName(), cityModels.get(pos).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.cityModels.size();
    }
}
