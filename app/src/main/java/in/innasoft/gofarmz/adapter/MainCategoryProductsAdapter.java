package in.innasoft.gofarmz.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.appevents.AppEventsLogger;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.ProductDescriptionActivity;
import in.innasoft.gofarmz.holder.MostSellingHolder;
import in.innasoft.gofarmz.itemclicklistners.MostSellingItemClickListener;
import in.innasoft.gofarmz.models.MostSellingModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.CitySelectionSession;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;


public class MainCategoryProductsAdapter extends RecyclerView.Adapter<MostSellingHolder> {

    private ArrayList<MostSellingModel> seedCategoryList;
    Context context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;
    CitySelectionSession citySelectionSession;


    Boolean isOnline = false;
    private boolean statusFlag;
    private int lastPosition = -1;
    public String disp_city_name = "", disp_city_id = "";
    ProgressDialog progressDialog;
    String device_id;
    String user_id = "0", jwt_token = null;
    UserSessionManager session;


    public MainCategoryProductsAdapter(ArrayList<MostSellingModel> seedCategoryList, Context context, int resource) {
        this.seedCategoryList = seedCategoryList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        citySelectionSession = new CitySelectionSession(context);
        HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
        disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
        disp_city_name = cityDetails.get(CitySelectionSession.CITY_NAME);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_two));

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        device_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        jwt_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("CHANGNNN", user_id + "//" + jwt_token);
        if (jwt_token == null) {
            user_id = "0";
        } else {
            user_id = userDetails.get(UserSessionManager.USER_ID);
        }

    }

    @Override
    public MostSellingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutSeedCategory = li.inflate(resource, parent, false);
        MostSellingHolder sch = new MostSellingHolder(layoutSeedCategory);
        return sch;
    }

    @Override
    public void onBindViewHolder(MostSellingHolder holder, final int position) {

        // Here you apply the animation when the view is bound
        // setAnimation(holder.itemView, position);

        String str = seedCategoryList.get(position).getName();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.name.setText(converted_string);
        if (seedCategoryList.get(position).getCount_name() != null && !seedCategoryList.get(position).getCount_name().equalsIgnoreCase("null"))
            holder.count_no.setText(seedCategoryList.get(position).getCount_name());
        holder.name.setTypeface(typeface);
        holder.count_no.setTypeface(typeface);

        holder.price.setText("Rs. " + seedCategoryList.get(position).getMrp_price() + " / " + seedCategoryList.get(position).getWeight_name());
        holder.price.setTypeface(typeface);


        holder.rating.setText(seedCategoryList.get(position).getUser_rating());
        holder.rating.setTypeface(typeface);

        Picasso.with(context)
                .load(seedCategoryList.get(position).getImages())
                .placeholder(R.drawable.tool_bar_background)
                .into(holder.image);


        if (seedCategoryList.get(position).getAvailability().equalsIgnoreCase("out_of_stock")) {
            holder.image.setAlpha(20);
            holder.buttonBuy.setVisibility(View.GONE);
            holder.textAvailable.setText("Out of Stock");
        } else {
            holder.textAvailable.setText(seedCategoryList.get(position).getAvailability());
            holder.buttonBuy.setVisibility(View.VISIBLE);
//            holder.image.setAlpha(0);
        }


        holder.setItemClickListener(new MostSellingItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                /*Facebook Compaign*/
                AppEventsLogger logger = AppEventsLogger.newLogger(context);
                logger.logEvent("Click on Main Category");

                if (!seedCategoryList.get(position).getAvailability().equalsIgnoreCase("out_of_stock")) {
                    Intent intent = new Intent(context, ProductDescriptionActivity.class);
                    intent.putExtra("product_id", seedCategoryList.get(position).getId());
                    intent.putExtra("city_id", disp_city_id);
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "Out of Stock", Toast.LENGTH_SHORT).show();
                }
            }
        });


        holder.buttonBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendDataCart(position);
            }
        });

    }

    private void updateWishList() {

    }


    @Override
    public int getItemCount() {
        return this.seedCategoryList.size();
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    /////////////////////////////////////////////ADD TO CART DATA///////////////////////////////////////////////
    private void sendDataCart(final int pos) {
        if (NetworkChecking.isConnected(context)) {
            progressDialog.show();

            Log.d("ADDCARTURL:", AppUrls.BASE_URL + AppUrls.ADD_CART_DATA);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ADD_CART_DATA,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("ADDCARTRESP:", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("status");
                                if (successResponceCode.equals("10100")) {

                                    cartCountData();
                                    progressDialog.dismiss();
                                    Toast.makeText(context, "Added to cart successfully", Toast.LENGTH_SHORT).show();

                                }

                                if (successResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(context, "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("11786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(context, "All fields are required..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("productId", seedCategoryList.get(pos).getId());
                    params.put("browserId", device_id);
                    params.put("productQuantity", "1");

                    Log.d("ADDCARTPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    //  headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", jwt_token);
                    Log.d("ADDCARTHEADER:", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    private void cartCountData() {
        Intent intent = new Intent("custom-count");

        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
