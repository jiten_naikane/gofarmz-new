package in.innasoft.gofarmz.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.LoginActivity;
import in.innasoft.gofarmz.activities.ProductDescriptionActivity;
import in.innasoft.gofarmz.activities.WishListActivity;
import in.innasoft.gofarmz.holder.WishListHolder;
import in.innasoft.gofarmz.itemclicklistners.WishListItemClickListener;
import in.innasoft.gofarmz.models.WishListModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;

public class WishListAdapter extends RecyclerView.Adapter<WishListHolder>{

    private ArrayList<WishListModel> wishListModel;
    WishListActivity context;
    LayoutInflater li;
    int resource;
    String  user_id,jwt_token,page_no;
    private boolean checkInternet;
    UserSessionManager session;
    ProgressDialog progressDialog;
    Typeface typeface,typeface2;



    public WishListAdapter(ArrayList<WishListModel> wishListModel, WishListActivity context, int resource) {
        this.wishListModel = wishListModel;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_two));

        session = new UserSessionManager(context);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        jwt_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("WhishLISt",user_id+"//"+jwt_token);
    }

    @Override
    public WishListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutSeedCategory = li.inflate(resource,null);
        WishListHolder sch = new WishListHolder(layoutSeedCategory);
        return sch;
    }

    @Override
    public void onBindViewHolder(WishListHolder holder, final int position) {


        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);


        String str = wishListModel.get(position).getProduct_name();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.wishlist_title_text.setText(converted_string);
        holder.wishlist_title_text.setTypeface(typeface);

        holder.wishlist_weight_text.setText("Weight :  "+wishListModel.get(position).getProduct_weight_name());
        holder.wishlist_title_text.setTypeface(typeface);

        holder.wishlist_prize_text.setText("\u20B9"+ wishListModel.get(position).getProduct_mrp_prize()+" /-") ;//+" / "+wishListModel.get(position).getProduct_weight_name());
        holder.wishlist_prize_text.setTypeface(typeface);




        Picasso.with(context)
                .load(wishListModel.get(position).getImages())
                .placeholder(R.drawable.nodata_image)
                .into(holder.wishlsit_image_iv);



        holder.wishlsit_remove_btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

               // page_no="1";
                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                builder.setTitle("Alert");
                builder.setMessage("Are you sure?");
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which)
                    {

                        if (session.checkLogin() != false)
                        {
                            Intent i = new Intent(context, LoginActivity.class);
                            context.startActivity(i);
                        }
                        else {
                            checkInternet = NetworkChecking.isConnected(context);
                            if (checkInternet)
                            {
                                Log.d("WISHLISDELTTURL", AppUrls.BASE_URL + AppUrls.DELETE_WISHLIST);
                           final String  wish_remove_id= wishListModel.get(position).getId();
                                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.DELETE_WISHLIST,
                                        new Response.Listener<String>()
                                        {
                                            @Override
                                            public void onResponse(String response) {
                                                progressDialog.dismiss();

                                                try {
                                                    JSONObject jsonObject = new JSONObject(response);
                                                    Log.d("WISHLISTDELTRESP", response);
                                                    String responceCode = jsonObject.getString("status");

                                                    if (responceCode.equalsIgnoreCase("10100"))
                                                    {
                                                        Toast.makeText(context, "Successfully Removed From WishList", Toast.LENGTH_SHORT).show();
                                                        progressDialog.dismiss();

                                                       /* Intent intent = new Intent(context,WishListActivity.class);
                                                        context.startActivity(intent);
                                                        context.finish();*/
                                                        wishListModel.clear();
                                                      ((WishListActivity)context).wishList(1);
                                                     //   ((WishListActivity) context).functionToRun();


                                                    }
                                                    if (responceCode.equals("11786")) {
                                                        Toast.makeText(context, " All fields are required", Toast.LENGTH_SHORT).show();
                                                    }
                                                    if (responceCode.equals("10786")) {
                                                        Toast.makeText(context, " Invalid Token", Toast.LENGTH_SHORT).show();
                                                    }
                                                    if (responceCode.equals("10200")) {
                                                        Toast.makeText(context, "Try Again.", Toast.LENGTH_SHORT).show();
                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                progressDialog.dismiss();

                                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                                } else if (error instanceof AuthFailureError) {

                                                } else if (error instanceof ServerError) {

                                                } else if (error instanceof NetworkError) {

                                                } else if (error instanceof ParseError) {

                                                }
                                            }
                                        }) {
                                    @Override
                                    protected Map<String, String> getParams() throws AuthFailureError
                                    {
                                        Map<String, String> params = new HashMap<String, String>();
                                        params.put("id",wish_remove_id);
                                        params.put("user_id", user_id);
                                        Log.d("WDELETPARAM:", params.toString());
                                        return params;
                                    }

                                    @Override
                                    public Map<String, String> getHeaders() throws AuthFailureError {
                                        Map<String, String> headers = new HashMap<>();

                                        //  headers.put("Content-Type", "application/json");
                                        headers.put("Authorization-Basic",jwt_token);
                                        Log.d("WishListHEADER", headers.toString());
                                        return headers;
                                    }

                                };
                                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                RequestQueue requestQueue = Volley.newRequestQueue(context);
                                requestQueue.add(stringRequest);

                            } else {
                                progressDialog.cancel();
                                Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();

            }
        });

        holder.setItemClickListener(new WishListItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos)
            {
                  Intent wishtoproduct=new Intent(context, ProductDescriptionActivity.class);
                wishtoproduct.putExtra("product_id",wishListModel.get(position).getProduct_id());
                    context.startActivity(wishtoproduct);
            }
        });
    }


    @Override
    public int getItemCount()
    {
        return this.wishListModel.size();
    }




}
