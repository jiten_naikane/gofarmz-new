package in.innasoft.gofarmz.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.ProductDescriptionActivity;
import in.innasoft.gofarmz.holder.MostSellingHolder;
import in.innasoft.gofarmz.itemclicklistners.MostSellingItemClickListener;
import in.innasoft.gofarmz.models.MostSellingModel;
import in.innasoft.gofarmz.utilities.CitySelectionSession;


public class RelatedProductsAdapter extends RecyclerView.Adapter<MostSellingHolder> {

    private ArrayList<MostSellingModel> seedCategoryList;
    ProductDescriptionActivity context;
    LayoutInflater li;
    int resource;
    Typeface typeface, typeface2;
    CitySelectionSession citySelectionSession;

    Boolean isOnline = false;
    private boolean statusFlag;
    private int lastPosition = -1;
    public String disp_city_name = "", disp_city_id = "";

    public RelatedProductsAdapter(ArrayList<MostSellingModel> seedCategoryList, ProductDescriptionActivity context, int resource) {
        this.seedCategoryList = seedCategoryList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_two));
        citySelectionSession = new CitySelectionSession(context);
        HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
        disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
        disp_city_name = cityDetails.get(CitySelectionSession.CITY_NAME);
    }

    @Override
    public MostSellingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutSeedCategory = li.inflate(resource, null);
        MostSellingHolder sch = new MostSellingHolder(layoutSeedCategory);
        return sch;
    }

    @Override
    public void onBindViewHolder(MostSellingHolder holder, final int position) {

        // Here you apply the animation when the view is bound
        // setAnimation(holder.itemView, position);

        String str = seedCategoryList.get(position).getName();
        String converted_string = str.substring(0, 1).toUpperCase() + str.substring(1);
        holder.name.setText(converted_string);
        if (seedCategoryList.get(position).getCount_name() != null && !seedCategoryList.get(position).getCount_name().equalsIgnoreCase("null"))
            holder.count_no.setText(seedCategoryList.get(position).getCount_name());
        holder.name.setTypeface(typeface);
        holder.count_no.setTypeface(typeface);
        holder.price.setText("Rs." + seedCategoryList.get(position).getMrp_price() + "/" + seedCategoryList.get(position).getWeight_name());
        holder.price.setTypeface(typeface);

        holder.rating.setText(seedCategoryList.get(position).getUser_rating());
        holder.rating.setTypeface(typeface);


//        if (seedCategoryList.get(position).getFeatures().equals("1")){
//            holder.new_image.setVisibility(View.VISIBLE);
//        }
//        else{
//            holder.new_image.setVisibility(View.GONE);
//        }

        Picasso.with(context)
                .load(seedCategoryList.get(position).getImages())
                .placeholder(R.drawable.tool_bar_background)
                .into(holder.image);

        holder.setItemClickListener(new MostSellingItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent intent = new Intent(context, ProductDescriptionActivity.class);
                intent.putExtra("product_id", seedCategoryList.get(position).getId());
                intent.putExtra("city_id", disp_city_id);
                context.startActivity(intent);
                context.finish();

            }
        });
    }


    @Override
    public int getItemCount() {
        return this.seedCategoryList.size();
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
