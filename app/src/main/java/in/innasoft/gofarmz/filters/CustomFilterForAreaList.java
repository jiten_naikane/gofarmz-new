package in.innasoft.gofarmz.filters;

import android.widget.Filter;
import java.util.ArrayList;

import in.innasoft.gofarmz.adapter.AreaAdapter;
import in.innasoft.gofarmz.models.AreaModel;

public class CustomFilterForAreaList extends Filter {

    AreaAdapter adapter;
    ArrayList<AreaModel> filterList;

    public CustomFilterForAreaList(ArrayList<AreaModel> filterList, AreaAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<AreaModel> filteredPlayers=new ArrayList<AreaModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.areaModels = (ArrayList<AreaModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
