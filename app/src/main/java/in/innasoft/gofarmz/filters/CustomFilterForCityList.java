package in.innasoft.gofarmz.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.gofarmz.adapter.CityAdapter;
import in.innasoft.gofarmz.models.CityProfileModel;

public class CustomFilterForCityList extends Filter {

    CityAdapter adapter;
    ArrayList<CityProfileModel> filterList;

    public CustomFilterForCityList(ArrayList<CityProfileModel> filterList, CityAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<CityProfileModel> filteredPlayers=new ArrayList<CityProfileModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.cityModels = (ArrayList<CityProfileModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
