package in.innasoft.gofarmz.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.gofarmz.adapter.StateAdapter;
import in.innasoft.gofarmz.models.StateModel;

public class CustomFilterForStateList extends Filter {

    StateAdapter adapter;
    ArrayList<StateModel> filterList;

    public CustomFilterForStateList(ArrayList<StateModel> filterList, StateAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<StateModel> filteredPlayers=new ArrayList<StateModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.stateModels = (ArrayList<StateModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
