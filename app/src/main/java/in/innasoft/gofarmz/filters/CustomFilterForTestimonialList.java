package in.innasoft.gofarmz.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.gofarmz.adapter.TestimonialAdapter;
import in.innasoft.gofarmz.models.TestimonialModel;

public class CustomFilterForTestimonialList extends Filter {

    TestimonialAdapter adapter;
    ArrayList<TestimonialModel> filterList;

    public CustomFilterForTestimonialList(ArrayList<TestimonialModel> filterList, TestimonialAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<TestimonialModel> filteredPlayers=new ArrayList<TestimonialModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint) )
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.testimonialModels = (ArrayList<TestimonialModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
