package in.innasoft.gofarmz.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class CartBucketDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "cartbucket.db";

    private static final String TABLE_DETAILS = "cart";

    public static final String CART_PRODUCT_ID = "cart_product_id";
    public static final String CART_PRODUCT_NAME = "cart_product_name";
    public static final String CART_PRODUCT_IMAGE = "cart_product_image";
    //    public static final String CART_CATEGORY_ID = "cart_category_id";
//    public static final String CART_CATEGORY_SUB_ID = "cart_category_sub_id";
    public static final String CART_PRICE = "cart_price";
    //    public static final String CART_WEIGHT = "cart_weight";
//    public static final String CART_DESCRIPTION = "cart_description";
    public static final String CART_FINAL_PRICE = "cart_final_price";
    public static final String CART_QUANTITY = "cart_quantity";

    public CartBucketDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public CartBucketDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ORGANIC_TABLE = "CREATE TABLE " + TABLE_DETAILS + "("
                + CART_PRODUCT_ID + " TEXT ,"
                + CART_PRODUCT_NAME + " TEXT ,"
                + CART_PRODUCT_IMAGE + " TEXT ,"
                + CART_PRICE + " TEXT ,"
                + CART_FINAL_PRICE + " INTEGER ,"
                + CART_QUANTITY + " INTEGER )";
        db.execSQL(CREATE_ORGANIC_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETAILS);
        onCreate(db);
    }

    public boolean addCartBucket(ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();
        long rowInserted = db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
        if (rowInserted != -1)
            return true;
        else
            return false;

    }

    public void updateCart(String product_id, String quantity, String final_price) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CART_QUANTITY, quantity);
        values.put(CART_FINAL_PRICE, final_price);
        db.update(TABLE_DETAILS, values, CART_PRODUCT_ID + " = " + product_id, null);
        db.close();
    }

    public boolean deleteProduct(String productId) {
        boolean returnvalue = false;

        SQLiteDatabase db = this.getWritableDatabase();
        returnvalue = db.delete(TABLE_DETAILS, CART_PRODUCT_ID + "=" + productId, null) > 0;
        db.close();
        return returnvalue;
    }

    public List<String> getCartProductId() {
        List<String> productId = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                productId.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return productId;
    }

    public List<String> getProductName() {
        List<String> productName = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                productName.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return productName;
    }

    public List<String> getProductImage() {
        List<String> productImage = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                productImage.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return productImage;
    }

    public List<String> getProductPrice() {
        List<String> productPrice = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                productPrice.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return productPrice;
    }

/*    public List<String> getProductWeight() {
        List<String> productWeight = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                productWeight.add(cursor.getString(6));

            } while (cursor.moveToNext());
        }
        return productWeight;
    }*/

    public List<String> getProductFinalPrice() {
        List<String> productFinalPrice = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                productFinalPrice.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return productFinalPrice;
    }

    public List<String> getProductQuantity() {
        List<String> productQuantity = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                productQuantity.add(cursor.getString(5));

            } while (cursor.moveToNext());
        }
        return productQuantity;
    }

    public int findNumberOfQuanties() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("select sum(cart_quantity) from cart;", null);

        if (cur.moveToFirst()) {
            return cur.getInt(0);

        } else {
            return 0;
        }
    }

    public int findGndTotalPrice() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("select sum(cart_final_price) from cart;", null);

        if (cur.moveToFirst()) {
            return cur.getInt(0);

        } else {
            return 0;
        }
    }

    public int findNumberOfProducts() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("select * from cart;", null);
        int retunValue = cur.getCount();
        db.close();
        return retunValue;

    }


    public void emptyCartBucket() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_DETAILS); //delete all rows in a table
        db.close();
    }
}
