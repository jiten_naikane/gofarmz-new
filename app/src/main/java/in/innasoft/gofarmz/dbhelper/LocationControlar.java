package in.innasoft.gofarmz.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;


public class LocationControlar extends SQLiteOpenHelper
{
    private static final String TAG = "LocationControlar";
    private static final int DATABASE_VERSION = 18;
    private static final String DATABASE_NAME = "cart.db";

    private static final String TBL_LOCATION = "cartbucket";

    public static final String PRODUCT_ID = "product_id";
    public static final String PRODUCT_NAME = "product_name";
    public static final String PRODUCT_WEIGHT = "product_weight";
    public static final String PRODUCT_QUANT = "quantity";
    public static final String PRODUCT_MRP_PRIZE ="mrp_prize";
   public static final String PRODUCT_SKUID = "product_skuid";
   public static final String PRODUCT_COUNT = "product_count";
   public static final String PRODUCT_IMAGE = "product_image";
   public static final String PRODUCT_FINAL_AMOUNT = "final_amount";



    public LocationControlar(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_TABLE_LOCATIONS = "CREATE TABLE " + TBL_LOCATION + "("
                + PRODUCT_ID + " TEXT ,"
                + PRODUCT_NAME + " TEXT ,"
                + PRODUCT_WEIGHT + " TEXT ,"
                + PRODUCT_QUANT + " INTEGER ,"
                + PRODUCT_MRP_PRIZE + " TEXT ,"
                + PRODUCT_SKUID + " TEXT ,"
                + PRODUCT_COUNT + " TEXT ,"
                + PRODUCT_FINAL_AMOUNT + " TEXT ,"
                + PRODUCT_IMAGE + " TEXT )";

        db.execSQL(CREATE_TABLE_LOCATIONS);
    }




    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TBL_LOCATION);
        onCreate(db);
    }

    public boolean addCartBucket(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        long rowInserted = db.insert(TBL_LOCATION, null, contentValues);
        db.close();
        if(rowInserted != -1)
            return true;
        else
            return false;

    }

    public void updateCart(String product_id, String quantity, String final_price){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PRODUCT_QUANT, quantity);
        values.put(PRODUCT_FINAL_AMOUNT, final_price);
        db.update(TBL_LOCATION, values, PRODUCT_ID+" = "+product_id, null);
        db.close();
    }

    public boolean deleteProduct(String productId)
    {
        boolean returnvalue = false;

        SQLiteDatabase db = this.getWritableDatabase();
        returnvalue = db.delete(TBL_LOCATION, PRODUCT_ID + "=" + productId, null) > 0;
        db.close();
        return returnvalue;
    }

    public List<String> getCartProductId() {
        List<String> productId = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TBL_LOCATION;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                productId.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return productId;
    }

    public List<String> getProductName() {
        List<String> productName = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TBL_LOCATION;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                productName.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return productName;
    }

    public List<String> getProductImage() {
        List<String> productImage = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TBL_LOCATION;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                productImage.add(cursor.getString(8));

            } while (cursor.moveToNext());
        }
        return productImage;
    }

    public List<String> getProductPrice() {
        List<String> productPrice = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TBL_LOCATION;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                productPrice.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return productPrice;
    }

    public List<String> getProductWeight() {
        List<String> productWeight = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TBL_LOCATION;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                productWeight.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return productWeight;
    }

    public List<String> getProductSkuId() {
        List<String> productWeight = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TBL_LOCATION;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                productWeight.add(cursor.getString(5));

            } while (cursor.moveToNext());
        }
        return productWeight;
    }

    public List<String> getProductCountNamet() {
        List<String> productWeight = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TBL_LOCATION;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                productWeight.add(cursor.getString(6));

            } while (cursor.moveToNext());
        }
        return productWeight;
    }




    public List<String> getProducatFinalPrice() {
        List<String> productFinalPrice = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TBL_LOCATION;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                productFinalPrice.add(cursor.getString(7));

            } while (cursor.moveToNext());
        }
        return productFinalPrice;
    }

    public List<String> getProductQuantity() {
        List<String> productQuantity = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TBL_LOCATION;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                productQuantity.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return productQuantity;
    }

    public int findNumberOfQuanties(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("select sum(cart_quantity) from cart;", null);

        if(cur.moveToFirst())
        {
            return cur.getInt(0);

        }else
        {
            return 0;
        }
    }

    public int findGndTotalPrice(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("select sum(final_amount) from cartbucket;", null);

        if(cur.moveToFirst())
        {
            return cur.getInt(0);

        }else
        {
            return 0;
        }
    }


    public int findNumberOfProducts(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("select * from cartbucket;", null);
        int retunValue = cur.getCount();
        db.close();
        return retunValue;

    }


    public void emptyCartBucket()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TBL_LOCATION); //delete all rows in a table
        db.close();
    }

}
