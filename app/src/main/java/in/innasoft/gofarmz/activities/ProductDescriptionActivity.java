package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.appevents.AppEventsLogger;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapter.RelatedProductsAdapter;
import in.innasoft.gofarmz.dbhelper.LocationControlar;
import in.innasoft.gofarmz.models.MostSellingModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.CitySelectionSession;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;


public class ProductDescriptionActivity extends AppCompatActivity implements View.OnClickListener {
    private boolean checkInternet;
    ImageView view_product_img, fab, image, textblinkReceipe;
    ProgressDialog progressDialog;
    Toolbar toolbar;
    TextView weight_text_title, quantity_text_title, skuid_text_title, count_no_text_title, description_txt, Related_product_titele_text;
    TextView weight, price, description, rating, quantity_text, sku_id, count_no, count_cart_text, toolbar_title, title, product_details;
    int quantity = 1;
    ImageView plus, minus, new_image, cart, close, share;
    Button add_cart;
    String device_id;
    int price_value;
    int total_price_value;
    RecyclerView most_selling_recyclerview;
    ArrayList<MostSellingModel> mostsellinglist;
    RelatedProductsAdapter mostsellingadapter;
    CitySelectionSession citySelectionSession;
    public String disp_city_name = "", disp_city_id = "";
    int defaultPageNo = 1;
    private static int displayedposition = 0;
    int total_number_of_items = 0;
    private boolean loading = true;
    int type_of_request = 0;
    private boolean userScrolled = true;
    LinearLayoutManager layoutManager;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    String main_category_id;
    String sub_category_id;
    String child_category_id;
    UserSessionManager session;
    String user_id = "0", jwt_token = null;
    private long mLastClickTime = 0;
    String id_delete;
    LinearLayout description_layout;
//    RatingBar rating_bar;
    String rateValue = "0";
    //new global variable
    String sku_id_string, product_name, qty, mrp_price, images, weight_name, count_name;
    LocationControlar cartBucketDBHelper;
    String cart_item_count = "";
    Typeface typeface, typeface2;
//    ReceipeAdapter receipeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_description);

        //  blinkTextView();

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_two));
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        jwt_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("CHANGNNN", user_id + "//" + jwt_token);
        if (jwt_token == null) {
            user_id = "0";
        } else {
            user_id = userDetails.get(UserSessionManager.USER_ID);
        }

        citySelectionSession = new CitySelectionSession(ProductDescriptionActivity.this);
        HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
        disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
        disp_city_name = cityDetails.get(CitySelectionSession.CITY_NAME);

        //   disp_lng = cityDetails.get(CitySelectionSession.CITY_LAT);
        //   disp_lng = cityDetails.get(CitySelectionSession.CITY_LNG);
        try {


        } catch (NullPointerException e) {
            String msg = (e.getMessage() == null) ? "Login failed!" : e.getMessage();
            Log.i("Login Error1", msg);
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("DEVICEID:", device_id);
        cartBucketDBHelper = new LocationControlar(ProductDescriptionActivity.this);

        count_cart_text = (TextView) findViewById(R.id.count_cart_text);

       /* String countValues = String.valueOf(cartBucketDBHelper.findNumberOfProducts());
        Log.d("COUNT",countValues);

        if (countValues.equals("null")|| countValues.equals("") || countValues.equals("0"))
        {
            count_cart_text.setVisibility(View.GONE);
        }else {
            count_cart_text.setVisibility(View.VISIBLE);
            count_cart_text.setText(countValues);
        }*/


        cartCountData();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        view_product_img = (ImageView) findViewById(R.id.view_product_img);
        cart = (ImageView) findViewById(R.id.cart);
        cart.setOnClickListener(this);
        weight = (TextView) findViewById(R.id.weight);
        weight.setTypeface(typeface);
        price = (TextView) findViewById(R.id.price);
        price.setTypeface(typeface);
        description = (TextView) findViewById(R.id.description);
        description.setTypeface(typeface);
        rating = (TextView) findViewById(R.id.rating);
        rating.setTypeface(typeface);
        quantity_text = (TextView) findViewById(R.id.quantity);
        quantity_text.setTypeface(typeface);
        sku_id = (TextView) findViewById(R.id.sku_id);
        sku_id.setTypeface(typeface);
         count_no = (TextView) findViewById(R.id.count_no);
        count_no.setTypeface(typeface);
        title = (TextView) findViewById(R.id.title);
        title.setTypeface(typeface);
        textblinkReceipe = (ImageView) findViewById(R.id.textblinkReceipe);
        textblinkReceipe.setOnClickListener(this);

        weight_text_title = (TextView) findViewById(R.id.weight_text_title);
        weight_text_title.setTypeface(typeface);

        quantity_text_title = (TextView) findViewById(R.id.quantity_text_title);
        quantity_text_title.setTypeface(typeface);

        skuid_text_title = (TextView) findViewById(R.id.skuid_text_title);
        skuid_text_title.setTypeface(typeface);

        count_no_text_title = (TextView) findViewById(R.id.count_no_text_title);
        count_no_text_title.setTypeface(typeface);
        // ,,,,,,
        description_txt = (TextView) findViewById(R.id.description_txt);
        description_txt.setTypeface(typeface2);

        product_details = (TextView) findViewById(R.id.product_details);
        product_details.setTypeface(typeface2);

        Related_product_titele_text = (TextView) findViewById(R.id.Related_product_titele_text);
        Related_product_titele_text.setTypeface(typeface2);

        count_no_text_title = (TextView) findViewById(R.id.count_no_text_title);
        count_no_text_title.setTypeface(typeface);

         description_layout = (LinearLayout) findViewById(R.id.description_layout);
        //plus = (ImageView) findViewById(R.id.plus);
        //  minus = (ImageView) findViewById(R.id.minus);
        image = (ImageView) findViewById(R.id.image);
        share = (ImageView) findViewById(R.id.share);
        add_cart = (Button) findViewById(R.id.add_cart);
        fab = (ImageView) findViewById(R.id.fab);
        new_image = (ImageView) findViewById(R.id.new_image);
        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);

        fab.setOnClickListener(this);
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(typeface);
        share.setOnClickListener(this);
        //  plus.setOnClickListener(this);
        //  minus.setOnClickListener(this);
        add_cart.setOnClickListener(this);
      /*  setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
        getProductCompleteView();

        quantity_text.setText(": " + quantity);


        most_selling_recyclerview = (RecyclerView) findViewById(R.id.most_selling_recyclerview);
        mostsellinglist = new ArrayList<MostSellingModel>();
        mostsellingadapter = new RelatedProductsAdapter(mostsellinglist, ProductDescriptionActivity.this, R.layout.most_selling_row);
        layoutManager = new LinearLayoutManager(this);

        most_selling_recyclerview.setNestedScrollingEnabled(false);
        most_selling_recyclerview.setLayoutManager(new LinearLayoutManager(ProductDescriptionActivity.this, LinearLayoutManager.HORIZONTAL, false));
        //   most_selling_recyclerview.setLayoutManager(layoutManager);

        most_selling_recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    displayedposition = pastVisiblesItems;
                    Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);

                    if (loading) {
                        if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            userScrolled = false;
                            Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
                            Log.d("PPPPPPPPP", String.valueOf(defaultPageNo));
                            if (totalItemCount < total_number_of_items) {
                                type_of_request = 1;

                                getMostSellingProducts(defaultPageNo);

                            } else {
                                Toast.makeText(ProductDescriptionActivity.this, "No More Results", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });
//        if (jwt_token != null) {
//            reviewStatus();
//
//        }
        updateLike();

//        getReviews();

        /*Facebook Compaign*/
        AppEventsLogger logger = AppEventsLogger.newLogger(ProductDescriptionActivity.this);
        logger.logEvent("Product Detail Page");
    }



    private void getProductCompleteView() {
        /*String countValues = String.valueOf(cartBucketDBHelper.findNumberOfProducts());

        if (countValues.equals("null")|| countValues.equals("") || countValues.equals("0")){
            count_cart_text.setVisibility(View.GONE);
        }else {
            count_cart_text.setVisibility(View.VISIBLE);
            count_cart_text.setText(countValues);
        }*/

        checkInternet = NetworkChecking.isConnected(ProductDescriptionActivity.this);
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_PRODUCT_DETAIL_VIEW,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("PRODURESP", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {

                                    JSONObject json = jsonObject.getJSONObject("data");
                                    String id = json.getString("id");

                                    sku_id_string = json.getString("sku_id");
                                    sku_id.setText(": " + sku_id_string);
                                    product_name = json.getString("product_name");
                                    toolbar_title.setText(product_name);
                                    title.setText(product_name);
                                    String user_rating = json.getString("user_rating");
                                    rating.setText(user_rating + "");
                                    String url_name = json.getString("url_name");
                                    qty = json.getString("qty");
                                    mrp_price = json.getString("mrp_price");
                                    price.setText("" + Html.fromHtml("\u20B9" + "<b>" + mrp_price + "</b>" + " /-"));
                                    price_value = Integer.parseInt(mrp_price);
                                    total_price_value = Integer.parseInt(mrp_price);
                                    String count_id = json.getString("count_id");
                                    images = AppUrls.IMAGE_URL + json.getString("images");
                                    Picasso.with(ProductDescriptionActivity.this)
                                            .load(images)
                                            .placeholder(R.drawable.tool_bar_background)
                                            .into(view_product_img);
//                                    String features = json.getString("features");

//                                    if (features.equals("1")){
//                                        new_image.setVisibility(View.VISIBLE);
//                                    }
//                                    else{
//                                        new_image.setVisibility(View.GONE);
//                                    }

//                                    final String youtube1 = json.getString("youtube1");
//                                    final String youtube2 = json.getString("youtube2");
//                                    if (youtube1.equals("") && youtube2.equals("")) {
//                                        related_videos_layout.setVisibility(View.GONE);
//                                    } else {
//                                        related_videos_layout.setVisibility(View.VISIBLE);
//                                        if (!youtube1.equals("") && !youtube2.equals("")) {
//                                            image.setVisibility(View.VISIBLE);
//                                            image1.setVisibility(View.VISIBLE);
//                                            image.setOnClickListener(new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View v) {
//                                                    try {
//                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(youtube1)));
//                                                    } catch (Exception e) {
//
//                                                    }
//                                                }
//                                            });
//                                            image1.setOnClickListener(new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View v) {
//                                                    try {
//                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(youtube2)));
//                                                    } catch (Exception e) {
//
//                                                    }
//
//                                                }
//                                            });
//                                        } else if (!youtube1.equals("") && youtube2.equals("")) {
//                                            image.setVisibility(View.VISIBLE);
//                                            image1.setVisibility(View.GONE);
//                                            image.setOnClickListener(new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View v) {
//                                                    try {
//                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(youtube1)));
//                                                    } catch (Exception e) {
//
//                                                    }
//                                                }
//                                            });
//                                        } else if (youtube1.equals("") && !youtube2.equals("")) {
//                                            image.setVisibility(View.GONE);
//                                            image1.setVisibility(View.VISIBLE);
//                                            image1.setOnClickListener(new View.OnClickListener() {
//                                                @Override
//                                                public void onClick(View v) {
//                                                    try {
//                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(youtube2)));
//                                                    } catch (Exception e) {
//
//                                                    }
//                                                }
//                                            });
//                                        }
//                                    }
                                    String about = json.getString("about");
                                    if (about.equals("") || about == null) {
                                        description_layout.setVisibility(View.GONE);
                                    } else {
                                        description_layout.setVisibility(View.VISIBLE);
                                        description.setText(Html.fromHtml(about));
                                    }
//                                    String moreinfo = json.getString("moreinfo");
//                                    if (moreinfo.equals("") || moreinfo == null) {
//                                        more_info_layout.setVisibility(View.GONE);
//                                    } else {
//                                        more_info_layout.setVisibility(View.VISIBLE);
//                                        more_info.setText(Html.fromHtml(moreinfo));
//                                    }

//                                    String seo_title = json.getString("seo_title");
//                                    String seo_description = json.getString("seo_description");
//
//                                    String seo_keywords = json.getString("seo_keywords");
                                    main_category_id = json.getString("main_category_id");
                                    sub_category_id = json.getString("sub_category_id");
                                    child_category_id = json.getString("child_category_id");
                                    weight_name = json.getString("weight_name");
                                    weight.setText(": " + Html.fromHtml("<b>" + weight_name + "</b>"));
                                    count_name = json.getString("count_name");
                                    if (!count_name.equals("0") && !count_name.equalsIgnoreCase("null") && count_name != null) {
                                        count_no.setVisibility(View.VISIBLE);
                                        count_no.setText(": " + count_name);
                                    } else {
                                        count_no.setVisibility(View.GONE);
                                    }
//                                    String status = json.getString("status");
                                }
                                getMostSellingProducts(defaultPageNo);
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("location_id", disp_city_id);
                    params.put("product_id", getIntent().getExtras().getString("product_id"));


                    Log.d("PRODUCTPARAM:", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(ProductDescriptionActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View v) {
//        if (v == share_layout) {
//            Intent sendIntent = new Intent();
//            sendIntent.setAction(Intent.ACTION_SEND);
//            sendIntent.putExtra(Intent.EXTRA_TEXT,
//                    product_name + " - " + mrp_price + "/" + weight_name);
//            sendIntent.setType("text/plain");
//            startActivity(sendIntent);
//        }
        if (v == share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT,
                    product_name + " - " + mrp_price + "/" + weight_name);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }
        if (v == minus) {
            quantity = Integer.parseInt(quantity_text.getText().toString());
            quantity--;
            quantity_text.setText("" + quantity--);

            int quantity_minus = Integer.parseInt(quantity_text.getText().toString());
            total_price_value = quantity_minus * price_value;
            Log.d("price_valuesss", "" + total_price_value);
            if (quantity < 1) {
                Toast.makeText(this, "Product Quantity should be 1 or More than 1", Toast.LENGTH_SHORT).show();
                quantity = 1;
                quantity_text.setText("" + quantity);

                int quantity_same = Integer.parseInt(quantity_text.getText().toString());
                total_price_value = quantity_same * price_value;
                Log.d("price_valuesss", "" + total_price_value);
            }

        }
        if (v == cart) {
            if (!cart_item_count.equals("0")) {
                String countValues = String.valueOf(cartBucketDBHelper.findNumberOfProducts());
                count_cart_text.setText(countValues);
                Intent intent = new Intent(this, CartProductListActivity.class);
                intent.putExtra("activity_name", "productDescription");
                startActivity(intent);
            } else {
                Toast.makeText(ProductDescriptionActivity.this, "Cart is Empty", Toast.LENGTH_SHORT).show();
            }

        }
        if (v == plus) {
            quantity = Integer.parseInt(quantity_text.getText().toString());
            quantity++;
            quantity_text.setText("" + quantity++);

            int quantity_plus = Integer.parseInt(quantity_text.getText().toString());
            total_price_value = quantity_plus * price_value;
            Log.d("price_valuesss", "" + total_price_value);
        }
        if (v == add_cart) {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 5000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            if (jwt_token == null) {
                user_id = "0";
                sendDataCart();
            } else {
                sendDataCart();
            }
        }

        if (v == fab) {
        }

        if (v == close) {
            finish();
        }
        if (v == textblinkReceipe) {
            Toast.makeText(getApplicationContext(), "BLINKKK", Toast.LENGTH_SHORT).show();
        }

    }

    /////////////////////////////////////////////ADD TO CART DATA///////////////////////////////////////////////
    private void sendDataCart() {
        checkInternet = NetworkChecking.isConnected(ProductDescriptionActivity.this);
        if (checkInternet) {
            progressDialog.show();

            Log.d("ADDCARTURL:", AppUrls.BASE_URL + AppUrls.ADD_CART_DATA);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ADD_CART_DATA,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("ADDCARTRESP:", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("status");
                                if (successResponceCode.equals("10100")) {

                                    cartCountData();
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Added to cart successfully", Toast.LENGTH_SHORT).show();

                                }

                                if (successResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("11786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "All fields are required..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("productId", getIntent().getExtras().getString("product_id"));
                    params.put("browserId", device_id);
                    params.put("productQuantity", String.valueOf(quantity));

//                    Log.d("ADDCARTPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    //  headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", jwt_token);
//                    Log.d("ADDCARTHEADER:", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    private void updateLike() {
        checkInternet = NetworkChecking.isConnected(ProductDescriptionActivity.this);
        if (checkInternet) {
            progressDialog.show();

//            Log.d("ADDCARTURL:", AppUrls.BASE_URL + AppUrls.ADD_CART_DATA);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_PRODUCT_WISHLIST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            Log.d("ADDCARTRESP:", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("status");
                                if (successResponceCode.equals("10100")) {
                                    JSONObject likeobj = jsonObject.getJSONObject("data");
                                    String like_string = likeobj.getString("recordTotalCnt");
                                    JSONObject id_object = likeobj.getJSONObject("productDetails");
//                                    String pr_de = String.valueOf(id_object);

                                    id_delete = id_object.getString("id");
//                                    Log.d("sdfjkgbfjh", id_delete + " - " + like_string);

                                    if (!like_string.equals("0")) {
                                        fab.setBackgroundResource(R.drawable.fav_yellow);
                                        fab.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                updateDisLike();

                                            }
                                        });
                                    } else {
                                        fab.setBackgroundResource(R.drawable.ic_heart_outline_white_24dp);
                                        fab.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (jwt_token == null || jwt_token.equals("")) {
                                                    // Toast.makeText(ProductDescriptionActivity.this, "User Must be Login for this Operation", Toast.LENGTH_SHORT).show();
                                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(ProductDescriptionActivity.this);
                                                    builder1.setMessage("User Must be Login for this Operation, Click yes to redirect to login Page");
                                                    builder1.setCancelable(true);

                                                    builder1.setPositiveButton(
                                                            "Yes",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                    dialog.cancel();
                                                                    Intent intent = new Intent(ProductDescriptionActivity.this, LoginActivity.class);
                                                                    intent.putExtra("activity_name", "productDescription");
                                                                    startActivity(intent);
                                                                }
                                                            });

                                                    builder1.setNegativeButton(
                                                            "No",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                    dialog.cancel();
                                                                }
                                                            });

                                                    AlertDialog alert11 = builder1.create();
                                                    alert11.show();
                                                } else {
                                                    updateWishList();
                                                }
                                            }
                                        });
                                    }
                                    progressDialog.dismiss();
                                    //   Toast.makeText(getApplicationContext(), "Added to cart successfully", Toast.LENGTH_SHORT).show();

                                }

                                if (successResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("11786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "All fields are required..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("product_id", getIntent().getExtras().getString("product_id"));


//                    Log.d("ADDCARTPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    //  headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", jwt_token);
//                    Log.d("ADDCARTHEADER:", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    private void updateDisLike() {
        checkInternet = NetworkChecking.isConnected(ProductDescriptionActivity.this);
        if (checkInternet) {
            progressDialog.show();

//            Log.d("ADDCARTURL:", AppUrls.BASE_URL + AppUrls.REMOVE_FAVOURATE);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REMOVE_FAVOURATE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            Log.d("fghdfngADDCARTRESP:", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("status");
                                if (successResponceCode.equals("10100")) {
                                    //  JSONObject likeobj = jsonObject.getJSONObject("data");
                                    Toast.makeText(getApplicationContext(), "Removed from wish List successfully", Toast.LENGTH_SHORT).show();
                                    //  fab.setBackgroundResource(R.drawable.love_image);
                                    updateLike();

                                    progressDialog.dismiss();
                                    //   Toast.makeText(getApplicationContext(), "Added to cart successfully", Toast.LENGTH_SHORT).show();

                                }

                                if (successResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("11786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "All fields are required..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("product_id", getIntent().getExtras().getString("product_id"));
                    params.put("id", id_delete);


//                    Log.d("ADDCARTPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    //  headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", jwt_token);
//                    Log.d("ADDCARTHEADER:", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }
    //////////////////////////////////////END///////////////////////////////////////////////

    private void getMostSellingProducts(final int default_page_number) {

        checkInternet = NetworkChecking.isConnected(ProductDescriptionActivity.this);
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_RELATED_PRODUCT_LIST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
//                                Log.d("fasdhxcfghfgtwwwwwwww", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    most_selling_recyclerview.setVisibility(View.VISIBLE);
//                                    related_products_layout.setVisibility(View.VISIBLE);

                                    JSONObject json = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = json.getJSONArray("recordData");
                                    int jsonArray_count = json.getInt("recordTotalCnt");

                                    total_number_of_items = jsonArray_count;
//                                    Log.d("LOADSTATUS", "OUTER " + loading + "  " + jsonArray_count);
                                    if (jsonArray_count > mostsellinglist.size()) {
                                        loading = true;
//                                        Log.d("LOADSTATUS", "INNER " + loading + "  " + jsonArray_count);
                                    } else {
                                        loading = false;
                                    }

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        MostSellingModel gbm = new MostSellingModel();

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        gbm.setId(jsonObject1.getString("id"));
                                        gbm.setName(jsonObject1.getString("name"));
                                        gbm.setUrl_name(jsonObject1.getString("url_name"));
                                        gbm.setCount_id(jsonObject1.getString("count_id"));
                                        gbm.setWeight_id(jsonObject1.getString("weight_id"));
                                        gbm.setCount_name(jsonObject1.getString("count_name"));
                                        gbm.setWeight_name(jsonObject1.getString("weight_name"));
                                        gbm.setQty(jsonObject1.getString("qty"));
                                        gbm.setMrp_price(jsonObject1.getString("mrp_price"));
                                        gbm.setImages(AppUrls.PRODUCTS_IMAGE_URL + jsonObject1.getString("images"));
                                        gbm.setFeatures(jsonObject1.getString("features"));
                                        gbm.setStatus(jsonObject1.getString("status"));
                                        gbm.setUser_rating(jsonObject1.getString("user_rating"));


                                        mostsellinglist.add(gbm);

                                    }
                                    layoutManager.scrollToPositionWithOffset(displayedposition, mostsellinglist.size());
                                    most_selling_recyclerview.setAdapter(mostsellingadapter);

                                }
                                if (responceCode.equals("12786")) {
                                    //  Toast.makeText(ProductDescriptionActivity.this, "No Data Found..!", Toast.LENGTH_SHORT).show();
//                                    related_products_layout.setVisibility(View.GONE);
                                    most_selling_recyclerview.setVisibility(View.GONE);
                                }
                                if (responceCode.equals("11786")) {
                                    Toast.makeText(ProductDescriptionActivity.this, "Error..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("location_id", disp_city_id);
                    params.put("page_no", String.valueOf(default_page_number));
                    params.put("main_category_id", main_category_id);
                    params.put("sub_category_id", sub_category_id);
                    params.put("product_id", getIntent().getExtras().getString("product_id"));

//                    Log.d("LoginREQUESTDATA:", params.toString());
                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(ProductDescriptionActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    public void updateWishList() {
        checkInternet = NetworkChecking.isConnected(ProductDescriptionActivity.this);
        if (checkInternet) {
            progressDialog.show();

//            Log.d("ADDFAVURL:", AppUrls.BASE_URL + AppUrls.ADD_FAVOURATE);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ADD_FAVOURATE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            Log.d("ADDFAVRESP:", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("status");
                                if (successResponceCode.equals("10100")) {
                                    // fab.setBackgroundResource(R.drawable.like_red_icon);
                                    progressDialog.dismiss();
                                    updateLike();
                                    Toast.makeText(getApplicationContext(), "Added Successfully", Toast.LENGTH_SHORT).show();

                                }

                                if (successResponceCode.equals("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("10300")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Already Exists..!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("11786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "All fields are required..!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("10786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Invalid Token.!", Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("product_id", getIntent().getExtras().getString("product_id"));

//                    Log.d("ADDFAVPAram:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    //  headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", jwt_token);
//                    Log.d("CAHGENHEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        jwt_token = userDetails.get(UserSessionManager.KEY_ACCSES);
//        Log.d("CHANGNNN", user_id + "//" + jwt_token);
        if (jwt_token == null) {
            user_id = "0";
        } else {
            user_id = userDetails.get(UserSessionManager.USER_ID);
        }
        cartCountData();

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_category, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void cartCountData() {
//        Log.d("RESSSSSCOUNTTTURL:", AppUrls.BASE_URL + AppUrls.GET_CART_COUNT);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_CART_COUNT,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

//                        Log.d("RESSSSSCOUNTTT:", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String status = jsonObject.getString("status");
                            if (status.equals("10100")) {

                                JSONObject jobdata = jsonObject.getJSONObject("data");
                                int coumnt = jobdata.getInt("recordDataCount");
                                if (coumnt == 0) {
                                    count_cart_text.setVisibility(View.GONE);
                                    cart_item_count = String.valueOf(coumnt);
                                } else {
                                    count_cart_text.setVisibility(View.VISIBLE);
                                    count_cart_text.setText("" + coumnt);
                                    cart_item_count = String.valueOf(coumnt);

                                }


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                        } else if (error instanceof AuthFailureError) {
                            //TODO
                        } else if (error instanceof ServerError) {
                            //TODO
                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {
                            //TODO
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("browserId", device_id);

//                Log.d("RESSSSSCOUNTTTPARAM:", params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ProductDescriptionActivity.this);
        requestQueue.add(stringRequest);
    }
}
