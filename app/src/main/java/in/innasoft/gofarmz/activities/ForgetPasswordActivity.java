package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;

public class ForgetPasswordActivity extends AppCompatActivity implements View.OnClickListener
{
    TextInputLayout forgot_til;
    EditText forgot_edt;
    Button change_btn;
    ProgressDialog progressDialog;
    private boolean checkInternet;
    CollapsingToolbarLayout ctl;
    AppBarLayout app_bar_layout;
    Typeface typeface;
    String device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("DEVICEID:",device_id);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));

        ctl = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
       // ctl.setTitle("FORGOT PASSWORD");
        ctl.setExpandedTitleTypeface(typeface);
        ctl.setCollapsedTitleTextColor(Color.WHITE);


        ctl.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        ctl.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        app_bar_layout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        forgot_til = (TextInputLayout) findViewById(R.id.forgot_til);

        forgot_edt = (EditText) findViewById(R.id.forgot_edt);
        forgot_edt.setTypeface(typeface);
       /* forgot_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    app_bar_layout.setExpanded(false, true);
                }
            }
        });*/

        change_btn = (Button) findViewById(R.id.change_btn);
        change_btn.setTypeface(typeface);
        change_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view)
    {
         if(view==change_btn)
         {
             if (validate())
             {
                 checkInternet = NetworkChecking.isConnected(this);
                 if (checkInternet)
                 {
                     progressDialog.show();
                     Log.d("FORGETURL", AppUrls.BASE_URL+AppUrls.FORGOT_PASSWORD);
                     final String forget_mobile=forgot_edt.getText().toString().trim();
                     final StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+AppUrls.FORGOT_PASSWORD,
                             new Response.Listener<String>() {
                                 @Override
                                 public void onResponse(String response) {

                                     Log.d("FORGETURLRESP",response);
                                     try {
                                         JSONObject jsonObject = new JSONObject(response);
                                         String successResponceCodeForget = jsonObject.getString("status");
                                         if (successResponceCodeForget.equals("10100"))
                                         {
                                             progressDialog.dismiss();
                                             Toast.makeText(getApplicationContext(), "OTP has been sent successfully...!", Toast.LENGTH_SHORT).show();
                                             Intent intent = new Intent(getApplicationContext(), ResetPasswordActivity.class);
                                             intent.putExtra("activity_name",getIntent().getExtras().getString("activity_name"));
                                             intent.putExtra("Mobile",forget_mobile);
                                             startActivity(intent);
                                             finish();
                                         }

                                        if (successResponceCodeForget.equals("10200"))
                                         {
                                             progressDialog.dismiss();
                                             Toast.makeText(getApplicationContext(), "Sorry, Try again.", Toast.LENGTH_SHORT).show();
                                         }

                                         if (successResponceCodeForget.equals("10300"))
                                         {
                                             progressDialog.dismiss();
                                             Toast.makeText(getApplicationContext(), "Incorrect Mobile No.", Toast.LENGTH_SHORT).show();
                                         }

                                         if(successResponceCodeForget.equals("11786"))
                                         {
                                             progressDialog.dismiss();
                                             Toast.makeText(getApplicationContext(), "All fields are required....!",Toast.LENGTH_SHORT).show();

                                         }
                                     } catch (JSONException e) {
                                         e.printStackTrace();
                                     }
                                 }
                             },
                             new Response.ErrorListener() {
                                 @Override
                                 public void onErrorResponse(VolleyError error) {

                                     progressDialog.dismiss();

                                     if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                                     } else if (error instanceof AuthFailureError) {
                                     } else if (error instanceof ServerError) {
                                     } else if (error instanceof NetworkError) {
                                     } else if (error instanceof ParseError) {
                                     }
                                 }
                             })
                     {
                         @Override
                         protected Map<String, String> getParams() throws AuthFailureError {
                             Map<String, String> params = new HashMap<String, String>();
                             params.put("mobile", forget_mobile);
                             params.put("browser_id", device_id);
                             Log.d("FORGETURLPARAM:",params.toString());
                             return params;
                         }
                     };
                     RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                     requestQueue.add(stringRequest);
                 }
                 else {
                     Snackbar snackbar = Snackbar.make(view, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                     snackbar.show();
                 }
             }
         }
    }

    private boolean validate()
    {

        String MOBILE_REGEX = "^[789]\\d{9}$";
     //   String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        boolean result = true;

        String forget = forgot_edt.getText().toString().trim();
        if ((forget == null || forget.equals("")) || forget.length() != 10 || !forget.matches(MOBILE_REGEX))
        {
            forgot_til.setError("Invalid Mobile Number");
            result = false;
        }
        else
        {
            forgot_til.setErrorEnabled(false);
        }
        return result;
    }
}
