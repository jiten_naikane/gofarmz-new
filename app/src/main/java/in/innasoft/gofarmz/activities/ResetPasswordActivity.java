package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;


public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    EditText mobile_edt,otp_edt,new_password_edt;
    TextInputLayout otp_til,new_password_til;
    String user_forget_mobile;
    ProgressDialog progressDialog;
    Button reset_btn;
    private boolean checkInternet;
    CollapsingToolbarLayout ctl;
    AppBarLayout app_bar_layout;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));

        ctl = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
      //  ctl.setTitle("RESET PASSWORD");
        ctl.setExpandedTitleTypeface(typeface);
        ctl.setCollapsedTitleTextColor(Color.WHITE);


        ctl.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        ctl.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        app_bar_layout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        Bundle bundle = getIntent().getExtras();
        user_forget_mobile = bundle.getString("Mobile");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        otp_til=(TextInputLayout)findViewById(R.id.otp_til);
        new_password_til=(TextInputLayout)findViewById(R.id.new_password_til);

        mobile_edt=(EditText)findViewById(R.id.mobile_edt);
        mobile_edt.setTypeface(typeface);
        mobile_edt.setText(user_forget_mobile);

        otp_edt = (EditText)findViewById(R.id.otp_edt);
        otp_edt.setTypeface(typeface);
      /*  otp_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    app_bar_layout.setExpanded(false, true);
                }
            }
        });*/

        new_password_edt = (EditText)findViewById(R.id.new_password_edt);
        new_password_edt.setTypeface(typeface);
        /*new_password_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    app_bar_layout.setExpanded(false, true);
                }
            }
        });
*/
        reset_btn=(Button)findViewById(R.id.reset_btn);
        reset_btn.setTypeface(typeface);
        reset_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view)
    {
        if(view ==reset_btn)
        {
           if(validate())
           {
               checkInternet = NetworkChecking.isConnected(this);
               if(checkInternet)
               {
                   progressDialog.show();
                   Log.d("RESETURLURL", AppUrls.BASE_URL+ AppUrls.RESET_PASSWORD);

                   final String reset_otp=otp_edt.getText().toString().trim();
                   final String reset_pwd=new_password_edt.getText().toString().trim();

                   StringRequest strReset=new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.RESET_PASSWORD,
                           new Response.Listener<String>() {
                       @Override
                       public void onResponse(String response)
                       {
                           progressDialog.dismiss();
                           try
                           {
                               JSONObject jsonObject = new JSONObject(response);
                               Log.d("RESETRESP", response);
                               String editSuccessResponceCode = jsonObject.getString("status");

                               if( editSuccessResponceCode.equalsIgnoreCase("10100"))
                               {
                                   progressDialog.dismiss();

                                   Toast.makeText(getApplicationContext(),"Your Password changed successfully..!",Toast.LENGTH_SHORT).show();
                                   Intent intent=new Intent(ResetPasswordActivity.this, LoginActivity.class);
                                   intent.putExtra("activity_name",getIntent().getExtras().getString("activity_name"));
                                   startActivity(intent);
                               }

                               if(editSuccessResponceCode.equalsIgnoreCase("10200"))
                               {
                                   Toast.makeText(getApplicationContext()," Sorry, Try again.!",Toast.LENGTH_SHORT).show();
                               }

                               if(editSuccessResponceCode.equalsIgnoreCase("10300"))
                               {
                                   Toast.makeText(getApplicationContext()," Incorrect OTP..!",Toast.LENGTH_SHORT).show();
                               }

                               if(editSuccessResponceCode.equalsIgnoreCase("11786"))
                               {
                                   Toast.makeText(getApplicationContext(),"All fields are required..!",Toast.LENGTH_SHORT).show();
                               }

                           }
                           catch(JSONException e)
                           {
                                e.printStackTrace();
                           }
                       }
                       }, new Response.ErrorListener() {
                               @Override
                               public void onErrorResponse(VolleyError error) {

                                   progressDialog.dismiss();


                                   if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                                   } else if (error instanceof AuthFailureError) {
                                   } else if (error instanceof ServerError) {
                                   } else if (error instanceof NetworkError) {
                                   } else if (error instanceof ParseError) {
                                   }
                               }
                           })
                   {
                       @Override
                       protected Map<String, String> getParams() throws AuthFailureError {
                           Map<String, String> params = new HashMap<String, String>();
                           params.put("mobile", user_forget_mobile);
                           params.put("otp", reset_otp);
                           params.put("new_pwd", reset_pwd);
                           Log.d("RESETURLPARAM:",params.toString());
                           return params;
                       }
                   };
                   RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                   requestQueue.add(strReset);
               }
               else
               {
                   Snackbar snackbar = Snackbar.make(view, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                   snackbar.show();
               }

           }

        }

    }










    private boolean validate()
    {

        boolean result = true;
        String otp = otp_edt.getText().toString().trim();
        if ((otp == null || otp.equals("")) || otp.length() != 6)
        {
            otp_til.setError("Invalid OTP");
            result = false;
        }else
        {
            otp_til.setErrorEnabled(false);
        }

        String psw = new_password_edt.getText().toString().trim();
        if (psw.isEmpty()|| psw.length() < 6)
        {
            new_password_til.setError("Invalid Password");
            result = false;
        }else {
            new_password_til.setErrorEnabled(false);
        }


        return result;
    }


}
