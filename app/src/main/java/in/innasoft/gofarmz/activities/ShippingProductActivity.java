package in.innasoft.gofarmz.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapter.ShippingAdapter;
import in.innasoft.gofarmz.dbhelper.LocationControlar;
import in.innasoft.gofarmz.holder.ShippingPaymentHolder;
import in.innasoft.gofarmz.itemclicklistners.ShippingPaymentItemClickListener;
import in.innasoft.gofarmz.models.Pincode;
import in.innasoft.gofarmz.models.ShippingModel;
import in.innasoft.gofarmz.models.ShippingPaymentModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.GPSTracker;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;

public class ShippingProductActivity extends AppCompatActivity implements View.OnClickListener {

    TextView order_id, product_name_txt, quantity_txt, price_txt, final_price, billing_txt, country_txt, state_txt,
            city_txt, area_txt, payment_txt, currentLocation_text, changeLocation_text;
    TextInputLayout address_til, pincode_til;
    EditText address_edt, pincode_edt;
    Typeface typeface, typeface2;
    Button pay_btn;
    ImageView nodata_image;
    String user_id, user_name, user_email, user_phone, token, device_id;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    String gender = "", country_id = "", country_name = "", state_id = "", state_name = "",
            city_id = "", city_name = "", area_id = "", area_name = "", address = "", pincode = "";

    UserSessionManager session;

    RecyclerView shipping_recylerview;
    ArrayList<ShippingModel> shippingModels = new ArrayList<>();
    ShippingAdapter shippingAdapter;

    ArrayList<Pincode> pincodeArrayList = new ArrayList<>();

    //PAYMENT
    RecyclerView payment_recyclerview;
    ArrayList<ShippingPaymentModel> shippingPaymentModels = new ArrayList<>();
    ShippingPaymentAdapter shippingPaymentAdapter;

    String send_country_id = "";
    String send_state_id = "";
    String send_city_id = "";
    String send_area_id = "";
    String pincode_data = "";

    LocationControlar locationControlar;
    String payment_string = "", order_pid;
//    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;
//    String payu_base_url;
//    CartBucketDBHelper cartBucketDBHelper;
//    String send_product_id, send_product_price, send_quantity, send_product_name, txnid = "", hash;

    GPSTracker gpsTracker;
    double latitude, longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_product);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Shipping Order Summary");


        gpsTracker = new GPSTracker(getApplicationContext());

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_two));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        locationControlar = new LocationControlar(ShippingProductActivity.this);
        session = new UserSessionManager(getApplicationContext());
//        cartBucketDBHelper = new CartBucketDBHelper(ShippingProductActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        user_email = userDetails.get(UserSessionManager.USER_EMAIL);
        user_phone = userDetails.get(UserSessionManager.USER_MOBILE);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        order_id = findViewById(R.id.order_id);
        order_id.setTypeface(typeface);
        product_name_txt = findViewById(R.id.product_name_txt);
        quantity_txt = findViewById(R.id.quantity_txt);
        price_txt = findViewById(R.id.price_txt);
        final_price = findViewById(R.id.final_price);
        final_price.setTypeface(typeface);
        billing_txt = findViewById(R.id.billing_txt);
        billing_txt.setTypeface(typeface2);
        country_txt = findViewById(R.id.country_txt);
        country_txt.setTypeface(typeface);
        country_txt.setOnClickListener(this);
        state_txt = findViewById(R.id.state_txt);
        state_txt.setTypeface(typeface);
        state_txt.setOnClickListener(this);
        city_txt = findViewById(R.id.city_txt);
        city_txt.setTypeface(typeface);
        city_txt.setOnClickListener(this);
        area_txt = findViewById(R.id.area_txt);
        area_txt.setTypeface(typeface);
        area_txt.setOnClickListener(this);
        payment_txt = findViewById(R.id.payment_txt);
        payment_txt.setTypeface(typeface2);

        currentLocation_text = findViewById(R.id.currentLocation);
        currentLocation_text.setTypeface(typeface);
        changeLocation_text = findViewById(R.id.changeLocation);
        changeLocation_text.setTypeface(typeface);
        changeLocation_text.setOnClickListener(this);

        address_til = findViewById(R.id.address_til);
        address_til.setTypeface(typeface);
        pincode_til = findViewById(R.id.pincode_til);
        pincode_til.setTypeface(typeface);

        address_edt = findViewById(R.id.address_edt);
        address_edt.setTypeface(typeface);
        pincode_edt = findViewById(R.id.pincode_edt);
        pincode_edt.setTypeface(typeface);

        payment_recyclerview = findViewById(R.id.payment_recyclerview);

        pay_btn = findViewById(R.id.pay_btn);
        pay_btn.setTypeface(typeface);

        pay_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (address.contains("Hyderabad") || address.contains("hyderabad")) {
                    if (contains(pincodeArrayList, pincode)) {
                        if (payment_string.contains("Cash on Delivery")) {
                            getPaymentType(payment_string);
//                        Intent intent = new Intent(ShippingProductActivity.this, MainActivity.class);
//                        startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Please select payment type", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ShippingProductActivity.this);
                        alertDialogBuilder.setMessage("Currently we are not delivering to this Location.");
                        alertDialogBuilder.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int arg1) {
                                        dialog.dismiss();
                                    }
                                });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                } else {
                    restricDialog();
                }

//                if (txnid != null && !txnid.equalsIgnoreCase("null") && txnid.length() > 0) {
//
//                    if (address.contains("Hyderabad") || address.contains("hyderabad")) {
//
//                        if (shippingModels.size() > 1) {
//                            List<String> product_Id = cartBucketDBHelper.getCartProductId();
//                            send_product_id = product_Id.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");
//                            final List<String> product_Price = cartBucketDBHelper.getProductFinalPrice();
//                            send_product_price = product_Price.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");
//                            final List<String> Quantity = cartBucketDBHelper.getProductQuantity();
//                            send_quantity = Quantity.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");
//                            final List<String> product_Name = cartBucketDBHelper.getProductName();
//                            send_product_name = product_Name.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");
//
//                        }
//
//                        Intent intent = new Intent(ShippingProductActivity.this, PayMentGateWay.class);
//
//                        if (shippingModels.size() == 1) {
//
//                            intent.putExtra("product_id", shippingModels.get(0).getProduct_id());
//                            intent.putExtra("product_name", shippingModels.get(0).getProduct_name());
//                            intent.putExtra("product_price", shippingModels.get(0).getProduct_mrp_price());
////                        intent.putExtra("grand_total", "1");
//                            intent.putExtra("grand_total", shippingModels.get(0).getGrand_total());
//                            intent.putExtra("quantity", shippingModels.get(0).getProduct_quantity());
//                            intent.putExtra("paid_amount", shippingModels.get(0).getGrand_total());
//
//                        } else {
//
//                            intent.putExtra("product_id", send_product_id);
//                            intent.putExtra("product_name", send_product_name);
//                            intent.putExtra("product_price", send_product_price);
////                        intent.putExtra("grand_total", "1");
//                            intent.putExtra("grand_total", String.valueOf(cartBucketDBHelper.findGndTotalPrice()));
//                            intent.putExtra("quantity", send_quantity);
//                            intent.putExtra("paid_amount", String.valueOf(cartBucketDBHelper.findGndTotalPrice()));
//
//                        }
//
//                        intent.putExtra("user_id", user_id);
//                        intent.putExtra("name", user_name);
//                        intent.putExtra("email", user_email);
//                        intent.putExtra("mobile", user_phone);
//                        intent.putExtra("address", address);
//                        intent.putExtra("txnid", txnid);
//                        intent.putExtra("hash", hash);
//                        intent.putExtra("delevery_charges", "0");
//                        intent.putExtra("trasport_type", "empty");
//                        if (order_pid != null)
//                            intent.putExtra("order_pid", order_pid);
//
//                        startActivity(intent);
//
//                    } else {
//                        restricDialog();
//                    }
//                } else {
//                    Toast.makeText(getApplicationContext(), "Please select payment type", Toast.LENGTH_SHORT).show();
//                }
            }
        });

        nodata_image = findViewById(R.id.nodata_image);

        shipping_recylerview = findViewById(R.id.shipping_recylerview);
        shipping_recylerview.setHasFixedSize(true);
        shipping_recylerview.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(ShippingProductActivity.this);
        shipping_recylerview.setLayoutManager(layoutManager1);
        shippingAdapter = new ShippingAdapter(shippingModels, ShippingProductActivity.this, R.layout.row_shipping);

        payment_recyclerview = findViewById(R.id.payment_recyclerview);
        payment_recyclerview.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager2 = new LinearLayoutManager(ShippingProductActivity.this);
        payment_recyclerview.setLayoutManager(layoutManager2);
        shippingPaymentAdapter = new ShippingPaymentAdapter(shippingPaymentModels, ShippingProductActivity.this, R.layout.row_payment_shipping);

        getProfile();
        shippingDetails();
        getPincodes();
    }

    public boolean contains(ArrayList<Pincode> list, String pincode) {
        for (Pincode item : list) {
            if (item.getPincode().equals(pincode)) {
                return true;
            }
        }
        return false;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 123) {
            if (resultCode == Activity.RESULT_OK) {

                if (data.getStringExtra("searchPlace") != null && !data.getStringExtra("searchPlace").equalsIgnoreCase("Null")) {
                    String addresss = data.getStringExtra("searchPlace");
                    currentLocation_text.setText(addresss);
                    address_edt.setText(addresss);
                    address = addresss;
                    send_area_id = addresss;
                    send_country_id = "1";
                    send_state_id = "2";
                    send_city_id = "1";
                    latitude = gpsTracker.getLatitude();
                    longitude = gpsTracker.getLongitude();
                }
                if (data.getBooleanExtra("currentLocation", false)) {
                    if (gpsTracker != null) {
                        String addresss = getCompleteAddressString(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                        currentLocation_text.setText(addresss);
                        address_edt.setText(addresss);
                        address = addresss;
                        send_area_id = addresss;
                        Log.v("address", addresss);
                        send_country_id = "1";
                        send_state_id = "2";
                        send_city_id = "1";
                        latitude = gpsTracker.getLatitude();
                        longitude = gpsTracker.getLongitude();
                    }
                }
                if (data.getStringExtra("Country") != null && !data.getStringExtra("Country").equalsIgnoreCase("null")) {
                    send_country_id = data.getStringExtra("Country");
                    send_state_id = data.getStringExtra("State");
//                    send_city_id = data.getStringExtra("City");
                    send_city_id = "1";
                    send_area_id = data.getStringExtra("Area");
                    address_edt.setText(data.getStringExtra("Address"));
                    pincode_edt.setText(data.getStringExtra("Pincode"));
                    pincode = data.getStringExtra("Pincode");
                    currentLocation_text.setText(data.getStringExtra("Address"));
                    address = data.getStringExtra("Address");
                    latitude = data.getDoubleExtra("latitude", 0);
                    longitude = data.getDoubleExtra("longitude", 0);
                }

            }
//            if (resultCode == Activity.RESULT_CANCELED) {
//                //Write your code if there's no result
//            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    private void getProfile() {

        checkInternet = NetworkChecking.isConnected(ShippingProductActivity.this);

        if (checkInternet) {
            progressDialog.show();

//            Log.d("PROFILEURL", AppUrls.BASE_URL + AppUrls.USER_PROFILE);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.USER_PROFILE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            Log.d("PROFILERESPONCE", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("status");
                                if (successResponceCode.equals("10100")) {
                                    progressDialog.dismiss();

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

//                                    String id = jsonObject1.getString("id");
//                                    String name = jsonObject1.getString("name");
                                    gender = jsonObject1.getString("gender");
//                                    String email = jsonObject1.getString("email");
//                                    String mobile = jsonObject1.getString("mobile");
                                    country_id = jsonObject1.getString("country");
                                    send_country_id = country_id;
                                    country_name = jsonObject1.getString("country_name");
                                    if (country_name == null || country_name.equals("null") || country_name.equals("")) {
                                        country_txt.setText("Select Country");
                                    } else {
                                        country_txt.setText(country_name);
                                    }
                                    state_id = jsonObject1.getString("state");
                                    send_state_id = state_id;
                                    state_name = jsonObject1.getString("state_name");
                                    if (state_name == null || state_name.equals("null") || state_name.equals("")) {
                                        state_txt.setText("Select State");
                                    } else {
                                        state_txt.setText(state_name);
                                    }
                                    city_id = jsonObject1.getString("city");
                                    send_city_id = city_id;
                                    city_name = jsonObject1.getString("city_name");
                                    if (city_name == null || city_name.equals("null") || city_name.equals("")) {
                                        city_txt.setText("Select City");
                                    } else {
                                        city_txt.setText(city_name);
                                    }

                                    if (jsonObject1.has("area") && !jsonObject1.isNull("area")) {
                                        area_id = jsonObject1.getString("area");
                                    }
                                    if (jsonObject1.has("area_name") && !jsonObject1.isNull("area_name")) {
                                        area_name = jsonObject1.getString("area_name");
                                        send_area_id = area_name;
                                        if (area_name == null || area_name.equals("null") || area_name.equals("")) {
                                            area_txt.setText("Select Area");
                                        } else {
                                            area_txt.setText(area_name);
                                        }
                                    }

                                    if (jsonObject1.has("pincode") && !jsonObject1.isNull("pincode")) {
                                        pincode = jsonObject1.getString("pincode");
                                        pincode_edt.setText(pincode);
                                    }

                                    if (gpsTracker.getLatitude() != 0) {
                                        String address1 = getCompleteAddressString(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                                        currentLocation_text.setText(address1);
                                        address_edt.setText(address1);
                                        address = address1;
                                        send_area_id = address1;
                                        send_city_id = "1";
                                        send_country_id = "1";
                                        send_state_id = "2";
                                    }
                                }

                                if (successResponceCode.equals("12786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "No Data Found...!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("11786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("10786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
//                    Log.d("USERPROFILE:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
//                    Log.d("PROFILEHEADER", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }

    private void shippingDetails() {
        shippingModels.clear();
        shippingPaymentModels.clear();

        final_price.setText("Total : " + String.valueOf("\u20B9" + "" + getIntent().getExtras().getString("totalprice") + " /-"));
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SHIPPING_PRODUCT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            Log.d("ShippingResponce", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    progressDialog.cancel();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
//                                    JSONObject jsonObject2 = jsonObject1.getJSONObject("userData");
//                                    String name = jsonObject2.getString("name");
//                                    String gender = jsonObject2.getString("gender");
//                                    String email = jsonObject2.getString("email");
//                                    String mobile = jsonObject2.getString("mobile");
//                                    String country = jsonObject2.getString("country");
//                                    String state = jsonObject2.getString("state");
//                                    String city = jsonObject2.getString("city");
//                                    String area = jsonObject2.getString("area");
//                                    String address = jsonObject2.getString("address");
//                                    String pincode = jsonObject2.getString("pincode");

//                                    String currency = jsonObject1.getString("currency");

                                    JSONArray jsonArray = jsonObject1.getJSONArray("recordData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        ShippingModel ot = new ShippingModel();
                                        JSONObject jsonObject3 = jsonArray.getJSONObject(i);

                                        String id = jsonObject3.getString("id");
                                        ot.setId(id);
                                        String browser_id = jsonObject3.getString("browser_id");
                                        ot.setId(browser_id);
                                        String user_id = jsonObject3.getString("user_id");
                                        ot.setUser_id(user_id);
                                        String user_type = jsonObject3.getString("user_type");
                                        ot.setUser_type(user_type);
                                        String product_sku_id = jsonObject3.getString("product_sku_id");
                                        ot.setProduct_sku_id(product_sku_id);
                                        order_id.setText("Order Id : " + product_sku_id);
                                        String product_id = jsonObject3.getString("product_id");
                                        ot.setProduct_id(product_id);
                                        String product_name = jsonObject3.getString("product_name");
                                        ot.setProduct_name(product_name);
                                        String images = AppUrls.PRODUCTS_IMAGE_URL + jsonObject3.getString("images");
                                        ot.setImages(images);
                                        String product_count_id = jsonObject3.getString("product_count_id");
                                        ot.setProduct_count_id(product_count_id);
                                        String product_weight_id = jsonObject3.getString("product_weight_id");
                                        ot.setProduct_weight_id(product_weight_id);
                                        String product_weight = jsonObject3.getString("product_weight");
                                        ot.setProduct_weight(product_weight);
                                        String product_count = jsonObject3.getString("product_count");
                                        ot.setProduct_count(product_count);
                                        String product_quantity = jsonObject3.getString("product_quantity");
                                        ot.setProduct_quantity(product_quantity);
                                        String product_mrp_price = jsonObject3.getString("product_mrp_price");
                                        ot.setProduct_mrp_price(product_mrp_price);
                                        String purchase_quantity = jsonObject3.getString("purchase_quantity");
                                        ot.setPurchase_quantity(purchase_quantity);
                                        String total_price = jsonObject3.getString("total_price");
                                        ot.setTotal_price(total_price);
                                        String grand_total = jsonObject3.getString("grand_total");
                                        ot.setGrand_total(grand_total);
                                        // final_price.setText("Total Price   :"+"\t"+"Rs."+grand_total+"/-");
                                        String ip_address = jsonObject3.getString("ip_address");
                                        ot.setIp_address(ip_address);
                                        String status = jsonObject3.getString("status");
                                        ot.setStatus(status);
                                        String created_date = jsonObject3.getString("created_date");
                                        ot.setCreated_date(created_date);
                                        String update_date_time = jsonObject3.getString("update_date_time");
                                        ot.setUpdate_date_time(update_date_time);
                                        String url_name = jsonObject3.getString("url_name");
                                        ot.setUrl_name(url_name);
                                        String count_id = jsonObject3.getString("count_id");
                                        ot.setCount_id(count_id);
                                        String weight_id = jsonObject3.getString("weight_id");
                                        ot.setWeight_id(weight_id);
                                        String mrp_price = jsonObject3.getString("mrp_price");
                                        ot.setMrp_price(mrp_price);
                                        String sku_id = jsonObject3.getString("sku_id");
                                        ot.setSku_id(sku_id);
                                        String count_name = jsonObject3.getString("count_name");
                                        ot.setCount_name(count_name);
                                        String weight_name = jsonObject3.getString("weight_name");
                                        ot.setWeight_name(weight_name);
                                        String discount = jsonObject3.getString("discount");
                                        ot.setDiscount(discount);

                                        shippingModels.add(ot);
                                    }
                                    shipping_recylerview.setAdapter(shippingAdapter);
                                    JSONArray jsonArray1 = jsonObject1.getJSONArray("payment_gateway");

                                    for (int j = 0; j < jsonArray1.length(); j++) {
                                        ShippingPaymentModel pay = new ShippingPaymentModel();
                                        JSONObject jsonObject3 = jsonArray1.getJSONObject(j);

                                        pay.setId(jsonObject3.getString("id"));
                                        pay.setName(jsonObject3.getString("name"));
                                        pay.setLogo(AppUrls.GATEWAY_IMAGE_LOGO + jsonObject3.getString("logo"));
                                        pay.setField1(jsonObject3.getString("field1"));
                                        pay.setField2(jsonObject3.getString("field2"));
                                        pay.setField3(jsonObject3.getString("field3"));
                                        pay.setField4(jsonObject3.getString("field4"));
                                        pay.setStatus(jsonObject3.getString("status"));
                                        pay.setCreate_date_time(jsonObject3.getString("create_date_time"));
                                        pay.setUpdate_time_admin(jsonObject3.getString("update_time_admin"));

                                        shippingPaymentModels.add(pay);
                                    }

                                    payment_recyclerview.setAdapter(shippingPaymentAdapter);

                                }

                                if (responceCode.equals("12786")) {

                                    nodata_image.setVisibility(View.VISIBLE);
                                    Toast.makeText(ShippingProductActivity.this, "No Orders Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {

                                    Toast.makeText(ShippingProductActivity.this, "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10786")) {

                                    Toast.makeText(ShippingProductActivity.this, "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
//                    Log.d("SHIPPINGHEADER", headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("browserId", device_id);
//                    Log.d("ShippingParams:", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void getPincodes() {

        checkInternet = NetworkChecking.isConnected(ShippingProductActivity.this);

        if (checkInternet) {
            progressDialog.show();

            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.PINCODES,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Pincode response ", response);
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("status");
                                if (successResponceCode.equals("10100")) {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    pincodeArrayList = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        Pincode pincode = new Pincode();
                                        pincode.setId(jsonObject1.getString("id"));
                                        pincode.setArea(jsonObject1.getString("area"));
                                        pincode.setPincode(jsonObject1.getString("pincode"));
                                        pincode.setIs_active(jsonObject1.getString("is_active"));
                                        pincodeArrayList.add(pincode);
                                    }
                                }

                                if (successResponceCode.equals("12786")) {
                                    Toast.makeText(getApplicationContext(), "No Data Found...!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("11786")) {
                                    Toast.makeText(getApplicationContext(), "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("10786")) {
                                    Toast.makeText(getApplicationContext(), "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                if (addresses.get(0).getLocality().equalsIgnoreCase("Hyderabad")) {
                    Address returnedAddress = addresses.get(0);
                    StringBuilder strReturnedAddress = new StringBuilder("");
                    for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                    }
                    strAdd = strReturnedAddress.toString();
                }
                for (Address address : addresses) {
                    if (address.getLocality() != null && address.getPostalCode() != null) {
                        pincode_edt.setText(address.getPostalCode());
                        pincode = address.getPostalCode();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    private void restricDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Delivery available in Hyderabad location.");
        alertDialogBuilder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int arg1) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == changeLocation_text) {
            startActivityForResult(new Intent(getApplicationContext(), BillingAddressActivity.class), 123);
        }
    }

    public class ShippingPaymentAdapter extends RecyclerView.Adapter<ShippingPaymentHolder> {

        public ArrayList<ShippingPaymentModel> shipingPaymentModels;
        public ShippingProductActivity context;
        LayoutInflater li;
        int resource;
        int focusedItem = -1;

        public ShippingPaymentAdapter(ArrayList<ShippingPaymentModel> shipingPaymentModels, ShippingProductActivity context, int resource) {
            this.shipingPaymentModels = shipingPaymentModels;
            this.context = context;
            this.resource = resource;
            li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            String className = this.getClass().getCanonicalName();
//            Log.d("CURRENTCLASSNAME", className);

        }

        @Override
        public ShippingPaymentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layout = li.inflate(resource, parent, false);
            ShippingPaymentHolder slh = new ShippingPaymentHolder(layout);
            return slh;
        }

        @Override
        public void onBindViewHolder(@NonNull final ShippingPaymentHolder holder, final int position) {

            if (focusedItem == position) {
                holder.pay_gateway_name.setChecked(true);
            } else {
                holder.pay_gateway_name.setChecked(false);
            }

            holder.pay_gateway_name.setText(shipingPaymentModels.get(position).getName());
            holder.pay_gateway_name.setTag(shipingPaymentModels.get(position).getName());

            holder.pay_gateway_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.pay_gateway_name.isChecked()) {
                        focusedItem = position;
                        payment_string = shipingPaymentModels.get(position).getId() + "*" + shipingPaymentModels.get(position).getName();
                    }
                }
            });


            Picasso.with(context)
                    .load(shipingPaymentModels.get(position).getLogo())
                    .placeholder(R.drawable.nodata_image)
                    .into(holder.cod_logo);


            holder.setItemClickListener(new ShippingPaymentItemClickListener() {
                @Override
                public void onItemClick(View v, int pos) {

                }
            });
        }

        @Override
        public int getItemCount() {
            return this.shipingPaymentModels.size();
        }
    }

    private void getPaymentType(final String payment_string) {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST,
                    AppUrls.BASE_URL + AppUrls.PROCEED_CHECKOUT,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("OrdersResponce", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    progressDialog.dismiss();
                                    JSONObject data = jsonObject.getJSONObject("data");
                                    int order = data.getInt("order_pid");
                                    order_pid = String.valueOf(order);
                                    final String payment_gateway_id = data.getString("payment_gateway_id");
                                    final String payment_gateway_name = data.getString("payment_gateway_name");

                                    if (payment_string.contains("Cash on Delivery")) {
                                        final Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                getOrderBooking(order_pid, payment_gateway_id, payment_gateway_name);
                                            }
                                        }, 1000);
                                    } else {
                                        Toast.makeText(ShippingProductActivity.this, "Please Wait Redirecting to payment page.", Toast.LENGTH_SHORT).show();
                                    }

//                                    JSONObject jobjPayuData = data.getJSONObject("payUData");
//
//                                    payu_base_url = jobjPayuData.getString("payu_base_url");
////                                    final String merchant_keykey = jobjPayuData.getString("key");
////                                    final String salt = jobjPayuData.getString("salt");
//                                    hash = jobjPayuData.getString("hash");
//                                    txnid = jobjPayuData.getString("txnid");
////                                    final String amount = jobjPayuData.getString("amount");
//                                    final String firstname = jobjPayuData.getString("firstname");
//                                    final String emailAddress = jobjPayuData.getString("email");
//                                    final String phone = jobjPayuData.getString("phone");
//                                    final String productinfo = jobjPayuData.getString("productinfo");
//                                    final String surl = jobjPayuData.getString("surl");
//                                    final String furl = jobjPayuData.getString("furl");
//                                    final int merchant_id = jobjPayuData.getInt("id");


//                                    final String name = firstname;
//                                    final String mobile = phone;
//                                    final String email = emailAddress;
//                                    final String address = address_edt.getText().toString();


                                    //go to payment gateway.
                                    //gotoPayuMoneyPage(order_pid, payment_gateway_id, payment_gateway_name, payu_base_url, merchant_keykey, salt, hash, txnid, amount, firstname, email, phone, productinfo, surl, furl, merchant_id);

                                }

                                if (responceCode.equals("12786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ShippingProductActivity.this, "No Orders Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ShippingProductActivity.this, "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ShippingProductActivity.this, "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
//                    Log.d("ORDERHEADER", headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("browserId", device_id);
                    params.put("payment_gateway", payment_string);
                    params.put("country", send_country_id);
                    params.put("state", send_state_id);
                    params.put("city", send_city_id);
                    params.put("area", send_area_id);
                    params.put("address", currentLocation_text.getText().toString());
                    params.put("pincode", pincode_edt.getText().toString());
                    params.put("latitude", "" + latitude);
                    params.put("longitude", "" + longitude);
//                    Log.d("OrdersParams:", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    public void getOrderBooking(final String order_pid, final String payment_gateway_id, final String payment_gateway_name) {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            Log.d("sdjvfgsdbzhvsd", AppUrls.BASE_URL + AppUrls.UPDATE_PAYMENT_DATA);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.UPDATE_PAYMENT_DATA,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("OrdersResponcexgcfjhgh", response);
                            // Toast.makeText(ShippingProductActivity.this, ""+response, Toast.LENGTH_SHORT).show();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    progressDialog.dismiss();
                                    String message = jsonObject.getString("message");

                                    Intent intent = new Intent(ShippingProductActivity.this, SuccessActivity.class);
                                    intent.putExtra("order_pid", order_pid);
                                    intent.putExtra("message", message);
                                    startActivity(intent);
                                    finish();

//                                    Toast.makeText(ShippingProductActivity.this, message, Toast.LENGTH_SHORT).show();
//                                    Intent intent = new Intent(ShippingProductActivity.this, MainActivity.class);
//                                    startActivity(intent);
//                                    finish();
                                }

                                if (responceCode.equals("12786")) {

                                    progressDialog.dismiss();

                                    Toast.makeText(ShippingProductActivity.this, "No Orders Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(ShippingProductActivity.this, "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10786")) {
                                    progressDialog.dismiss();

                                    Toast.makeText(ShippingProductActivity.this, "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("browserId", device_id);
                    params.put("order_pid", order_pid);
                    params.put("payment_gateway_id", payment_gateway_id);
                    params.put("payment_gateway_name", payment_gateway_name);
                    Log.d("Orderfdghdfgdf:", params.toString());
                    // Toast.makeText(ShippingProductActivity.this, ""+params.toString(), Toast.LENGTH_SHORT).show();
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    Log.d("ORDERHEADER", headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }
}