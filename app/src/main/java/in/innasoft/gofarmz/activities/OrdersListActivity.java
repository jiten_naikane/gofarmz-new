package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapter.OrderListAdapter;
import in.innasoft.gofarmz.models.OrderListModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;

public class OrdersListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    ImageView nodata_image;
    RecyclerView orderlist_recylerview;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager session;
    String user_id, token;
    SwipeRefreshLayout swipe;
    String totalPrice = "0";
    HashMap<String, String> userDetails;
    /*Testimonial List*/
    OrderListAdapter orderListAdapter;
    ArrayList<OrderListModel> orderListModels = new ArrayList<OrderListModel>();
    ArrayList<String> orderList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Orders List");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe);

        swipe.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        swipe.setOnRefreshListener(this);

        nodata_image = (ImageView) findViewById(R.id.nodata_image);

        orderlist_recylerview = (RecyclerView) findViewById(R.id.orderlist_recylerview);
        orderlist_recylerview.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(OrdersListActivity.this);
        orderlist_recylerview.setLayoutManager(layoutManager);
        orderListAdapter = new OrderListAdapter(orderListModels, OrdersListActivity.this, R.layout.row_orders);
        if (token == null) {
            user_id = "0";
            getOrders(user_id);
        } else {
            user_id = userDetails.get(UserSessionManager.USER_ID);
            getOrders(user_id);
        }

    }

    public void getOrders(final String user_id) {
        orderListModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_ORDER_HISTORY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("OrdersResponce", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    progressDialog.cancel();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String currency = jsonObject1.getString("currency");
                                    String recordTotalCnt = jsonObject1.getString("recordTotalCnt");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("recordData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        OrderListModel cm = new OrderListModel();
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        if (!orderListModels.contains(jsonObject2.getString("reference_id"))) {
                                            cm.setId(jsonObject2.getString("id"));
                                            cm.setReference_id(jsonObject2.getString("reference_id"));
                                            cm.setOrder_id(jsonObject2.getString("order_id"));
                                            cm.setFinal_price(jsonObject2.getString("final_price"));
                                            cm.setOrder_status(jsonObject2.getString("order_status"));
                                            cm.setOrder_date(jsonObject2.getString("order_date"));
                                            cm.setOrder_time(jsonObject2.getString("order_time"));
                                            cm.setStatus(jsonObject2.getString("status"));
                                            cm.setImage(AppUrls.PRODUCTS_IMAGE_URL + jsonObject2.getString("images"));

                                            totalPrice += "" + jsonObject2.getString("final_price");

                                            orderListModels.add(cm);
                                        }
                                    }
                                    swipe.setRefreshing(false);
                                    orderlist_recylerview.setAdapter(orderListAdapter);
                                }

                                if (responceCode.equals("12786")) {
                                    swipe.setRefreshing(false);
                                    progressDialog.dismiss();
                                    nodata_image.setVisibility(View.VISIBLE);
                                    Toast.makeText(OrdersListActivity.this, "No Orders Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    progressDialog.dismiss();
                                    swipe.setRefreshing(false);
                                    Toast.makeText(OrdersListActivity.this, "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10786")) {
                                    progressDialog.dismiss();
                                    swipe.setRefreshing(false);
                                    Toast.makeText(OrdersListActivity.this, "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();
                    swipe.setRefreshing(false);

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    Log.d("ORDERHEADER", headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("page_no", "1");
                    Log.d("OrdersParams:", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

 @Override
    public void onRefresh() {
        if (token == null) {
            user_id = "0";
            getOrders(user_id);
        } else {
            user_id = userDetails.get(UserSessionManager.USER_ID);
            getOrders(user_id);
        }
    }

  /*     @Override
    protected void onResume() {
        super.onResume();
        if (token == null) {
            user_id = "0";
            getOrders(user_id);
        } else {
            user_id = userDetails.get(UserSessionManager.USER_ID);
            getOrders(user_id);
        }

    }*/
}
