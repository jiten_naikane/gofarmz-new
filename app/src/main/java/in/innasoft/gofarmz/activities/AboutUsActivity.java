package in.innasoft.gofarmz.activities;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.gofarmz.R;


public class AboutUsActivity extends AppCompatActivity {

    Typeface typeface, typeface2;
    TextView name_txt,version_txt,company_txt,company_website_txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_two));

        name_txt = (TextView) findViewById(R.id.name_txt);
        name_txt.setTypeface(typeface);
        version_txt = (TextView) findViewById(R.id.version_txt);
        version_txt.setTypeface(typeface);
        company_txt = (TextView) findViewById(R.id.company_txt);
        company_txt.setTypeface(typeface);
        company_website_txt = (TextView) findViewById(R.id.company_website_txt);
        company_website_txt.setTypeface(typeface);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("About Us");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
