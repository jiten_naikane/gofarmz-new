package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.florent37.materialtextfield.MaterialTextField;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    TextView forgot_txt, register_here_txt;
    EditText username_edt, password_edt;
    TextInputLayout userNameTil, passwordTil;
    Button login_btn;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager session;
    CollapsingToolbarLayout ctl;
    AppBarLayout app_bar_layout;
    Typeface typeface;
    String device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("DEVICEID:", device_id);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));

        ctl = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
//        ctl.setTitle("LOGIN");
        ctl.setExpandedTitleTypeface(typeface);
        ctl.setCollapsedTitleTextColor(Color.WHITE);

        ctl.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        ctl.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        app_bar_layout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        session = new UserSessionManager(getApplicationContext());

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        forgot_txt = (TextView) findViewById(R.id.forgot_txt);
        forgot_txt.setTypeface(typeface);
        forgot_txt.setOnClickListener(this);

        register_here_txt = (TextView) findViewById(R.id.register_here_txt);
        register_here_txt.setTypeface(typeface);
        register_here_txt.setOnClickListener(this);

        userNameTil = (TextInputLayout) findViewById(R.id.userNameTil);

        passwordTil = (TextInputLayout) findViewById(R.id.passwordTil);


        username_edt = (EditText) findViewById(R.id.username_edt);
        username_edt.setTypeface(typeface);

       /* username_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus)
                {
                    app_bar_layout.setExpanded(false, true);


                }
            }
        });*/


        password_edt = (EditText) findViewById(R.id.password_edt);
        password_edt.setTypeface(typeface);

       /* password_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    app_bar_layout.setExpanded(false, true);
                }
            }
        });
*/
        login_btn = (Button) findViewById(R.id.login_btn);
        login_btn.setTypeface(typeface);
        login_btn.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        if (view == login_btn) {
            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {
                    final String email = username_edt.getText().toString().trim();
                    final String psw = password_edt.getText().toString().trim();
                    progressDialog.show();
                    PackageInfo pInfo = null;
                    String version = null;
                    try {
                        pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                        version = pInfo.versionName;
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                    final String finalVersion = version;

                    Log.d("LOGINURL:", AppUrls.BASE_URL + AppUrls.LOGIN);
                    StringRequest strReqLogin = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGIN, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("LOGIN", response);
                                String editSuccessResponceCode = jsonObject.getString("status");

                                if (editSuccessResponceCode.equalsIgnoreCase("10100")) {

                                    progressDialog.dismiss();
                                    JSONObject jdata = jsonObject.getJSONObject("data");
                                    String jwt = jdata.getString("jwt");
                                    Log.d("JWT", jwt);
                                    String[] parts = jwt.split("\\.");
                                    byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                                    String decodedString = "";
                                    try {

                                        decodedString = new String(dataDec, "UTF-8");
                                        Log.d("TOKENSFSF", decodedString);
                                        JSONObject jsonObject2 = new JSONObject(decodedString);
                                        Log.d("JSONDATAsfa", jsonObject2.toString());
                                        JSONObject jsonObject3 = jsonObject2.getJSONObject("data");

                                        String user_id = jsonObject3.getString("user_id");
                                        String user_name = jsonObject3.getString("user_name");
                                        String email = jsonObject3.getString("email");
                                        String mobile = jsonObject3.getString("mobile");
                                        final String account_status = jsonObject3.getString("account_status");
                                        String browser_session_id = jsonObject3.getString("browser_session_id");

                                        Log.d("LOGINUSERDETAIL:", jwt + "//" + user_id + "//" + user_name + "//" + email + "//" + mobile + "//" + account_status + "//" + browser_session_id);


                                        session.createUserLoginSession(jwt, user_id, user_name, mobile, email, account_status, browser_session_id);
//                                        Toast.makeText(getApplicationContext(), "Your Account has been Successfully verified...!", Toast.LENGTH_SHORT).show();
                                        if (getIntent().getExtras().getString("activity_name").equals("splash")) {
                                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);

                                            if (account_status.equals("0")) {
                                                Intent intse = new Intent(LoginActivity.this, MainActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(intse);
                                            }
                                        } else {
                                            finish();
                                        }
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }

                                    Toast.makeText(getApplicationContext(), "You are Logged in Successfully...!", Toast.LENGTH_SHORT).show();
                                   /* Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);*/
                                }

                                if (editSuccessResponceCode.equalsIgnoreCase("10200")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Your account has been disabled...!", Toast.LENGTH_SHORT).show();
                                }
                                if (editSuccessResponceCode.equalsIgnoreCase("10300")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Login failed. Invalid username or password..!", Toast.LENGTH_SHORT).show();
                                }

                                if (editSuccessResponceCode.equalsIgnoreCase("11786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "All fields are required..!", Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {


                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("email", email);
                            params.put("password", psw);
                            params.put("browser_id", device_id);
                            Log.d("LoginREQUESTDATA:", params.toString());
                            return params;
                        }
                       /* @Override
                        public byte[] getBody() throws AuthFailureError
                        {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("email", email);
                            params.put("password", psw);
                            Log.d("LoginREQUESTDATA ", "PARAMS " + params.toString());
                            return new JSONObject(params).toString().getBytes();
                        }*/
                    };

                    strReqLogin.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                    requestQueue.add(strReqLogin);


                } else {
                    Snackbar snackbar = Snackbar.make(view, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }

        }
        if (view == register_here_txt) {
            Intent regIntent = new Intent(LoginActivity.this, RegisterActivity.class);
            regIntent.putExtra("activity_name", getIntent().getExtras().getString("activity_name"));
            startActivity(regIntent);

        }
        if (view == forgot_txt) {
            Intent regIntent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
            regIntent.putExtra("activity_name", getIntent().getExtras().getString("activity_name"));
            startActivity(regIntent);
        }

    }

    private boolean validate() {

        boolean result = true;

        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String username = username_edt.getText().toString().trim();
        String psw = password_edt.getText().toString().trim();
        if (!username.matches(EMAIL_REGEX) && psw.isEmpty() || psw.length() < 6) {
            //  passwordTil.setErrorEnabled(false);
            Toast.makeText(this, "Invalid Email and Password", Toast.LENGTH_SHORT).show();
        } else if (!username.matches(EMAIL_REGEX)) {
            Toast.makeText(this, "Invalid Email", Toast.LENGTH_SHORT).show();
            // userNameTil.setError("Invalid Email");
            result = false;
        } else if (psw.isEmpty() || psw.length() < 6) {
            Toast.makeText(this, "Invalid Password. Password Must contain minimum 6 Characters", Toast.LENGTH_SHORT).show();
            //  userNameTil.setErrorEnabled(false);
        }


        return result;
    }
}
