package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.UserSessionManager;

public class FailureActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView toolbar_tittle, failure_txt, failure_txt_title;
    Typeface typeface;
    UserSessionManager session;
    ProgressDialog progressDialog;
    String order_id, referenceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_failure);

        session = new UserSessionManager(getApplicationContext());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Failure");


        typeface = Typeface.createFromAsset(this.getAssets(), "museosanscyrl.ttf");
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        progressDialog.setCancelable(false);


        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        toolbar_tittle = (TextView) findViewById(R.id.toolbar_tittle);
        toolbar_tittle.setTypeface(typeface);
        failure_txt = (TextView) findViewById(R.id.failure_txt);
        failure_txt_title = (TextView) findViewById(R.id.failure_txt_title);
        failure_txt.setTypeface(typeface);
        failure_txt_title.setTypeface(typeface);

        Bundle bundle = getIntent().getExtras();

        order_id = bundle.getString("order_pid");
        referenceId = bundle.getString("referenceId");

        progressDialog.show();
        String url = AppUrls.BASE_URL + "payu_failure?order_id=" + order_id + "&reference_id_app=" + referenceId;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("success after pay ", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("FULLRESPONCE", response);
                            if (jsonObject.getBoolean("success")) {
                                final String message = jsonObject.getString("message");
                                progressDialog.dismiss();
                                failure_txt.setText(message);

                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        failure_txt.setText(message);
                                    }
                                });
                            }


                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
           /* @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
               *//* Log.d("SENDINGVALUESDSSDS", user_id+"\n"+product_id+"\n"+product_price+"\n"+quantity+"\n"+name+"\n"+email+"\n"+mobile+"\n"+address+"\n"+grand_total+"\n"+delevery_charges+"\n"+trasport_type+"\n"+paid_amount+"\n10"+paymentId);
                params.put("user_id", user_id);
                params.put("product_id", product_id);
                params.put("product_price", product_price);
                params.put("quantity", quantity);
                params.put("name", name);
                params.put("email", email);
                params.put("mobile", mobile);
                params.put("address", address);
                params.put("grand_total", grand_total);
                params.put("delevery_charges", delevery_charges);
                params.put("trasport_type", trasport_type);
                params.put("paid_amount", paid_amount);
                params.put("transaction_charges", "0");
                params.put("trasaction_id", paymentId);*//*
                return params;
            }*/
        };
        RequestQueue requestQueue = Volley.newRequestQueue(FailureActivity.this);
        requestQueue.add(stringRequest);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == close) {
                Intent i = new Intent(FailureActivity.this, MainActivity.class);
                startActivity(i);
                finish();
        }
    }

    @Override
    public void onBackPressed() {
            Intent i = new Intent(FailureActivity.this, MainActivity.class);
            startActivity(i);
            finish();
    }
}
