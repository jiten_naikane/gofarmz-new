package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapter.HomeCityAdapter;
import in.innasoft.gofarmz.models.CityModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.CitySelectionSession;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;

public class  LocationDialogActivity extends AppCompatActivity implements View.OnClickListener {

    UserSessionManager session;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    ArrayList<CityModel> homecityModels = new ArrayList<CityModel>();
    ArrayList<String> citi_list = new ArrayList<String>();
    HomeCityAdapter homecityAdapter;
    CitySelectionSession citySelectionSession;
    Typeface typeface;
    TextView home_city_txt;
    ImageView close;
    public String user_id,user_name,user_email,user_phone,profile_pic_url,login_type,sendCityName,CityID,latitute,longitute, notification_status = "0";
    public String disp_city_name = "", disp_city_id, disp_lat, disp_lng;
    AlertDialog dialog,dialog2;
    RecyclerView home_city_recyclerview;
    SearchView home_city_search;
    LinearLayoutManager  layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_dialog);
        session = new UserSessionManager(getApplicationContext());

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));

        home_city_txt = (TextView) findViewById(R.id.home_city_txt);
        home_city_txt.setTypeface(typeface);

        citySelectionSession = new CitySelectionSession(getApplicationContext());

        if(citySelectionSession.checkCityCreated() == false){
            HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
            disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
            disp_city_name = cityDetails.get(CitySelectionSession.CITY_NAME);
            disp_lng = cityDetails.get(CitySelectionSession.CITY_LAT);
            disp_lng = cityDetails.get(CitySelectionSession.CITY_LNG);
            try{
                Log.d("FIIINDCITYLENGTH", disp_city_name);
                home_city_txt.setText(disp_city_name);
            }catch (NullPointerException e){
                String msg = (e.getMessage()==null)?"Login failed!":e.getMessage();
                Log.i("Login Error1",msg);
            }
        }else {
            home_city_txt.setText("Select City");
            //Toast.makeText(this, "Select City", Toast.LENGTH_SHORT).show();
        }



        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);


        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);

        home_city_search = (SearchView) findViewById(R.id.home_city_search);
        home_city_search.setQueryHint("search your City...");

        home_city_search.setIconifiedByDefault(true);
        home_city_search.setFocusable(true);
        home_city_search.setIconified(false);

        //home_city_search.requestFocusFromTouch();

        EditText searchEditText = (EditText)home_city_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(Color.parseColor("#000000"));
        searchEditText.setHintTextColor(Color.parseColor("#d3d3d3"));
        searchEditText.setTypeface(typeface);

        home_city_recyclerview = (RecyclerView) findViewById(R.id.home_city_recyclerview);
        home_city_recyclerview.setHasFixedSize(true);
       // home_city_recyclerview.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2, GridLayoutManager.VERTICAL, false));
        layoutManager = new LinearLayoutManager(this);
        home_city_recyclerview.setLayoutManager(layoutManager);
        homecityAdapter = new HomeCityAdapter(homecityModels, LocationDialogActivity.this, R.layout.row_home_city_list);
        home_city_recyclerview.setNestedScrollingEnabled(false);
        home_city_recyclerview.setSaveFromParentEnabled(true);
        home_city_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home_city_search.setIconified(false);
            }
        });


        home_city_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                homecityAdapter.getFilter().filter(query);
                return false;
            }
        });
        getCityData();

    }

    private void getCityData()
    {

        checkInternet = NetworkChecking.isConnected(LocationDialogActivity.this);
        if (checkInternet) {

            homecityModels.clear();
            progressDialog.show();

            StringRequest string_request = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOCATION,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            String repo = response.toString();
                            Log.e("RESPCITY", "RESPCITY:" + repo);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("Citites:", response);

                                String responceCode = jsonObject.getString("status");

                                if (responceCode.equals("10100"))
                                {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++)
                                    {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        CityModel pcm = new CityModel();
                                        pcm.setCityId(jsonObject1.getString("id"));
                                        String city_name = jsonObject1.getString("name");
                                       // String lat = jsonObject1.getString("lat");
                                     //   String longi = jsonObject1.getString("lang");
                                        pcm.setCityName(city_name);
                                    //    pcm.setLatitute(lat);
                                    //    pcm.setLognigtute(longi);
                                    //    pcm.setCity_image(AppUrls.BASE_URL+""+jsonObject1.getString("city_image"));
                                        citi_list.add(city_name);
                                        homecityModels.add(pcm);
                                    }
                                    home_city_recyclerview.setAdapter(homecityAdapter);

                                    //   cityDialog();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Error:" + error, Toast.LENGTH_LONG).show();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                    } else if (error instanceof AuthFailureError) {
                        //TODO
                    } else if (error instanceof ServerError) {
                        //TODO
                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {
                        //TODO
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(LocationDialogActivity.this);
            requestQueue.add(string_request);
        }else {
            Toast.makeText(LocationDialogActivity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public void setCityName(String cityName,String city_id,String latitut,String lognitute)
    {

        dialog2.dismiss();
        homecityAdapter.notifyDataSetChanged();
        sendCityName = cityName;
        CityID=city_id;
        latitute=latitut;
        longitute=lognitute;
        citySelectionSession.createCity(city_id, cityName);
        Log.d("CITYDETAIL:",sendCityName+"::::"+CityID+":::"+latitute+":::"+longitute);
        //   homepage_city.setText(sendCityName);
    }


    @Override
    public void onClick(View v)
    {
      if(v==close)
       {
         finish();
      }
    }

    @Override
    public void onBackPressed()
    {

        Toast.makeText(this, "Please Select City", Toast.LENGTH_SHORT).show();

        /* if(home_city_txt.equals("Select City"))
         {

         }
         else
             {
               Intent main=new Intent(AlertCityActivity.this,MainActivity.class);
                 startActivity(main);
             }*/

    }
}
