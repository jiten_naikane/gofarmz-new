package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapter.OrderTrackAdapter;
import in.innasoft.gofarmz.adapter.OrderViewAdapter;
import in.innasoft.gofarmz.models.OrderTrackingModel;
import in.innasoft.gofarmz.models.OrderViewModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;

public class OrderViewActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    ImageView nodata_image;
    RecyclerView vieworder_recylerview, track_order_recylerview;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager session;
    String user_id, token, orders_pid;
    Typeface typeface, typeface2;
    /*Testimonial List*/
    OrderViewAdapter orderViewAdapter;
    OrderTrackAdapter ordertrackAdapter;
    ArrayList<OrderViewModel> orderViewModels = new ArrayList<>();
    ArrayList<OrderTrackingModel> ordertrackingmodel = new ArrayList<>();
//    ArrayList<String> orderViewList = new ArrayList<>();
    String id, reference_id, order_id, final_price, order_status, order_date, order_time, status;
    TextView order_id_text, final_price_text, delivery_address_text_title, delivery_address, trackiing_text_title, order_details_text, order_ref_id, order_date_text, product_details_text;
    Button cancel_order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_view);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Order Details");

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_two));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            orders_pid = bundle.getString("orders_pid");

        nodata_image = findViewById(R.id.nodata_image);
        order_id_text = findViewById(R.id.order_id);
        order_id_text.setTypeface(typeface);
        final_price_text = findViewById(R.id.final_price);
        final_price_text.setTypeface(typeface);

        delivery_address_text_title = findViewById(R.id.delivery_address_text_title);
        delivery_address_text_title.setTypeface(typeface2);

        order_details_text = findViewById(R.id.order_details_text);
        order_details_text.setTypeface(typeface2);

        product_details_text = findViewById(R.id.product_details_text);
        product_details_text.setTypeface(typeface2);

        order_date_text = findViewById(R.id.order_date);
        order_date_text.setTypeface(typeface);

        order_ref_id = findViewById(R.id.order_ref_id);
        order_ref_id.setTypeface(typeface);

        delivery_address = findViewById(R.id.delivery_address);
        delivery_address.setTypeface(typeface);

        trackiing_text_title = findViewById(R.id.trackiing_text_title);
        trackiing_text_title.setTypeface(typeface2);

        cancel_order = findViewById(R.id.cancel_order);
        cancel_order.setTypeface(typeface);
        cancel_order.setVisibility(View.GONE);

        cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ordertrackingmodel.clear();
                getCancelOrder();
            }
        });

        vieworder_recylerview = findViewById(R.id.vieworder_recylerview);
        // vieworder_recylerview.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(OrderViewActivity.this);
        vieworder_recylerview.setLayoutManager(layoutManager);
        vieworder_recylerview.setNestedScrollingEnabled(false);
        orderViewAdapter = new OrderViewAdapter(orderViewModels, OrderViewActivity.this, R.layout.row_vieworder);

        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(OrderViewActivity.this, R.drawable.divider));
        RecyclerView.ItemDecoration dividerItemDecoration1 = new DividerItemDecorator(ContextCompat.getDrawable(OrderViewActivity.this, R.drawable.divider));
        vieworder_recylerview.addItemDecoration(dividerItemDecoration);


        track_order_recylerview = findViewById(R.id.track_order_recylerview);
        //track_order_recylerview.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(OrderViewActivity.this);
        track_order_recylerview.setLayoutManager(layoutManager1);
        track_order_recylerview.setNestedScrollingEnabled(false);
        ordertrackAdapter = new OrderTrackAdapter(ordertrackingmodel, OrderViewActivity.this, R.layout.row_track_order);
        track_order_recylerview.addItemDecoration(dividerItemDecoration1);


        orderDetails();
    }

    private void orderDetails() {
        orderViewModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_ORDER_TRACK,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("OrdersResponce", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    progressDialog.cancel();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONObject order_info = jsonObject1.getJSONObject("order_info");
                                    id = order_info.getString("id");
                                    reference_id = order_info.getString("reference_id");
                                    order_ref_id.setText("ORDER REF ID - " + reference_id);
                                    order_id = order_info.getString("order_id");
                                    order_id_text.setText("ORDER ID - " + order_id);
                                    final_price = order_info.getString("final_price");
                                    final_price_text.setText("Final Price : \u20B9" + final_price + " /-");
                                    order_status = order_info.getString("order_status");
                                    order_date = order_info.getString("order_date");
                                    order_date_text.setText("ORDER DATE - " + order_date);
                                    order_time = order_info.getString("order_time");

                                    String name = order_info.getString("name");
                                    String mobile = order_info.getString("mobile");
                                    String country = order_info.getString("country");
                                    String state = order_info.getString("state");
                                    String city = order_info.getString("city");
                                    String area = order_info.getString("area");
                                    String address = order_info.getString("address");
                                    String pincode = order_info.getString("pincode");

                                    delivery_address.setText(name + "\n" + mobile + "\n" + address + ", " + area + "\n" + city + ", " + state + "\n" + country + "\n" + "Pincode : " + pincode);

                                    status = order_info.getString("status");
//                                    if (status.equals("1")) {
//
//                                    }
                                    if (status.equals("3")) {
                                        cancel_order.setVisibility(View.GONE);
                                    }
                                    String currency = jsonObject1.getString("currency");

                                    JSONArray jsonArray = jsonObject1.getJSONArray("products_info");
                                    if (!order_status.equalsIgnoreCase("Failed")) {
                                        JSONArray track_info_array = jsonObject1.getJSONArray("track_info");
                                        if (track_info_array.length() > 0) {
                                            track_order_recylerview.setVisibility(View.VISIBLE);
                                            trackiing_text_title.setVisibility(View.VISIBLE);
                                            for (int j = 0; j < track_info_array.length(); j++) {
                                                OrderTrackingModel ot = new OrderTrackingModel();
                                                JSONObject jsonObject_track = track_info_array.getJSONObject(j);
                                                ot.setOrder_status(jsonObject_track.getString("order_status"));
                                                ot.setMessage(jsonObject_track.getString("message"));
                                                ot.setCreate_date_time(jsonObject_track.getString("create_date_time"));
                                                ordertrackingmodel.add(ot);
                                            }
                                            track_order_recylerview.setAdapter(ordertrackAdapter);
                                        }
                                    } else {
                                        track_order_recylerview.setVisibility(View.GONE);
                                        trackiing_text_title.setVisibility(View.GONE);
                                    }
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        OrderViewModel cm = new OrderViewModel();
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                                        cm.setProduct_sku_id(jsonObject2.getString("product_sku_id"));
                                        cm.setProduct_id(jsonObject2.getString("product_id"));
                                        cm.setProduct_name(jsonObject2.getString("product_name"));
                                        cm.setWeight_name(jsonObject2.getString("weight_name"));
                                        cm.setCount_name(jsonObject2.getString("count_name"));
                                        cm.setImages(AppUrls.PRODUCTS_IMAGE_URL + jsonObject2.getString("images"));
                                        cm.setProduct_mrp_price(jsonObject2.getString("product_mrp_price"));
                                        cm.setPurchase_quantity(jsonObject2.getString("purchase_quantity"));
                                        cm.setTotal_price(jsonObject2.getString("total_price"));
                                        cm.setGrand_total(jsonObject2.getString("grand_total"));
                                        orderViewModels.add(cm);
                                    }

                                    vieworder_recylerview.setAdapter(orderViewAdapter);

                                }

                                if (responceCode.equals("12786")) {

                                    nodata_image.setVisibility(View.VISIBLE);
                                    Toast.makeText(OrderViewActivity.this, "No Orders Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {

                                    Toast.makeText(OrderViewActivity.this, "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10786")) {

                                    Toast.makeText(OrderViewActivity.this, "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    Log.d("ORDERHEADER", headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("orders_pid", getIntent().getExtras().getString("orders_pid"));
                    Log.d("OrdersParams:", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_cancel_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.cancel_order:


                getCancelOrder();


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getCancelOrder() {

        checkInternet = NetworkChecking.isConnected(OrderViewActivity.this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_ORDER_CANCEL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("OrdersResponce", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    progressDialog.cancel();
                                    Toast.makeText(OrderViewActivity.this, "Your order has been cancelled..!", Toast.LENGTH_SHORT).show();
                                    orderDetails();
                                }

                                if (responceCode.equals("10200")) {
                                    progressDialog.cancel();
                                    Toast.makeText(OrderViewActivity.this, "Sorry, Try Again..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    progressDialog.cancel();
                                    Toast.makeText(OrderViewActivity.this, "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10786")) {
                                    progressDialog.cancel();
                                    Toast.makeText(OrderViewActivity.this, "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    Log.d("ORDERCANCELHEADER", headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("orders_pid", id);
                    Log.d("OrdersCancel", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(OrderViewActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(OrderViewActivity.this.getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public void onRefresh() {
        orderDetails();
    }
}
