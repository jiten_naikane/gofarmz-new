package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;


public class AccountVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    EditText otp_edt, mobile_edt;
    Button resend_txt, submit_btn;
    TextInputLayout mobile_til, otp_til;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String user_reg_mobile, device_id;
    ProgressDialog pprogressDialog;
    CollapsingToolbarLayout ctl;
    AppBarLayout app_bar_layout;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verification);
        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("DEVICEID:", device_id);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));

        ctl = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        ctl.setExpandedTitleTypeface(typeface);
        ctl.setCollapsedTitleTextColor(Color.WHITE);
        ctl.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        ctl.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        app_bar_layout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        userSessionManager = new UserSessionManager(getApplicationContext());

        Bundle bundle = getIntent().getExtras();
        user_reg_mobile = bundle.getString("mobile");

        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Verifying wait...");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        mobile_til = (TextInputLayout) findViewById(R.id.mobile_til);
        mobile_til.setTypeface(typeface);
        otp_til = (TextInputLayout) findViewById(R.id.otp_til);

        mobile_edt = (EditText) findViewById(R.id.mobile_edt);
        mobile_edt.setTypeface(typeface);
        mobile_edt.setText(user_reg_mobile);

        otp_edt = (EditText) findViewById(R.id.otp_edt);
        otp_edt.setTypeface(typeface);

        resend_txt = (Button) findViewById(R.id.resend_txt);
        resend_txt.setTypeface(typeface);
        resend_txt.setOnClickListener(this);

        submit_btn = (Button) findViewById(R.id.submit_btn);
        submit_btn.setTypeface(typeface);
        submit_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if (view == resend_txt) {
            checkInternet = NetworkChecking.isConnected(this);

            if (checkInternet) {
                pprogressDialog.show();
                Log.d("RESENDURL:", AppUrls.BASE_URL + AppUrls.RESEND_OTP);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.RESEND_OTP,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("RESENDOTPRESP:", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successResponceCode = jsonObject.getString("status");
                                    if (successResponceCode.equals("10100 ")) {
                                        pprogressDialog.dismiss();

                                        Toast.makeText(getApplicationContext(), "New OTP Sent Successfully", Toast.LENGTH_SHORT).show();

                                    }
                                    if (successResponceCode.equals("10200")) {
                                        pprogressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                                    }

                                    if (successResponceCode.equals("10300")) {
                                        pprogressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Incorrect Mobile No....!", Toast.LENGTH_SHORT).show();
                                    }
                                    if (successResponceCode.equals("11786")) {
                                        pprogressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "All fields are required...!", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    pprogressDialog.cancel();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pprogressDialog.cancel();

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("mobile", user_reg_mobile);
                        Log.d("RESENDOTPPARAM:", params.toString());
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(AccountVerificationActivity.this);
                requestQueue.add(stringRequest);

            } else {
                Snackbar snackbar = Snackbar.make(view, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
        if (view == submit_btn) {
            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {
                    final String otp = otp_edt.getText().toString().trim();
                    pprogressDialog.show();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ACCOUNT_VERIFICATION,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String successResponceCode = jsonObject.getString("status");
                                        if (successResponceCode.equals("10100")) {

                                            pprogressDialog.dismiss();
                                            JSONObject jdata = jsonObject.getJSONObject("data");
                                            String jwt = jdata.getString("jwt");

                                            String[] parts = jwt.split("\\.");
                                            byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                                            String decodedString = "";
                                            try {

                                                decodedString = new String(dataDec, "UTF-8");
                                                Log.d("TOKENSFSF", decodedString);
                                                JSONObject jsonObject2 = new JSONObject(decodedString);
                                                Log.d("JSONDATAsfa", jsonObject2.toString());
                                                JSONObject jsonObject3 = jsonObject2.getJSONObject("data");

                                                String user_id = jsonObject3.getString("user_id");
                                                String user_name = jsonObject3.getString("user_name");
                                                String email = jsonObject3.getString("email");
                                                String mobile = jsonObject3.getString("mobile");
                                                String account_status = jsonObject3.getString("account_status");
                                                String browser_session_id = jsonObject3.getString("browser_session_id");

                                                Log.d("USERDETAIL:", user_id + "//" + user_name + "//" + email + "//" + mobile + "//" + account_status + "//" + browser_session_id);

                                                userSessionManager.createUserLoginSession(jwt, user_id, user_name, mobile, email, account_status, browser_session_id);
                                                Toast.makeText(getApplicationContext(), "Your Account has been Successfully verified...!", Toast.LENGTH_SHORT).show();
                                                if (getIntent().getExtras().getString("activity_name").equals("splash")) {
                                                    Intent intent = new Intent(AccountVerificationActivity.this, MainActivity.class);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(intent);

                                                    if (account_status.equals("0")) {
                                                        Intent inte = new Intent(AccountVerificationActivity.this, LoginActivity.class);
                                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                        startActivity(inte);
                                                    }
                                                } else {
                                                    finish();
                                                }
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        if (successResponceCode.equals("10200")) {
                                            pprogressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                                        }
                                        if (successResponceCode.equals("10300 ")) {
                                            pprogressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Incorrect OTP...!", Toast.LENGTH_SHORT).show();
                                        }
                                        if (successResponceCode.equals("11786")) {
                                            pprogressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "All fields are required...!", Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("otp", otp);
                            params.put("mobile", user_reg_mobile);
                            params.put("browser_id", device_id);
                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(AccountVerificationActivity.this);
                    requestQueue.add(stringRequest);

                } else {
                    Snackbar snackbar = Snackbar.make(view, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }

        }

    }

    private boolean validate() {

        boolean result = true;
        String otp = otp_edt.getText().toString().trim();
        if ((otp == null || otp.equals("")) || otp.length() != 6) {
            otp_til.setError("Invalid OTP");
            result = false;
        } else {
            otp_til.setErrorEnabled(false);
        }


        String mobile = mobile_edt.getText().toString().trim();
        if ((mobile == null || mobile.equals("")) || mobile.length() != 10) {
            mobile_til.setError("Invalid Mobile Number");
            result = false;
        } else {
            mobile_til.setErrorEnabled(false);
        }
        return result;
    }


}
