package in.innasoft.gofarmz.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapter.PlaceAutocompleteAdapter;
import in.innasoft.gofarmz.utilities.CustomMapFragment;
import in.innasoft.gofarmz.utilities.GPSTracker;
import in.innasoft.gofarmz.utilities.MapWrapperLayout;
import in.innasoft.gofarmz.utilities.UserSessionManager;

public class BillingAddressActivity extends AppCompatActivity implements View.OnClickListener, MapWrapperLayout.OnDragListener, GoogleApiClient.OnConnectionFailedListener {

    Button buttonSubmit;
    String send_country_id = "";
    String send_state_id = "";
    String send_city_id = "";
    String send_area_id = "";
    String pinCode = "";
    String token;

    Typeface typeface;

    //Google Maps Code to fetch address

    private GoogleMap googleMap;
    private CustomMapFragment mCustomMapFragment;

    private View mMarkerParentView;
    private ImageView mMarkerImageView;

    private int imageParentWidth = -1;
    private int imageParentHeight = -1;
    private int imageHeight = -1;
    private int centerX = -1;
    private int centerY = -1;

    private TextView mLocationTextView;
    private AutoCompleteTextView mAutocompleteView;
    PlaceAutocompleteAdapter mAdapter;
    private GoogleApiClient mGoogleApiClient;
    private double mLatitude, mLongitude;
    GPSTracker gpsTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.billing_address_activity);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Select Address");

        UserSessionManager session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));
        gpsTracker = new GPSTracker(getApplicationContext());

        buttonSubmit = (Button) findViewById(R.id.buttonSubmit);

        mCustomMapFragment = ((CustomMapFragment) getFragmentManager().findFragmentById(R.id.map));
        mLocationTextView = (TextView) findViewById(R.id.location_text_view);
        mMarkerParentView = findViewById(R.id.marker_view_incl);
        mMarkerImageView = (ImageView) findViewById(R.id.marker_icon_view);
        mAutocompleteView = (AutoCompleteTextView) findViewById(R.id.googleplacesearch);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0 /* clientId */, this)
                .addApi(Places.GEO_DATA_API)
                .build();
        mAdapter = new PlaceAutocompleteAdapter(this, R.layout.google_places_search_items, mGoogleApiClient, null, null);
        mAutocompleteView.setAdapter(mAdapter);
        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);

        mCustomMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap gMap) {
                googleMap = gMap;

                if (ActivityCompat.checkSelfPermission(BillingAddressActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(BillingAddressActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(true);

                LatLng currentLocation = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15f));
                updateLocation(currentLocation);

                mCustomMapFragment.setOnDragListener(BillingAddressActivity.this);

            }
        });

        buttonSubmit.setOnClickListener(this);
    }

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {

        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);
            hideKeyboard();
            mLatitude = (place.getLatLng().latitude);
            mLongitude = (place.getLatLng().longitude);
            LatLng newLatLngTemp = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
            // LatLng centerLatLng=new LatLng(mMap.getCameraPosition().target.latitude,mMap.getCameraPosition().target.longitude);
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(newLatLngTemp, 15f));
            updateLocation(newLatLngTemp);
        }
    };

    public void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            final PlaceAutocompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);


            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
        }
    };

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        imageParentWidth = mMarkerParentView.getWidth();
        imageParentHeight = mMarkerParentView.getHeight();
        imageHeight = mMarkerImageView.getHeight();

        centerX = imageParentWidth / 2;
        centerY = (imageParentHeight / 2) + (imageHeight / 2);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonSubmit:
                Intent returnIntent = getIntent();
                send_country_id = "1";
                send_state_id = "2";
                send_city_id = "1";
                returnIntent.putExtra("Country", send_country_id);
                returnIntent.putExtra("State", send_state_id);
                returnIntent.putExtra("City", send_city_id);
                returnIntent.putExtra("Area", send_area_id);
                returnIntent.putExtra("Address", send_area_id);
                returnIntent.putExtra("Pincode", pinCode);
                returnIntent.putExtra("latitude", mLatitude);
                returnIntent.putExtra("longitude", mLongitude);

                setResult(RESULT_OK, returnIntent);
                finish();
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDrag(MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            Projection projection = (googleMap != null && googleMap
                    .getProjection() != null) ? googleMap.getProjection()
                    : null;
            //
            if (projection != null) {
                LatLng centerLatLng = projection.fromScreenLocation(new Point(
                        centerX, centerY));
                updateLocation(centerLatLng);
            }
        }
    }

    private void updateLocation(LatLng centerLatLng) {
        if (centerLatLng != null) {
            Geocoder geocoder = new Geocoder(BillingAddressActivity.this,
                    Locale.getDefault());

            List<Address> addresses = new ArrayList<Address>();
            try {
                addresses = geocoder.getFromLocation(centerLatLng.latitude,
                        centerLatLng.longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addresses != null && addresses.size() > 0) {

                String addressIndex0 = (addresses.get(0).getAddressLine(0) != null) ? addresses
                        .get(0).getAddressLine(0) : null;
//                String addressIndex1 = (addresses.get(0).getAddressLine(1) != null) ? addresses
//                        .get(0).getAddressLine(1) : null;
//                String addressIndex2 = (addresses.get(0).getAddressLine(2) != null) ? addresses
//                        .get(0).getAddressLine(2) : null;
//                String addressIndex3 = (addresses.get(0).getAddressLine(3) != null) ? addresses
//                        .get(0).getAddressLine(3) : null;

                String completeAddress = addressIndex0;

//                if (addressIndex2 != null && !addressIndex2.equalsIgnoreCase("null")) {
//                    completeAddress += "," + addressIndex2;
//                }
//                if (addressIndex3 != null && !addressIndex3.equalsIgnoreCase("null")) {
//                    completeAddress += "," + addressIndex3;
//                }
                if (completeAddress != null) {
                    mLocationTextView.setText(completeAddress);
                    send_area_id = completeAddress;
                    pinCode = addresses.get(0).getPostalCode();
                    mLatitude = centerLatLng.latitude;
                    mLongitude = centerLatLng.longitude;
                }
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
