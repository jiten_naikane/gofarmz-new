package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapter.TestimonialAdapter;
import in.innasoft.gofarmz.models.TestimonialModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;

public class TestimonialActivity extends AppCompatActivity {

    RecyclerView testimonial_recyclerview;
    boolean checkInternet;
    ProgressDialog progressDialog;

    /*Testimonial List*/
    TestimonialAdapter testimonialAdapter;
    ArrayList<TestimonialModel> testimonialModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testimonial);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Testimonials");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        testimonial_recyclerview = (RecyclerView) findViewById(R.id.testimonial_recyclerview);
        testimonial_recyclerview.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(TestimonialActivity.this);
        testimonial_recyclerview.setLayoutManager(layoutManager);
        testimonialAdapter = new TestimonialAdapter(testimonialModels, TestimonialActivity.this, R.layout.row_testimonial);

        getTestimonials();
    }

    private void getTestimonials() {
        testimonialModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_TESTIMONIALS,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            Log.d("TestimonialResponce",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")){
                                    progressDialog.cancel();
                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    String recordTotalCnt = jsonObject1.getString("recordTotalCnt");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("recordData");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        TestimonialModel cm = new TestimonialModel();
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        cm.setId(jsonObject2.getString("id"));
                                        cm.setName(jsonObject2.getString("name"));
                                        cm.setDescription(jsonObject2.getString("description"));
                                        testimonialModels.add(cm);
                                    }
                                    testimonial_recyclerview.setAdapter(testimonialAdapter);
                                }

                                if (responceCode.equals("12786")){
                                    Toast.makeText(TestimonialActivity.this, "No Testimonials Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if(responceCode.equals("11786"))
                                {
                                    Toast.makeText(TestimonialActivity.this, "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            })
            {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("page_no", "1");
//                    Log.d("TestimonialParams:",params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        }else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
