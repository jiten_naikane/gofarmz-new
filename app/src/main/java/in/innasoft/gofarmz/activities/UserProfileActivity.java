package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapter.AreaAdapter;
import in.innasoft.gofarmz.adapter.CityAdapter;
import in.innasoft.gofarmz.adapter.CountryAdapter;
import in.innasoft.gofarmz.adapter.StateAdapter;
import in.innasoft.gofarmz.models.AreaModel;
import in.innasoft.gofarmz.models.CityProfileModel;
import in.innasoft.gofarmz.models.CountriesModel;
import in.innasoft.gofarmz.models.StateModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;


public class UserProfileActivity extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout name_til, mobile_til, email_til, address_til, pincode_til;
    EditText name_edt, mobile_edt, email_edt, address_edt, pincode_edt;
    TextView gender_txt, country_txt, state_txt, city_txt, area_txt;
    RadioGroup gender_rg;
    RadioButton male_rb, female_rb;
    Typeface typeface;
    Button update_btn;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager session;
    String user_id, token = "", name = "", gender = "", email = "", mobile = "", country_id = "", country_name = "", state_id = "", state_name = "",
            city_id = "", city_name = "", area_id = "", area_name = "", address = "", pincode = "";
    AlertDialog countrydialog, statedialog, citydialog, areadialog;

    /*Countrys List*/
    CountryAdapter countryAdapter;
    ArrayList<CountriesModel> countryModelArrayList = new ArrayList<>();
    ArrayList<String> countryList = new ArrayList<>();
    String send_country_id = "";

    /*State List*/
    StateAdapter stateAdapter;
    ArrayList<StateModel> stateModels = new ArrayList<>();
    ArrayList<String> stateList = new ArrayList<>();
    int state_check = 0;
    String send_state_id = "";

    /*City List*/
    CityAdapter cityAdapter;
    ArrayList<CityProfileModel> cityModels = new ArrayList<>();
    ArrayList<String> cityList = new ArrayList<>();
    String send_city_id = "";

    /*Area List*/
    AreaAdapter areaAdapter;
    ArrayList<AreaModel> areaModels = new ArrayList<>();
    ArrayList<String> areaList = new ArrayList<>();
    String send_area_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("My Profile");

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));
        //  typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_two));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        name = userDetails.get(UserSessionManager.USER_NAME);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        email = userDetails.get(UserSessionManager.USER_EMAIL);

//        Log.d("SESSIONDETAILS",user_id+"\n"+token+"\n"+name+"\n"+mobile+"\n"+email);

        name_til = (TextInputLayout) findViewById(R.id.name_til);
        mobile_til = (TextInputLayout) findViewById(R.id.mobile_til);
        email_til = (TextInputLayout) findViewById(R.id.email_til);
        address_til = (TextInputLayout) findViewById(R.id.address_til);
        pincode_til = (TextInputLayout) findViewById(R.id.pincode_til);

        name_edt = (EditText) findViewById(R.id.name_edt);
        name_edt.setText(name);
        name_edt.setTypeface(typeface);
        mobile_edt = (EditText) findViewById(R.id.mobile_edt);
        mobile_edt.setText(mobile);
        mobile_edt.setTypeface(typeface);
        email_edt = (EditText) findViewById(R.id.email_edt);
        email_edt.setText(email);
        email_edt.setTypeface(typeface);
        address_edt = (EditText) findViewById(R.id.address_edt);
        address_edt.setTypeface(typeface);
        pincode_edt = (EditText) findViewById(R.id.pincode_edt);
        pincode_edt.setTypeface(typeface);

        country_txt = (TextView) findViewById(R.id.country_txt);
        country_txt.setTypeface(typeface);
        country_txt.setOnClickListener(this);
        state_txt = (TextView) findViewById(R.id.state_txt);
        state_txt.setTypeface(typeface);
        state_txt.setOnClickListener(this);
        city_txt = (TextView) findViewById(R.id.city_txt);
        city_txt.setTypeface(typeface);
        city_txt.setOnClickListener(this);
        area_txt = (TextView) findViewById(R.id.area_txt);
        area_txt.setTypeface(typeface);
        area_txt.setOnClickListener(this);
        gender_txt = (TextView) findViewById(R.id.gender_txt);
        gender_txt.setTypeface(typeface);

        gender_rg = (RadioGroup) findViewById(R.id.gender_rg);

        male_rb = (RadioButton) findViewById(R.id.male_rb);
        male_rb.setTypeface(typeface);
        female_rb = (RadioButton) findViewById(R.id.female_rb);
        female_rb.setTypeface(typeface);

        /*cancel_btn = (Button) findViewById(R.id.cancel_btn);
        cancel_btn.setOnClickListener(this);*/
        update_btn = (Button) findViewById(R.id.update_btn);
        update_btn.setTypeface(typeface);
        update_btn.setOnClickListener(this);

        getProfile();

    }

    private void getProfile() {

        checkInternet = NetworkChecking.isConnected(UserProfileActivity.this);

        if (checkInternet) {
            progressDialog.show();

//            Log.d("PROFILEURL", AppUrls.BASE_URL+ AppUrls.USER_PROFILE);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.USER_PROFILE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            Log.d("PROFILERESPONCE",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("status");
                                if (successResponceCode.equals("10100")) {
                                    progressDialog.dismiss();

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

//                                    String id = jsonObject1.getString("id");
//                                    String name = jsonObject1.getString("name");
                                    gender = jsonObject1.getString("gender");
                                    if (gender.equals("Male") || gender.equals("male")) {
                                        male_rb.setChecked(true);
                                    }
                                    if (gender.equals("Female") || gender.equals("female")) {
                                        female_rb.setChecked(true);
                                    }
//                                    String email = jsonObject1.getString("email");
//                                    String mobile = jsonObject1.getString("mobile");
                                    country_id = jsonObject1.getString("country");
                                    send_country_id = country_id;
                                    country_name = jsonObject1.getString("country_name");
                                    if (country_name == null || country_name.equals("null") || country_name.equals("")) {
                                        country_txt.setText("Select Country");
                                    } else {
                                        country_txt.setText(country_name);
                                    }
                                    state_id = jsonObject1.getString("state");
                                    send_state_id = state_id;
                                    state_name = jsonObject1.getString("state_name");
                                    if (state_name == null || state_name.equals("null") || state_name.equals("")) {
                                        state_txt.setText("Select State");
                                    } else {
                                        state_txt.setText(state_name);
                                    }
                                    city_id = jsonObject1.getString("city");
                                    send_city_id = city_id;
                                    city_name = jsonObject1.getString("city_name");
                                    if (city_name == null || city_name.equals("null") || city_name.equals("")) {
                                        city_txt.setText("Select City");
                                    } else {
                                        city_txt.setText(city_name);
                                    }

                                    area_id = jsonObject1.getString("area");
                                    send_area_id = area_id;
                                    area_name = jsonObject1.getString("area_name");
                                    if (area_name == null || area_name.equals("null") || area_name.equals("")) {
                                        area_txt.setText("Select Area");
                                    } else {
                                        area_txt.setText(area_name);
                                    }

                                    address = jsonObject1.getString("address");
                                    address_edt.setText(address);
                                    pincode = jsonObject1.getString("pincode");
                                    pincode_edt.setText(pincode);

                                }

                                if (successResponceCode.equals("12786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "No Data Found...!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("11786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("10786")) {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
//                    Log.d("USERPROFILE:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
//                    Log.d("PROFILEHEADER", headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }

    @Override
    public void onClick(View v) {
        /*if (v == cancel_btn){
            finish();
        }*/
        if (v == country_txt) {
            countryList();
        }
        if (v == state_txt) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                if (!state_txt.getText().toString().equals("No States Found") || (!country_txt.getText().toString().equals("Select Country"))) {
                    stateDialog();
                } else {
                    Toast.makeText(this, "No States Found", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
            }
        }
        if (v == city_txt) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                cityDialog();
            } else {
                Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
        if (v == area_txt) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                areaDialog();
            } else {
                Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
        if (v == update_btn) {
            updateProfile();
        }
    }

    public void countryList() {
        countryModelArrayList.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.COUNTRY_LIST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            Log.d("CountryResponce", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    progressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        CountriesModel cm = new CountriesModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        cm.setId(jsonObject1.getString("id"));
                                        String country_name = jsonObject1.getString("name");
                                        cm.setName(country_name);
                                        cm.setName(jsonObject1.getString("name"));

                                        countryList.add(country_name);
                                        countryModelArrayList.add(cm);
                                    }
                                    countryDialog();
                                }

                                if (responceCode.equals("12786")) {
                                    Toast.makeText(UserProfileActivity.this, "No Countries Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    Toast.makeText(UserProfileActivity.this, "All Fields Required..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
//                    Log.d("SENDADSSAFSAF", "HEADDER " + headers.toString());
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }


    private void countryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.country_list, null);

//        TextView country_txt = (TextView) dialog_layout.findViewById(R.id.country_txt);

        final SearchView country_search = (SearchView) dialog_layout.findViewById(R.id.country_search);
//        EditText searchEditText = (EditText) country_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView country_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.country_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        country_recyclerview.setLayoutManager(layoutManager);

        countryAdapter = new CountryAdapter(countryModelArrayList, UserProfileActivity.this, R.layout.row_country);

        country_recyclerview.setAdapter(countryAdapter);
        country_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                country_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        countrydialog = builder.create();

        country_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                countryAdapter.getFilter().filter(query);
                return false;
            }
        });
        countrydialog.show();
    }

    public void setCountryName(String countryName, String countryId) {

        countrydialog.dismiss();
        countryAdapter.notifyDataSetChanged();

//        String sendCountryName = countryName;
        country_txt.setText(countryName);
        send_country_id = countryId;
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        state_check = 1;
        state_txt.setText("Select State");
        stateList(send_country_id);
    }

    public void stateList(final String countryId) {
        stateModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.STATE_LIST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    progressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        StateModel sm = new StateModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        sm.setId(jsonObject1.getString("id"));
                                        String state_name = jsonObject1.getString("name");
                                        sm.setName(state_name);
                                        sm.setName(jsonObject1.getString("name"));

                                        stateList.add(state_name);
                                        stateModels.add(sm);
                                    }
                                    // stateDialog();
                                }

                                if (responceCode.equals("12786")) {
                                    progressDialog.cancel();
                                    state_txt.setText("No States Found");
                                    Toast.makeText(UserProfileActivity.this, "No States Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    Toast.makeText(UserProfileActivity.this, "All Fields Required...!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
//                    Log.d("STATEHEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("country_id", countryId);
//                    Log.d("STATEPARAMS", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void stateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.state_list, null);

//        TextView state_txt = (TextView) dialog_layout.findViewById(R.id.state_txt);

        final SearchView state_search = (SearchView) dialog_layout.findViewById(R.id.state_search);
//        EditText searchEditText = (EditText) state_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView state_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.state_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        state_recyclerview.setLayoutManager(layoutManager);

        stateAdapter = new StateAdapter(stateModels, UserProfileActivity.this, R.layout.row_state);

        state_recyclerview.setAdapter(stateAdapter);
        state_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                state_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        statedialog = builder.create();

        state_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                stateAdapter.getFilter().filter(query);
                return false;
            }
        });
        statedialog.show();
    }

    public void setStateName(String stateName, String stateId) {

        statedialog.dismiss();
        stateAdapter.notifyDataSetChanged();

//        String sendStateName = stateName;
        send_state_id = stateId;
        state_txt.setText(stateName);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        cityList(send_state_id);
    }

    private void cityList(final String send_state_id) {
        cityModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.CITY_LIST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            Log.d("CCCCCCCCCCCCCC", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    progressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        CityProfileModel cm = new CityProfileModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        cm.setId(jsonObject1.getString("id"));
                                        String city_name = jsonObject1.getString("name");
                                        cm.setName(city_name);
                                        cm.setName(jsonObject1.getString("name"));

                                        cityList.add(city_name);
                                        cityModels.add(cm);
                                    }
                                }

                                if (responceCode.equals("12786")) {
                                    progressDialog.cancel();
                                    city_txt.setText("No City Found");
                                    Toast.makeText(UserProfileActivity.this, "No City Found", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    Toast.makeText(UserProfileActivity.this, "All Fields Required", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10786")) {
                                    Toast.makeText(UserProfileActivity.this, "Invalid Token", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
//                    Log.d("CITYHEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("state_id", send_state_id);
//                    Log.d("CITYPARAMS", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void cityDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.city_list, null);

//        TextView city_txt = (TextView) dialog_layout.findViewById(R.id.city_txt);

        final SearchView city_search = (SearchView) dialog_layout.findViewById(R.id.city_search);
//        EditText searchEditText = (EditText) city_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView city_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.city_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        city_recyclerview.setLayoutManager(layoutManager);

        cityAdapter = new CityAdapter(cityModels, UserProfileActivity.this, R.layout.row_city);

        city_recyclerview.setAdapter(cityAdapter);
        city_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                city_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        citydialog = builder.create();

        city_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                cityAdapter.getFilter().filter(query);
                return false;
            }
        });
        citydialog.show();
    }

    public void setCityName(String cityName, String city_id) {

        citydialog.dismiss();
        cityAdapter.notifyDataSetChanged();

//        String sendCityName = cityName;
        send_city_id = city_id;
        city_txt.setText(cityName);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        areaList(send_city_id);
    }

    private void areaList(final String send_city_id) {
        areaModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.AREA_LIST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
//                            Log.d("AREARESPONCE", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    progressDialog.cancel();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        AreaModel cm = new AreaModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        cm.setId(jsonObject1.getString("id"));
                                        String area_name = jsonObject1.getString("name");
                                        cm.setName(area_name);
                                        cm.setName(jsonObject1.getString("name"));

                                        areaList.add(area_name);
                                        areaModels.add(cm);
                                    }
                                }

                                if (responceCode.equals("12786")) {
                                    progressDialog.cancel();
                                    area_txt.setText("No Area Found");
                                    Toast.makeText(UserProfileActivity.this, "No Area Found", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    Toast.makeText(UserProfileActivity.this, "All Fields Required", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10786")) {
                                    Toast.makeText(UserProfileActivity.this, "Invalid Token", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                progressDialog.cancel();
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
//                    Log.d("AREAHEADER", "HEADDER " + headers.toString());
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("city_id", send_city_id);
//                    Log.d("AREAPARAMS", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);

        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void areaDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialog_layout = inflater.inflate(R.layout.area_list, null);

//        TextView area_txt = (TextView) dialog_layout.findViewById(R.id.area_txt);

        final SearchView area_search = (SearchView) dialog_layout.findViewById(R.id.area_search);
//        EditText searchEditText = (EditText) area_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView area_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.area_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        area_recyclerview.setLayoutManager(layoutManager);

        areaAdapter = new AreaAdapter(areaModels, UserProfileActivity.this, R.layout.row_area);

        area_recyclerview.setAdapter(areaAdapter);
        area_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                area_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        areadialog = builder.create();

        area_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                areaAdapter.getFilter().filter(query);
                return false;
            }
        });
        areadialog.show();
    }

    public void setAreaName(String areaName, String area_id) {

        areadialog.dismiss();
        areaAdapter.notifyDataSetChanged();

//        String sendAreaName = areaName;
        send_area_id = area_id;
        area_txt.setText(areaName);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    private void updateProfile() {
        if (validate()) {
            checkInternet = NetworkChecking.isConnected(UserProfileActivity.this);
            if (checkInternet) {
                progressDialog.show();

//                Log.d("UPDATEPROFILEURL", AppUrls.BASE_URL + AppUrls.UPDATE_PROFILE);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.UPDATE_PROFILE,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
//                                Log.d("UPDATEPROFILERESPONCE", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successResponceCode = jsonObject.getString("status");
                                    if (successResponceCode.equals("10100")) {
                                        progressDialog.dismiss();

                                        Toast.makeText(UserProfileActivity.this, "Your Profile Updated Successfully..!", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(UserProfileActivity.this, MainActivity.class);
                                        startActivity(intent);
                                    }

                                    if (successResponceCode.equals("10200")) {
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Sorry, Try Again...!", Toast.LENGTH_SHORT).show();
                                    }

                                    if (successResponceCode.equals("10300")) {
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Mobile Already Exists..!", Toast.LENGTH_SHORT).show();
                                    }

                                    if (successResponceCode.equals("11786")) {
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "All fields required..!", Toast.LENGTH_SHORT).show();
                                    }
                                    if (successResponceCode.equals("10786")) {
                                        progressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Invalid Token..!", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", user_id);
                        params.put("name", name_edt.getText().toString());
                        if (male_rb.isChecked()) {
                            params.put("gender", "Male");
                        }
                        if (female_rb.isChecked()) {
                            params.put("gender", "Female");
                        }

                        params.put("email", email_edt.getText().toString());
                        params.put("mobile", mobile_edt.getText().toString());
                        params.put("country", send_country_id);
                        params.put("state", send_state_id);
                        params.put("city", send_city_id);
                        params.put("area", send_area_id);
                        params.put("address", address_edt.getText().toString());
                        params.put("pincode", pincode_edt.getText().toString());
//                        Log.d("UPDATEPROFILE:", params.toString());
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization-Basic", token);
//                        Log.d("UPDATEPROFILEHEADER", headers.toString());
                        return headers;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(this);
                requestQueue.add(stringRequest);
            }
        }
    }

    private boolean validate() {

        boolean result = true;

        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        String MOBILE_REGEX = "^[789]\\d{9}$";

        String name = name_edt.getText().toString().trim();
        if ((name == null || name.equals("") || name.length() < 3)) {
            name_til.setError("Minimum 3 characters required");
            result = false;
        } else {
            if (!name.matches("^[\\p{L} .'-]+$")) {
                name_til.setError("Special characters not allowed");
                result = false;

            } else {
                name_til.setErrorEnabled(false);
            }
        }

        String mobile = mobile_edt.getText().toString().trim();
        if (!mobile.matches(MOBILE_REGEX)) {
            mobile_til.setError("Invalid Mobile Number");
            result = false;
        } else {
            mobile_til.setErrorEnabled(false);
        }

        String email = email_edt.getText().toString().trim();
        if (!email.matches(EMAIL_REGEX)) {
            email_til.setError("Invalid Email");
            result = false;
        } else {
            email_til.setErrorEnabled(false);
        }

        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
