package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;

public class ChangePasswordActivity extends AppCompatActivity  implements View.OnClickListener
{
    private boolean checkInternet;
    ProgressDialog progressDialog;
    UserSessionManager session;
    String user_id,jwt_token;
    EditText new_password_edt,current_password_edt;
    Button change_btn;
    TextInputLayout current_password_til,new_password_til;
    CollapsingToolbarLayout ctl;
    AppBarLayout app_bar_layout;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));

        ctl = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
      //  ctl.setTitle("CHANGE PASSWORD");
        ctl.setExpandedTitleTypeface(typeface);
        ctl.setCollapsedTitleTextColor(Color.WHITE);
        ctl.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        ctl.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        app_bar_layout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        jwt_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("CHANGNNN",user_id+"//"+jwt_token);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        current_password_til = (TextInputLayout) findViewById(R.id.current_password_til);
        new_password_til = (TextInputLayout) findViewById(R.id.new_password_til);

        current_password_edt = (EditText) findViewById(R.id.current_password_edt);
        current_password_edt.setTypeface(typeface);
       /* current_password_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    app_bar_layout.setExpanded(false, true);
                }
            }
        });*/

        new_password_edt = (EditText) findViewById(R.id.new_password_edt);
        new_password_edt.setTypeface(typeface);
      /*  new_password_edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    app_bar_layout.setExpanded(false, true);
                }
            }
        });
*/
        change_btn = (Button) findViewById(R.id.change_btn);
        change_btn.setTypeface(typeface);
        change_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view)
    {
        if(view==change_btn)
        {
            if (validate())
            {
                checkInternet = NetworkChecking.isConnected(ChangePasswordActivity.this);
                if (checkInternet)
                {
                    progressDialog.show();
                    final String currentPassword = current_password_edt.getText().toString().trim();
                    final String newPassword = new_password_edt.getText().toString().trim();
                    Log.d("CHAGNEPASSURL:", AppUrls.BASE_URL+ AppUrls.CHANGE_PASSWORD);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+ AppUrls.CHANGE_PASSWORD,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response)
                                {
                                    Log.d("CHAGNEPASSRESP:",response);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String successResponceCode = jsonObject.getString("status");
                                        if(successResponceCode.equals("10100"))
                                        {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Your password has been Changed...!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                                            intent.putExtra("activity_name","splash");
                                            startActivity(intent);
                                        }

                                        if(successResponceCode.equals("10200"))
                                        {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                                        }

                                        if(successResponceCode.equals("10300"))
                                        {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Your new password and old password should not match..!", Toast.LENGTH_SHORT).show();
                                        }

                                        if(successResponceCode.equals("10400"))
                                        {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Your old password is incorrect..!", Toast.LENGTH_SHORT).show();
                                        }

                                        if(successResponceCode.equals("11786"))
                                        {
                                            progressDialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "All fields are required.!", Toast.LENGTH_SHORT).show();
                                        }


                                    }
                                    catch (JSONException e)
                                    {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            })
                    {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError
                        {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id", user_id);
                            params.put("old_pwd", currentPassword);
                            params.put("new_pwd", newPassword);
                            Log.d("CHAGNEPASS:",params.toString());
                            return params;
                        }
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<>();

                          //  headers.put("Content-Type", "application/json");
                            headers.put("Authorization-Basic",jwt_token);
                            Log.d("CAHGENHEADER", "HEADDER "+headers.toString());
                            return headers;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(this);
                    requestQueue.add(stringRequest);
                }
            }
        }
    }

    private boolean validate() {

        boolean result = true;

        String currentPassword = current_password_edt.getText().toString().trim();
        if ((currentPassword == null || currentPassword.equals("")) || currentPassword.matches("^-\\s") || (currentPassword.length() < 6)) {
            current_password_til.setError("Invalid Password");
            result = false;
        }else {
            current_password_til.setErrorEnabled(false);
        }

        String newPassword = new_password_edt.getText().toString().trim();
        if ((newPassword == null || newPassword.equals("")) || newPassword.matches("^-\\s") || (newPassword.length() < 6)) {
            new_password_til.setError("Invalid Password");
            result = false;
        }else {
            new_password_til.setErrorEnabled(false);
        }

        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
