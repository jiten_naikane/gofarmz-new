package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;

public class BlogDescriptionActivity extends AppCompatActivity {

    private boolean checkInternet;
    ImageView blog_img;
    TextView blog_tittle,blog_desp;
    ProgressDialog progressDialog;
    String blog_id;
    Typeface typeface,typeface2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_description);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));
        typeface2 = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_two));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Blog Description");

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        Bundle bundle = getIntent().getExtras();
        blog_id = bundle.getString("blog_id");

        blog_img = (ImageView) findViewById(R.id.blog_img);

        blog_tittle = (TextView) findViewById(R.id.blog_tittle);
        blog_tittle.setTypeface(typeface2);

        blog_desp = (TextView) findViewById(R.id.blog_desp);
        blog_desp.setTypeface(typeface);

        getBlogDetails();
    }

    private void getBlogDetails() {
        checkInternet = NetworkChecking.isConnected(BlogDescriptionActivity.this);
        if (checkInternet)
        {
            progressDialog.show();

            Log.d("BLOGVIEW", AppUrls.BASE_URL+ AppUrls.GET_BLOGS_VIEW);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL+ AppUrls.GET_BLOGS_VIEW,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            Log.d("BlogViewResponce",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("status");
                                if(successResponceCode.equals("10100"))
                                {
                                    progressDialog.dismiss();

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                    String id = jsonObject1.getString("id");
                                    String category_id = jsonObject1.getString("category_id");
                                    String name = jsonObject1.getString("name");
                                    blog_tittle.setText(name);
                                    String url_name = jsonObject1.getString("url_name");
                                    String description = jsonObject1.getString("description");
                                    blog_desp.setText(Html.fromHtml(description));
                                    String image = AppUrls.BASE_URL+jsonObject1.getString("image");
                                    Picasso.with(BlogDescriptionActivity.this)
                                            .load(image)
                                            .placeholder(R.drawable.nodata_image)
                                            .into(blog_img);
                                    String seo_title = jsonObject1.getString("seo_title");
                                    String seo_description = jsonObject1.getString("seo_description");
                                    String seo_keywords = jsonObject1.getString("seo_keywords");
                                    String status = jsonObject1.getString("status");
                                    String create_date_time = jsonObject1.getString("create_date_time");
                                    String update_date_time = jsonObject1.getString("update_date_time");
                                }

                                if(successResponceCode.equals("12786"))
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "No Data Found...!", Toast.LENGTH_SHORT).show();
                                }

                                if(successResponceCode.equals("11786"))
                                {
                                    progressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "All fields are required.!", Toast.LENGTH_SHORT).show();
                                }

                            }
                            catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    })
            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("blog_id", blog_id);
                    Log.d("CHAGNEPASS:",params.toString());
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

