package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapter.WishListAdapter;
import in.innasoft.gofarmz.models.WishListModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;

public class WishListActivity extends AppCompatActivity {
    private boolean checkInternet;
    UserSessionManager session;
    ProgressDialog pprogressDialog;
    WishListAdapter wishListAdapter;
    RecyclerView wishlist_recylerview;
    ImageView nodata_image;
    LinearLayoutManager layoutManager;
    String user_id, jwt_token;

    ArrayList<WishListModel> wishListtModel = new ArrayList<>();
    int defaultPageNo = 1;
    private static int displayedposition = 0;
    int total_number_of_items = 0;
    private boolean loading = true;
    int type_of_request = 0;
    private boolean userScrolled = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("WishList");

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        jwt_token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("WhishLISt", user_id + "//" + jwt_token);

        pprogressDialog = new ProgressDialog(this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);


        wishlist_recylerview = (RecyclerView) findViewById(R.id.wishlist_recylerview);

        wishListAdapter = new WishListAdapter(wishListtModel, WishListActivity.this, R.layout.row_wishlist_list);
        layoutManager = new LinearLayoutManager(this);
        wishlist_recylerview.setNestedScrollingEnabled(false);
        wishlist_recylerview.setLayoutManager(layoutManager);
        // wishList(defaultPageNo);

        wishlist_recylerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    displayedposition = pastVisiblesItems;
                    Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);

                    if (loading) {
                        if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            userScrolled = false;
                            Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
                            Log.d("PPPPPPPPP", String.valueOf(defaultPageNo));
                            if (totalItemCount < total_number_of_items) {
                                type_of_request = 1;
                                wishList(defaultPageNo);

                            } else {
                                Toast.makeText(WishListActivity.this, "No More Results", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });

    }

    public void wishList(final int defaultPageNo) {


        checkInternet = NetworkChecking.isConnected(WishListActivity.this);
        if (checkInternet) {
            Log.d("WISHLISTURL", AppUrls.BASE_URL + AppUrls.GET_WISHLIST);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_WISHLIST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("WISHLISTRESP", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    wishlist_recylerview.setVisibility(View.VISIBLE);

                                    JSONObject json = jsonObject.getJSONObject("data");
                                    int jsonArray_count = json.getInt("recordTotalCnt");
                                    JSONArray jsonArray = json.getJSONArray("recordData");
                                    if (jsonArray.equals("[]")) {
                                        nodata_image.setVisibility(View.VISIBLE);
                                    }

                                    total_number_of_items = jsonArray_count;
                                    Log.d("LOADSTATUS", "OUTER " + loading + "  " + jsonArray_count);
                                    if (jsonArray_count > wishListtModel.size()) {
                                        loading = true;
                                        Log.d("LOADSTATUS", "INNER " + loading + "  " + jsonArray_count);
                                    } else {
                                        loading = false;
                                    }

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        WishListModel gbm = new WishListModel();

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        gbm.setId(jsonObject1.getString("id"));
                                        gbm.setProduct_id(jsonObject1.getString("product_id"));
                                        gbm.setProduct_name(jsonObject1.getString("product_name"));
                                        gbm.setProduct_mrp_prize(jsonObject1.getString("mrp_price"));
                                        gbm.setProduct_weight_name(jsonObject1.getString("weight_name"));
                                        gbm.setImages(AppUrls.WISHLIST_IMAGE_URL + jsonObject1.getString("images"));
                                        gbm.setStatus(jsonObject1.getString("status"));

                                        wishListtModel.add(gbm);

                                    }
                                    layoutManager.scrollToPositionWithOffset(displayedposition, wishListtModel.size());
                                    wishlist_recylerview.setAdapter(wishListAdapter);

                                }
                                if (responceCode.equals("12786")) {
                                    Toast.makeText(WishListActivity.this, "No Data Found..", Toast.LENGTH_LONG).show();
                                    wishlist_recylerview.setVisibility(View.GONE);
                                    finish();
                                }


                                if (responceCode.equals("11786")) {
                                    Toast.makeText(WishListActivity.this, "All fields are required..", Toast.LENGTH_LONG).show();
                                }

                                if (responceCode.equals("10786")) {
                                    Toast.makeText(WishListActivity.this, " Invalid Token..", Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("page_no", String.valueOf(defaultPageNo));
                    params.put("user_id", user_id);
                    Log.d("WISHLISTREQUESTDATA:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();

                    //  headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", jwt_token);
                    Log.d("WishListHEADER", headers.toString());
                    return headers;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(WishListActivity.this);
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        wishListtModel.clear();
        displayedposition = 0;
        wishList(defaultPageNo);

    }
}
