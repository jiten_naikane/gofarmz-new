package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapter.BlogListAdapter;
import in.innasoft.gofarmz.models.BlogListModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;

public class BlogsListActivity extends AppCompatActivity {


    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    RecyclerView blogs_recylerview;
    BlogListAdapter blogAdapter;
    ImageView nodata_image;
    ArrayList<BlogListModel>blogListModel=new ArrayList<BlogListModel>();
    int defaultPageNo = 1;
    private static int displayedposition = 0;
    int total_number_of_items = 0;
    private boolean loading = true;
    int type_of_request = 0;
    private boolean userScrolled = true;
    LinearLayoutManager  layoutManager;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blogs_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Blogs");

        pprogressDialog = new ProgressDialog(BlogsListActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        nodata_image = (ImageView)findViewById(R.id.nodata_image);

        blogs_recylerview = (RecyclerView)findViewById(R.id.blogs_recylerview);

        blogAdapter = new BlogListAdapter(blogListModel, BlogsListActivity.this, R.layout.row_blog_list);
        layoutManager = new LinearLayoutManager(this);
        blogs_recylerview.setNestedScrollingEnabled(false);
        blogs_recylerview.setLayoutManager(layoutManager);

        blogList(defaultPageNo);
        blogs_recylerview.addOnScrollListener(new RecyclerView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                if(dy > 0)
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    displayedposition = pastVisiblesItems;
                    Log.v("SDFASFAFAFAF", "Last Item Wow !"+pastVisiblesItems+", "+visibleItemCount+", "+totalItemCount);

                    if (loading)
                    {
                        if ( userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            userScrolled = false;
                            Log.v("SDFASFAFAFAF", "Last Item Wow !"+pastVisiblesItems+", "+visibleItemCount+", "+totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
                            Log.d("PPPPPPPPP", String.valueOf(defaultPageNo));
                            if(totalItemCount < total_number_of_items) {
                                type_of_request = 1;
                                blogList(defaultPageNo);

                            }else {
                                Toast.makeText(BlogsListActivity.this, "No More Results", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });



    }

    private void blogList(final int default_page_number)
    {

        checkInternet = NetworkChecking.isConnected(BlogsListActivity.this);
        if (checkInternet)
        {
            Log.d("BLOGURL", AppUrls.BASE_URL + AppUrls.GET_BLOGS_LIST);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_BLOGS_LIST,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("BLOGRESP",response);
                                String responceCode = jsonObject.getString("status");
                                if(responceCode.equals("10100")) {


                                    JSONObject json = jsonObject.getJSONObject("data");
                                    int jsonArray_count = json.getInt("recordTotalCnt");
                                    JSONArray jsonArray = json.getJSONArray("recordData");
                                    if(jsonArray.equals("[]"))
                                    {
                                        nodata_image.setVisibility(View.VISIBLE);
                                    }

                                    total_number_of_items = jsonArray_count;
                                    Log.d("LOADSTATUS",  "OUTER "+loading+"  "+jsonArray_count);
                                    if(jsonArray_count > blogListModel.size())
                                    {
                                        loading = true;
                                        Log.d("LOADSTATUS",  "INNER "+loading+"  "+jsonArray_count);
                                    }else {
                                        loading = false;
                                    }

                                    for (int i = 0; i < jsonArray.length(); i++)
                                    {

                                        BlogListModel gbm = new BlogListModel();

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        gbm.setId(jsonObject1.getString("id"));
                                        gbm.setName(jsonObject1.getString("name"));
                                        gbm.setImage(AppUrls.BASE_URL+jsonObject1.getString("image"));

                                        blogListModel.add(gbm);

                                    }
                                    layoutManager.scrollToPositionWithOffset(displayedposition , blogListModel.size());
                                    blogs_recylerview.setAdapter(blogAdapter);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            }){


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("page_no", String.valueOf(default_page_number));

                    Log.d("BlogREQUESTDATA:",params.toString());
                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(BlogsListActivity.this);
            requestQueue.add(stringRequest);

        }else {
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
