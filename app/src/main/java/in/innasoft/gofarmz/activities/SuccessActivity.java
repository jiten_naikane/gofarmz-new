package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utilities.UserSessionManager;

public class SuccessActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView toolbar_tittle, success_txt, order_info;
    Typeface typeface;
    UserSessionManager session;
    String order_id, message;
//    ProgressDialog progressDialog;


//    public String user_id, product_id, product_price, quantity, name, email, mobile, address, grand_total, delevery_charges, trasport_type, paid_amount, paymentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Success");


        session = new UserSessionManager(getApplicationContext());

//        progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Please wait......");
//        progressDialog.setProgressStyle(R.style.DialogTheme);
//        progressDialog.setCancelable(false);

        typeface = Typeface.createFromAsset(this.getAssets(), "museosanscyrl.ttf");

        order_id = getIntent().getExtras().getString("order_pid");
        message = getIntent().getExtras().getString("message");

//        Bundle bundle = getIntent().getExtras();
//        user_id = bundle.getString("user_id");
//        product_id = bundle.getString("product_id");
//        product_price = bundle.getString("product_price");
//        quantity = bundle.getString("quantity");
//        name = bundle.getString("name");
//        email = bundle.getString("email");
//        mobile = bundle.getString("mobile");
//        address = bundle.getString("address");
//        grand_total = bundle.getString("grand_total");
//        delevery_charges = bundle.getString("delevery_charges");
//        trasport_type = bundle.getString("trasport_type");
//        paid_amount = bundle.getString("paid_amount");
//        paymentId = bundle.getString("trasaction_id");
//        order_id = bundle.getString("order_pid");
//        referenceId = bundle.getString("referenceId");


        close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(this);
        toolbar_tittle = (TextView) findViewById(R.id.toolbar_tittle);
        order_info = (TextView) findViewById(R.id.order_info);
        toolbar_tittle.setTypeface(typeface);
        order_info.setTypeface(typeface);
        success_txt = (TextView) findViewById(R.id.success_txt);
        success_txt.setTypeface(typeface);


        order_info.setText(message);

        //order_info.setText(" Your Transaction is : "+payment_id+"\n Your Order Id : "+order_id);


//        Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();

//        Log.v("Url success ", ">>>> " + order_id + "?transaction_id=" + paymentId);

     /*   String url = AppUrls.BASE_URL+"payu_success?transaction_id="+paymentId+"&order_id="+order_id+"&reference_id_app="+referenceId;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("success after pay ", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.d("FULLRESPONCE", response);
                            final String message = jsonObject.getString("message");
                            progressDialog.dismiss();
                            order_info.setText(message);

                            SuccessActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    order_info.setText(message);
                                }
                            });

                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }

                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
           *//* @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
               *//**//* Log.d("SENDINGVALUESDSSDS", user_id+"\n"+product_id+"\n"+product_price+"\n"+quantity+"\n"+name+"\n"+email+"\n"+mobile+"\n"+address+"\n"+grand_total+"\n"+delevery_charges+"\n"+trasport_type+"\n"+paid_amount+"\n10"+paymentId);
                params.put("user_id", user_id);
                params.put("product_id", product_id);
                params.put("product_price", product_price);
                params.put("quantity", quantity);
                params.put("name", name);
                params.put("email", email);
                params.put("mobile", mobile);
                params.put("address", address);
                params.put("grand_total", grand_total);
                params.put("delevery_charges", delevery_charges);
                params.put("trasport_type", trasport_type);
                params.put("paid_amount", paid_amount);
                params.put("transaction_charges", "0");
                params.put("trasaction_id", paymentId);*//**//*
                return params;
            }*//*
        };
        RequestQueue requestQueue = Volley.newRequestQueue(SuccessActivity.this);
        requestQueue.add(stringRequest);*/

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(View v) {
        if (v == close) {
            Intent i = new Intent(SuccessActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(SuccessActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
