package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapter.CartListAdapter;
import in.innasoft.gofarmz.dbhelper.LocationControlar;
import in.innasoft.gofarmz.models.CartListModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;


public class CartProductListActivity extends AppCompatActivity {
    RecyclerView cartlist_recylerview;
    CartListAdapter cartListAdapter;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    ProgressDialog pprogressDialog;
    ImageView nodata_image;
    String user_id, jwt_token, device_id, user_name, user_mobile, user_email, browser_session_id, acc_status;
    ArrayList<CartListModel> cartListModels = new ArrayList<CartListModel>();
    Button proceed_btn;
    TextView Grand_total_txt;
    //    String totalPrice = "0";
    public static boolean add = true;
    Typeface typeface;
    LocationControlar locationControlar;
    //    ArrayList<CartListModel> listLocations;
    RelativeLayout buy_ll;
    String final_value = "";
//    CartBucketDBHelper cartBucketDBHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_product_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Cart List");
        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));

        locationControlar = new LocationControlar(CartProductListActivity.this);
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        jwt_token = userDetails.get(UserSessionManager.KEY_ACCSES);
//        cartBucketDBHelper = new CartBucketDBHelper(CartProductListActivity.this);

        user_name = userDetails.get(UserSessionManager.USER_NAME);
        user_mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        user_email = userDetails.get(UserSessionManager.USER_EMAIL);
        browser_session_id = userDetails.get(UserSessionManager.BROWSER_SESSION_ID);
        acc_status = userDetails.get(UserSessionManager.USER_ACCOUNT_STATUS);
        proceed_btn = findViewById(R.id.proceed_btn);
        proceed_btn.setTypeface(typeface);
        Grand_total_txt = findViewById(R.id.Grand_total_txt);
        Grand_total_txt.setTypeface(typeface);

        buy_ll = findViewById(R.id.buy_ll);
        Log.d("cart-list", user_id + "//" + jwt_token);


        proceed_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (jwt_token == null) {
                    user_id = "0";
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(CartProductListActivity.this);
                    builder1.setMessage("User Must be Login for this Operation, Click yes to redirect to login Page");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    Intent intent = new Intent(CartProductListActivity.this, LoginActivity.class);
                                    intent.putExtra("activity_name", "productDescription");
                                    startActivity(intent);
                                }
                            });

                    builder1.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {

                    Intent intent = new Intent(CartProductListActivity.this, ShippingProductActivity.class);
                    intent.putExtra("totalprice", final_value);
                    startActivity(intent);

                    //  Toast.makeText(CartProductListActivity.this, "COD is working", Toast.LENGTH_SHORT).show();
                }
            }
        });


        pprogressDialog = new ProgressDialog(CartProductListActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        pprogressDialog.setCancelable(false);
        pprogressDialog.setCanceledOnTouchOutside(false);

        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("DEVICEID:", device_id);

        nodata_image = findViewById(R.id.nodata_image);

        cartlist_recylerview = findViewById(R.id.cartlist_recylerview);
        cartlist_recylerview.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(CartProductListActivity.this);
        cartlist_recylerview.setLayoutManager(layoutManager);
        cartListAdapter = new CartListAdapter(cartListModels, CartProductListActivity.this, R.layout.row_cart_list_data);
        cartlist_recylerview.setAdapter(cartListAdapter);

    }

    public void getCartListData(final String user_id) {
        checkInternet = NetworkChecking.isConnected(CartProductListActivity.this);
        if (checkInternet) {

            if (pprogressDialog != null)
                pprogressDialog.show();

            Log.d("ADDFAVURL:", AppUrls.BASE_URL + AppUrls.GET_CART_DATA);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_CART_DATA,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("ADDFAVRESPnhfgnmh:", response);
                            if (pprogressDialog.isShowing())
                                pprogressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("status");
                                if (successResponceCode.equals("10100")) {
                                    // fab.setBackgroundResource(R.drawable.like_red_icon);
                                    cartlist_recylerview.setVisibility(View.VISIBLE);
                                    buy_ll.setVisibility(View.VISIBLE);
                                    JSONObject json = jsonObject.getJSONObject("data");
                                    int final_values = json.getInt("finalTotal");
                                    final_value = String.valueOf(final_values);
                                    Grand_total_txt.setText("Grand Total : \u20B9" + final_value + " /-");
                                    JSONArray json_array = json.getJSONArray("recordData");
                                    cartListModels.clear();
                                    for (int i = 0; i < json_array.length(); i++) {

                                        CartListModel gbm = new CartListModel();

                                        JSONObject jsonObject1 = json_array.getJSONObject(i);
                                        gbm.setId(jsonObject1.getString("id"));
                                        gbm.setBrowser_id(jsonObject1.getString("browser_id"));
                                        gbm.setUser_id(jsonObject1.getString("user_id"));
                                        gbm.setUser_type(jsonObject1.getString("user_type"));
                                        gbm.setProduct_sku_id(jsonObject1.getString("product_sku_id"));
                                        gbm.setProduct_id(jsonObject1.getString("product_id"));
                                        gbm.setProduct_name(jsonObject1.getString("product_name"));
                                        gbm.setImages(AppUrls.PRODUCTS_IMAGE_URL + jsonObject1.getString("images"));
                                        gbm.setProduct_count_id(jsonObject1.getString("product_count_id"));
                                        gbm.setProduct_weight_id(jsonObject1.getString("product_weight_id"));
                                        gbm.setProduct_weight(jsonObject1.getString("product_weight"));
                                        gbm.setProduct_count(jsonObject1.getString("product_count"));
                                        gbm.setProduct_quantity(jsonObject1.getString("product_quantity"));
                                        gbm.setProduct_mrp_price(jsonObject1.getString("product_mrp_price"));
                                        gbm.setPurchase_quantity(jsonObject1.getString("purchase_quantity"));
                                        gbm.setTotal_price(jsonObject1.getString("total_price"));
                                        gbm.setGrand_total(jsonObject1.getString("grand_total"));
                                        gbm.setDiscount(jsonObject1.getString("discount"));

                                        if (!cartListModels.contains(gbm))
                                            cartListModels.add(gbm);

                                        /*ContentValues values = new ContentValues();
                                        values.put(CartBucketDBHelper.CART_PRODUCT_ID, jsonObject1.getString("product_id"));
                                        values.put(CartBucketDBHelper.CART_PRODUCT_NAME, jsonObject1.getString("product_name"));
                                        values.put(CartBucketDBHelper.CART_PRODUCT_IMAGE, AppUrls.PRODUCTS_IMAGE_URL + jsonObject1.getString("images"));
                                        values.put(CartBucketDBHelper.CART_PRICE, jsonObject1.getString("product_mrp_price"));
                                        values.put(CartBucketDBHelper.CART_FINAL_PRICE, jsonObject1.getString("grand_total"));
                                        values.put(CartBucketDBHelper.CART_QUANTITY, jsonObject1.getString("purchase_quantity"));
                                        cartBucketDBHelper.addCartBucket(values);*/
                                    }

                                    cartlist_recylerview.setAdapter(cartListAdapter);
//                                    cartListAdapter.notifyDataSetChanged();

                                }

                                if (successResponceCode.equals("10200")) {
                                    pprogressDialog.dismiss();
                                    // Toast.makeText(getApplicationContext(), "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("10300")) {
                                    pprogressDialog.dismiss();
                                }

                                if (successResponceCode.equals("11786")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "All fields are required..!", Toast.LENGTH_SHORT).show();
                                }
                                if (successResponceCode.equals("12786")) {
                                    pprogressDialog.dismiss();
                                    buy_ll.setVisibility(View.GONE);
                                    // Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
                                    cartlist_recylerview.setVisibility(View.GONE);
                                    finish();
                                }

                                if (successResponceCode.equals("10786")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Invalid Token.!", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("browserId", device_id);
                    Log.d("ADDFAVPAram:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    //  headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", jwt_token);
                    Log.d("CAHGENHEADER", "HEADDER " + headers.toString());
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void getCartListDatas() {
        cartListModels.clear();

        List<String> productId = locationControlar.getCartProductId();

        if (productId.size() > 0) {

            List<String> productName = locationControlar.getProductName();
            List<String> productWeight = locationControlar.getProductWeight();
            List<String> productQuantity = locationControlar.getProductQuantity();
            List<String> productPrice = locationControlar.getProductPrice();
            List<String> productsku_id = locationControlar.getProductSkuId();
            List<String> product_count_name = locationControlar.getProductCountNamet();
            List<String> productImage = locationControlar.getProductImage();
            List<String> productfinal_amount = locationControlar.getProducatFinalPrice();


            for (int i = 0; i < productId.size(); i++) {

                CartListModel bpm = new CartListModel();

                bpm.setImages(productImage.get(i));
                bpm.setProduct_id(productId.get(i));
                bpm.setProduct_name(productName.get(i));
                bpm.setProduct_mrp_price(productPrice.get(i));
                bpm.setProduct_weight(productWeight.get(i));
                bpm.setProduct_quantity(productQuantity.get(i));
                bpm.setProduct_sku_id(productsku_id.get(i));
                bpm.setProduct_count(product_count_name.get(i));
                bpm.setTotal_price(productfinal_amount.get(i));

                cartListModels.add(bpm);
            }


            cartlist_recylerview.setAdapter(cartListAdapter);

            Grand_total_txt.setText(String.valueOf("Grand Total  :  " + "Rs. " + locationControlar.findGndTotalPrice() + "/-"));

        } else {
            Grand_total_txt.setVisibility(View.GONE);
            proceed_btn.setVisibility(View.GONE);
            nodata_image.setVisibility(View.VISIBLE);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_empty_cart, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;


            case R.id.empty_cart:
                if (jwt_token == null) {
                    user_id = "0";

                    getEmptyCart();
                } else {
                    getEmptyCart();
                }

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getEmptyCart() {
        // locationControlar.emptyCartBucket();
        //  refreshCartPage();
        //  proceed_btn.setVisibility(View.GONE);
        checkInternet = NetworkChecking.isConnected(CartProductListActivity.this);
        if (checkInternet) {
            pprogressDialog.show();

            Log.d("EMPTYCARTURL:", AppUrls.BASE_URL + AppUrls.EMPTY_CART_DATA);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.EMPTY_CART_DATA,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("EMPTYCARTRESP:", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String successResponceCode = jsonObject.getString("status");
                                if (successResponceCode.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), " Your Cart Is Empty!", Toast.LENGTH_SHORT).show();
                                    finish();
//                                    getCartListData(user_id);
                                    // locationControlar.emptyCartBucket();

                                }

                                if (successResponceCode.equals("12786")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Sorry, Try again...!", Toast.LENGTH_SHORT).show();
                                }

                                if (successResponceCode.equals("11786")) {
                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "All fields are required.!", Toast.LENGTH_SHORT).show();
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("browserId", device_id);

                    Log.d("EMPTYCARTPARAM:", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();

                    //  headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", jwt_token);
                    Log.d("EMPTYCARTHEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            pprogressDialog.cancel();
            Toast.makeText(CartProductListActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        userSessionManager = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        jwt_token = userDetails.get(UserSessionManager.KEY_ACCSES);

        user_name = userDetails.get(UserSessionManager.USER_NAME);
        user_mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        user_email = userDetails.get(UserSessionManager.USER_EMAIL);
        browser_session_id = userDetails.get(UserSessionManager.BROWSER_SESSION_ID);
        acc_status = userDetails.get(UserSessionManager.USER_ACCOUNT_STATUS);

        Log.d("cart-list", user_id + "//" + jwt_token);

        if (jwt_token == null) {
            user_id = "0";
            getCartListData(user_id);
        } else {
            getCartListData(user_id);
        }
    }

    public Double getTotal(ArrayList<CartListModel> listLocations) {
        Double total = 0.0;
        Log.e("Size =", "" + listLocations.size());
        for (int i = 0; i < listLocations.size(); i++) {
            total = total + Double.parseDouble(listLocations.get(i).getTotal_price());
        }
        return total;

    }


    public void refreshCartPage() {
        cartListAdapter.notifyDataSetChanged();
        getCartListData(user_id);
    }
}
