package in.innasoft.gofarmz.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapter.MainCategoryProductsAdapter;
import in.innasoft.gofarmz.models.CityModel;
import in.innasoft.gofarmz.models.MainCategoryModel;
import in.innasoft.gofarmz.models.MostSellingModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.CitySelectionSession;
import in.innasoft.gofarmz.utilities.NetworkChecking;


public class HomeFragment extends Fragment {

    View view;

    private boolean checkInternet;
    RecyclerView most_selling_recyclerview;
    ArrayList<MostSellingModel> mostsellinglist;
    MainCategoryProductsAdapter mostsellingadapter;
    public String disp_city_name = "", disp_city_id = "";
    CitySelectionSession citySelectionSession;

    ArrayList<MainCategoryModel> mainCategoryList = new ArrayList<>();

    int defaultPageNo = 1;
    int displayedposition = 0;
    int displayedposition1 = 0;
    int total_number_of_items = 0;
    private boolean loading = true;
    int type_of_request = 0;
    private boolean userScrolled = true;
    LinearLayoutManager layoutManager;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager layoutManager_linear;
    String weight_id = "empty", min_price = "empty", max_price = "empty";
    String sub_category_id = "0", child_category_id = "0";
    String sort_by = "id-desc";
    TextView textTitle;
    ArrayList<String> citi_list = new ArrayList<>();
    ArrayList<CityModel> homecityModels = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);

        citySelectionSession = new CitySelectionSession(getActivity());

        loadCityData();


        textTitle = (TextView) view.findViewById(R.id.textTitle);

        most_selling_recyclerview = (RecyclerView) view.findViewById(R.id.most_selling_recyclerview);
        mostsellinglist = new ArrayList<>();
        mostsellingadapter = new MainCategoryProductsAdapter(mostsellinglist, getActivity(), R.layout.main_category_products_row);
        layoutManager = new GridLayoutManager(getActivity(), 1);
        most_selling_recyclerview.setNestedScrollingEnabled(false);
        most_selling_recyclerview.setLayoutManager(layoutManager);


        most_selling_recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {

                    try {
                        visibleItemCount = layoutManager.getChildCount();
                        totalItemCount = layoutManager.getItemCount();
                        pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                        visibleItemCount = layoutManager_linear.getChildCount();
                        totalItemCount = layoutManager_linear.getItemCount();
                        pastVisiblesItems = layoutManager_linear.findFirstVisibleItemPosition();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    displayedposition = pastVisiblesItems;
                    displayedposition1 = pastVisiblesItems;
//                    Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);

                    if (loading) {
                        if (userScrolled && (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            userScrolled = false;
//                            Log.v("SDFASFAFAFAF", "Last Item Wow !" + pastVisiblesItems + ", " + visibleItemCount + ", " + totalItemCount);
                            defaultPageNo = defaultPageNo + 1;
//                            Log.d("PPPPPPPPP", String.valueOf(defaultPageNo));
                            if (totalItemCount < total_number_of_items) {
                                type_of_request = 1;
                                getMostSellingProducts(defaultPageNo, sort_by, weight_id, min_price, max_price, sub_category_id, child_category_id);

                            } else {
                                Toast.makeText(getActivity(), "No More Results", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });


        return view;
    }


    private void getCityData() {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            homecityModels.clear();
            StringRequest string_request = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOCATION,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
//                            Log.e("RESPCITY", "RESPCITY:" + response.toString());
                            try {
                                JSONObject jsonObject = new JSONObject(response);
//                                Log.d("Citites:", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        CityModel pcm = new CityModel();
                                        pcm.setCityId(jsonObject1.getString("id"));
                                        String city_name = jsonObject1.getString("name");
                                        pcm.setCityName(city_name);
                                        citi_list.add(city_name);
                                        homecityModels.add(pcm);
                                    }
                                    //   cityDialog();
                                    citySelectionSession.createCity(homecityModels.get(0).getCityId(), homecityModels.get(0).getCityName());
                                    loadCityData();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getActivity(), "Error:" + error, Toast.LENGTH_LONG).show();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {
                        //TODO
                    } else if (error instanceof ServerError) {
                        //TODO
                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {
                        //TODO
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(string_request);
        } else {
            Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadCityData() {
        if (!citySelectionSession.checkCityCreated()) {
            HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
            disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
            disp_city_name = cityDetails.get(CitySelectionSession.CITY_NAME);

            getMainCategory(defaultPageNo);

        } else {
            getCityData();
        }
    }

    private void getMainCategory(final int default_page_number) {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
//            Log.d("MAINCATEGURL", AppUrls.BASE_URL + AppUrls.GET_MAIN_CATEGORY);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_MAIN_CATEGORY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
//                                Log.d("MAINCATEGRESP", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    JSONObject json = jsonObject.getJSONObject("data");
                                    int jsonArray_count = json.getInt("recordTotalCnt");
                                    JSONArray jsonArray = json.getJSONArray("recordData");
                                    total_number_of_items = jsonArray_count;
                                    if (jsonArray_count > mainCategoryList.size()) {
                                        loading = true;
                                    } else {
                                        loading = false;
                                    }

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        MainCategoryModel gbm = new MainCategoryModel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        gbm.setCategoryId(jsonObject1.getString("id"));
                                        gbm.setCategoryName(jsonObject1.getString("name"));
                                        mainCategoryList.add(gbm);
                                    }
                                    textTitle.setText(mainCategoryList.get(0).getCategoryName());
                                    getMostSellingProducts(defaultPageNo, "id-desc", "empty", "empty", "empty", "0", "0");

                                }
                                if (responceCode.equals("12786")) {
                                    Toast.makeText(getActivity(), "No Data Found..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("11786")) {
                                    Toast.makeText(getActivity(), "All fields are required..!", Toast.LENGTH_SHORT).show();
                                }
                                if (responceCode.equals("10786")) {
                                    Toast.makeText(getActivity(), "Invalid Token.!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("page_no", String.valueOf(default_page_number));
//                    Log.d("MainCATEREQUESTDATA:", params.toString());
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }


    private void getMostSellingProducts(final int default_page_number, final String sort_by, final String wei_id, final String mi_price, final String ma_price, final String sub_cat_id, final String child_cat_id) {

        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_REFINE_FILTER_PRODUCTS,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
//                                Log.d("ressdjfasdh1233", response);
                                String responceCode = jsonObject.getString("status");
                                if (responceCode.equals("10100")) {
                                    most_selling_recyclerview.setVisibility(View.VISIBLE);

                                    JSONObject json = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = json.getJSONArray("recordData");
                                    int jsonArray_count = json.getInt("recordTotalCnt");

                                    total_number_of_items = jsonArray_count;
//                                    Log.d("LOADSTATUS", "OUTER " + loading + "  " + jsonArray_count);
                                    if (jsonArray_count > mostsellinglist.size()) {
                                        loading = true;
//                                        Log.d("LOADSTATUS", "INNER " + loading + "  " + jsonArray_count);
                                    } else {
                                        loading = false;
                                    }

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        MostSellingModel gbm = new MostSellingModel();

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        gbm.setId(jsonObject1.getString("id"));
                                        gbm.setName(jsonObject1.getString("product_name"));
                                        gbm.setAvailability(jsonObject1.getString("availability"));
                                        gbm.setUrl_name(jsonObject1.getString("url_name"));
                                        gbm.setCount_id(jsonObject1.getString("location_id"));
                                        gbm.setWeight_id(jsonObject1.getString("weight_id"));
                                        gbm.setCount_name(jsonObject1.getString("count_name"));
                                        gbm.setWeight_name(jsonObject1.getString("weight_name"));
                                        gbm.setQty(jsonObject1.getString("qty"));
                                        gbm.setMrp_price(jsonObject1.getString("mrp_price"));
                                        gbm.setImages(AppUrls.PRODUCTS_IMAGE_URL + jsonObject1.getString("images"));
                                        gbm.setFeatures(jsonObject1.getString("features"));
                                        gbm.setStatus(jsonObject1.getString("status"));
                                        gbm.setUser_rating(jsonObject1.getString("user_rating"));

                                        mostsellinglist.add(gbm);

                                    }

                                    most_selling_recyclerview.setAdapter(mostsellingadapter);

                                }
                                if (responceCode.equals("12786")) {
                                    Toast.makeText(getActivity(), "No Data Found..!", Toast.LENGTH_SHORT).show();
                                    most_selling_recyclerview.setVisibility(View.GONE);
                                }
                                if (responceCode.equals("11786")) {
                                    //  Toast.makeText(getActivity(), "Error..!", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            }) {


                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("location_id", disp_city_id);
                    params.put("page_no", String.valueOf(default_page_number));
                    params.put("main_category_id", mainCategoryList.get(0).getCategoryId());
                    params.put("sub_category_id", sub_cat_id);
                    params.put("child_category_id", child_cat_id);
                    params.put("sort_by", sort_by);
                    params.put("weight_id", wei_id);
                    params.put("min_price", mi_price);
                    params.put("max_price", ma_price);
//                    Log.d("LoginREQUESTDATA:", params.toString());
                    return params;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        } else {
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }

    }
}