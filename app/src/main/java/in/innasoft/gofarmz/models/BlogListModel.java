package in.innasoft.gofarmz.models;

/**
 * Created by Devolper on 24-Jul-17.
 */

public class BlogListModel
{
    String id;
    String name;
    String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
