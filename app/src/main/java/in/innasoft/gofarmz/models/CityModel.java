package in.innasoft.gofarmz.models;

/**
 * Created by Devolper on 24-Jul-17.
 */

public class CityModel
{
    String cityId;
    String cityName;
    String latitute;
    String lognigtute;
    String city_image;

    public String getCity_image() {
        return city_image;
    }

    public void setCity_image(String city_image) {
        this.city_image = city_image;
    }

    public String getLatitute() {
        return latitute;
    }

    public void setLatitute(String latitute) {
        this.latitute = latitute;
    }

    public String getLognigtute() {
        return lognigtute;
    }

    public void setLognigtute(String lognigtute) {
        this.lognigtute = lognigtute;
    }



    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
