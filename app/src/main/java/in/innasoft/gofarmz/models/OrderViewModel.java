package in.innasoft.gofarmz.models;

public class OrderViewModel {
    public String currency;
    public String recordTotalCnt;
    public String id;
    public String product_sku_id;
    public String product_id;
    public String product_name;
    public String weight_name;
    public String count_name;
    public String images;
    public String product_mrp_price;
    public String purchase_quantity;
    public String total_price;
    public String grand_total;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRecordTotalCnt() {
        return recordTotalCnt;
    }

    public void setRecordTotalCnt(String recordTotalCnt) {
        this.recordTotalCnt = recordTotalCnt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_sku_id() {
        return product_sku_id;
    }

    public void setProduct_sku_id(String product_sku_id) {
        this.product_sku_id = product_sku_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getWeight_name() {
        return weight_name;
    }

    public void setWeight_name(String weight_name) {
        this.weight_name = weight_name;
    }

    public String getCount_name() {
        return count_name;
    }

    public void setCount_name(String count_name) {
        this.count_name = count_name;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getProduct_mrp_price() {
        return product_mrp_price;
    }

    public void setProduct_mrp_price(String product_mrp_price) {
        this.product_mrp_price = product_mrp_price;
    }

    public String getPurchase_quantity() {
        return purchase_quantity;
    }

    public void setPurchase_quantity(String purchase_quantity) {
        this.purchase_quantity = purchase_quantity;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(String grand_total) {
        this.grand_total = grand_total;
    }
}
