package in.innasoft.gofarmz.models;

/**
 * Created by Devolper on 24-Jul-17.
 */

public class WishListModel
{
    String id;
    String product_id;
    String product_name;
    String product_mrp_prize;
    String product_weight_name;

    public String getProduct_mrp_prize() {
        return product_mrp_prize;
    }

    public void setProduct_mrp_prize(String product_mrp_prize) {
        this.product_mrp_prize = product_mrp_prize;
    }

    public String getProduct_weight_name() {
        return product_weight_name;
    }

    public void setProduct_weight_name(String product_weight_name) {
        this.product_weight_name = product_weight_name;
    }

    String images;
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
