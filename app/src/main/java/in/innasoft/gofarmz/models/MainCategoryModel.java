package in.innasoft.gofarmz.models;

/**
 * Created by Devolper on 24-Jul-17.
 */

public class MainCategoryModel
{

    String categoryId;
    String categoryName;

    public String getCategoryImages() {
        return categoryImages;
    }

    public void setCategoryImages(String categoryImages) {
        this.categoryImages = categoryImages;
    }

    String categoryImages;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }






}
