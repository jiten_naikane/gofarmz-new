package in.innasoft.gofarmz;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.activities.AboutUsActivity;
import in.innasoft.gofarmz.activities.BlogsListActivity;
import in.innasoft.gofarmz.activities.CartProductListActivity;
import in.innasoft.gofarmz.activities.ChangePasswordActivity;
import in.innasoft.gofarmz.activities.LocationDialogActivity;
import in.innasoft.gofarmz.activities.LoginActivity;
import in.innasoft.gofarmz.activities.OrdersListActivity;
import in.innasoft.gofarmz.activities.TestimonialActivity;
import in.innasoft.gofarmz.activities.UserProfileActivity;
import in.innasoft.gofarmz.activities.WishListActivity;
import in.innasoft.gofarmz.models.CityModel;
import in.innasoft.gofarmz.utilities.AppUrls;
import in.innasoft.gofarmz.utilities.CitySelectionSession;
import in.innasoft.gofarmz.utilities.CustomTypefaceSpan;
import in.innasoft.gofarmz.utilities.NetworkChecking;
import in.innasoft.gofarmz.utilities.UserSessionManager;


public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;

    CitySelectionSession citySelectionSession;
    private DrawerLayout drawer;
    NavigationView navigationView;
    Typeface typeface;
    TextView toolbar_title, tvSnackBar, count_tv, location;
    private Boolean exit = false;
    UserSessionManager session;
    private boolean checkInternet;
    ProgressDialog progressDialog;
    public String jwt_key, user_id, user_name, user_email, user_phone, device_id;
    ImageView cart_iv;
    LinearLayout location_layout;
    public String disp_city_name = "", disp_city_id = "";
    String cart_item_count = "";
    // CartBucketDBHelper cartBucketDBHelper;
    ArrayList<String> citi_list = new ArrayList<String>();
    ArrayList<CityModel> homecityModels = new ArrayList<CityModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        Log.d("android_id",android_id);

        typeface = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.fonttype_one));

        //  cartBucketDBHelper = new CartBucketDBHelper(MainActivity.this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......");
        progressDialog.setProgressStyle(R.style.DialogTheme);

        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        user_email = userDetails.get(UserSessionManager.USER_EMAIL);
        user_phone = userDetails.get(UserSessionManager.USER_MOBILE);
        jwt_key = userDetails.get(UserSessionManager.KEY_ACCSES);

        if (jwt_key == null) {
            user_id = "0";
            cartCountData();
        } else {
            user_id = userDetails.get(UserSessionManager.USER_ID);
            cartCountData();
        }


        citySelectionSession = new CitySelectionSession(getApplicationContext());

        Log.d("USERDETAILS", jwt_key + "//" + user_id + ", " + user_name + ", " + user_email + ", " + user_phone);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        setSupportActionBar(toolbar);

        if (jwt_key == null) {
            navigationView.getMenu().findItem(R.id.profile).setVisible(false);
            navigationView.getMenu().findItem(R.id.my_orders).setVisible(false);
            navigationView.getMenu().findItem(R.id.wish_list).setVisible(false);
            navigationView.getMenu().findItem(R.id.my_cart).setVisible(true);
            navigationView.getMenu().findItem(R.id.change_password).setVisible(false);
            navigationView.getMenu().findItem(R.id.logout).setVisible(false);
        } else {
            navigationView.getMenu().findItem(R.id.login).setVisible(false);
        }

        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(typeface);

        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("DEVICEID:", device_id);


        location_layout = (LinearLayout) findViewById(R.id.location_layout);
        count_tv = (TextView) findViewById(R.id.count_tv);

        // count_tv.setTypeface(typeface);

        /*String countValues = String.valueOf(cartBucketDBHelper.findNumberOfProducts());

        if (countValues.equals("null")|| countValues.equals("") || countValues.equals("0")){
            count_tv.setVisibility(View.GONE);
        }else {
            count_tv.setVisibility(View.VISIBLE);
            count_tv.setText(countValues);
        }*/

        location = (TextView) findViewById(R.id.location);
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, LocationDialogActivity.class);
                startActivity(intent);
            }
        });
        location_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, LocationDialogActivity.class);
                startActivity(intent);
            }
        });


        cart_iv = (ImageView) findViewById(R.id.cart_iv);
        cart_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Facebook Compaign*/
                AppEventsLogger logger = AppEventsLogger.newLogger(MainActivity.this);
                logger.logEvent("Click on Cart from home page");

                if (!cart_item_count.equals("0")) {
                    Intent intent = new Intent(MainActivity.this, CartProductListActivity.class);
                    intent.putExtra("activity_name", "productDescription");
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Cart is Empty", Toast.LENGTH_SHORT).show();
                }

                //   String countValues = String.valueOf(cartBucketDBHelper.findNumberOfProducts());
                //   count_tv.setText(countValues);


            }
        });

        tvSnackBar = (TextView) findViewById(R.id.tvSnackBar);
        tvSnackBar.setTypeface(typeface);

        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);

       /* NavigationMenuView navMenuView = (NavigationMenuView) navigationView.getChildAt(0);
        navMenuView.addItemDecoration(new DividerItemDecoration(MainActivity.this,DividerItemDecoration.HORIZONTAL));*/

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            applyFontToMenuItem(mi);
        }


       loadCityData();

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                drawer.closeDrawers();
                if (menuItem.getItemId() == R.id.home) {

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                }
                if (menuItem.getItemId() == R.id.my_orders) {

                    Intent intent = new Intent(getApplicationContext(), OrdersListActivity.class);
                    startActivity(intent);
                }
                if (menuItem.getItemId() == R.id.login) {

                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.putExtra("activity_name", "splash");
                    startActivity(intent);
                }


                if (menuItem.getItemId() == R.id.profile) {
                    Intent intent = new Intent(getApplicationContext(), UserProfileActivity.class);
                    startActivity(intent);
                }

                if (menuItem.getItemId() == R.id.change_password) {

                    Intent intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                    intent.putExtra("activity_name", "splash");
                    startActivity(intent);
                }

                if (menuItem.getItemId() == R.id.blogs) {

                    Intent intent = new Intent(getApplicationContext(), BlogsListActivity.class);
                    startActivity(intent);
                }


                if (menuItem.getItemId() == R.id.my_cart) {

                    if (!cart_item_count.equals("0")) {
                        Intent intent = new Intent(MainActivity.this, CartProductListActivity.class);
                        intent.putExtra("activity_name", "productDescription");
                        startActivity(intent);
                    } else {
                        Toast.makeText(MainActivity.this, "Cart is Empty", Toast.LENGTH_SHORT).show();
                    }
                }

                if (menuItem.getItemId() == R.id.about_us) {

                    Intent intent = new Intent(getApplicationContext(), AboutUsActivity.class);
                    startActivity(intent);
                }
                if (menuItem.getItemId() == R.id.testimonials) {

                    Intent intent = new Intent(getApplicationContext(), TestimonialActivity.class);
                    startActivity(intent);
                }

                if (menuItem.getItemId() == R.id.wish_list) {

                    Intent intent = new Intent(getApplicationContext(), WishListActivity.class);
                    startActivity(intent);
                }


                if (menuItem.getItemId() == R.id.share) {

                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "cfoodz");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + getPackageName());
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }

                if (menuItem.getItemId() == R.id.logout) {
                    checkInternet = NetworkChecking.isConnected(MainActivity.this);
                    if (checkInternet) {
                        progressDialog.show();
                        if (session.isUserLoggedIn()) {

                            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGOUT,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            Log.d("LOGOUTCFOODZ:", response);
                                            session.logoutUser();
                                            citySelectionSession.clearCity();
                                            //  cartBucketDBHelper.emptyCartBucket();
                                            Toast.makeText(getApplicationContext(), "You Logged Out Successfully...!", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(intent);
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {

                                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                                            } else if (error instanceof AuthFailureError) {
                                            } else if (error instanceof ServerError) {
                                            } else if (error instanceof NetworkError) {
                                            } else if (error instanceof ParseError) {
                                            }
                                        }
                                    }) {
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {

                                    Map<String, String> params = new HashMap<String, String>();

                                    params.put("user_id", user_id);
                                    params.put("browser_id", device_id);
                                    Log.d("PARAM", params.toString());
                                    return params;
                                }

                                @Override
                                public Map<String, String> getHeaders() throws AuthFailureError {
                                    Map<String, String> headers = new HashMap<>();

                                    // headers.put("Content-Type", "application/json");
                                    headers.put("Authorization-Basic", jwt_key);
                                    Log.d("LOGUTHEADER", headers.toString());
                                    return headers;
                                }
                            };
                            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
                            requestQueue.add(stringRequest);
                        }

                    } else {
                        Snackbar snack = Snackbar.make(tvSnackBar, "No Internet Connection..!", Snackbar.LENGTH_SHORT);
                        View view = snack.getView();
                        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                        tv.setTextColor(Color.WHITE);
                        snack.show();
                    }
                }
                return false;
            }
        });

        View navigationheadderView = navigationView.getHeaderView(0);

       /* txtViewName = (TextView) navigationheadderView.findViewById(R.id.txtViewName);
        txtViewName.setTypeface(typeface);
        txtViewName.setText(user_name);
        txtViewEmail = (TextView) navigationheadderView.findViewById(R.id.txtViewEmail);
        txtViewEmail.setTypeface(typeface);
        txtViewEmail.setText(user_email);
        txtViewPhone = (TextView) navigationheadderView.findViewById(R.id.txtViewPhone);
        txtViewPhone.setTypeface(typeface);
        txtViewPhone.setText(user_phone);
        profile_pic_iv = (ImageView) navigationheadderView.findViewById(R.id.profile_pic_iv);

        Picasso.with(MainActivity.this)
                .load(profile_pic_url)
                .placeholder(R.drawable.logo)
                .fit()
                .into(profile_pic_iv);*/


        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mMessageReceiver, new IntentFilter("custom-count"));

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        /*Facebook Compaign*/
        AppEventsLogger logger = AppEventsLogger.newLogger(MainActivity.this);
        logger.logEvent("Home Page");

    }


    private void getCityData() {

        checkInternet = NetworkChecking.isConnected(MainActivity.this);
        if (checkInternet) {

            homecityModels.clear();
            progressDialog.show();

            StringRequest string_request = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOCATION,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            progressDialog.dismiss();
                            String repo = response.toString();
                            Log.e("RESPCITY", "RESPCITY:" + repo);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("Citites:", response);

                                String responceCode = jsonObject.getString("status");

                                if (responceCode.equals("10100")) {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        CityModel pcm = new CityModel();
                                        pcm.setCityId(jsonObject1.getString("id"));
                                        String city_name = jsonObject1.getString("name");
                                        pcm.setCityName(city_name);
                                        citi_list.add(city_name);
                                        homecityModels.add(pcm);
                                    }

                                    //   cityDialog();

                                    citySelectionSession.createCity(homecityModels.get(0).getCityId(), homecityModels.get(0).getCityName());

                                    loadCityData();


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressDialog.cancel();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Error:" + error, Toast.LENGTH_LONG).show();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                    } else if (error instanceof AuthFailureError) {
                        //TODO
                    } else if (error instanceof ServerError) {
                        //TODO
                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {
                        //TODO
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(string_request);
        } else {
            Toast.makeText(MainActivity.this, "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadCityData() {
        if (citySelectionSession.checkCityCreated() == false) {
            HashMap<String, String> cityDetails = citySelectionSession.getCityDetails();
            disp_city_id = cityDetails.get(CitySelectionSession.CITY_ID);
            disp_city_name = cityDetails.get(CitySelectionSession.CITY_NAME);
            //   disp_lng = cityDetails.get(CitySelectionSession.CITY_LAT);
            //   disp_lng = cityDetails.get(CitySelectionSession.CITY_LNG);
            try {
                Log.d("FIIINDCITYLENGTH", disp_city_name);
                location.setText(disp_city_name);
                location.setTypeface(typeface);

            } catch (NullPointerException e) {
                String msg = (e.getMessage() == null) ? "Login failed!" : e.getMessage();
                Log.i("Login Error1", msg);
            }
        } else {

            getCityData();

//            Intent intent = new Intent(MainActivity.this, LocationDialogActivity.class);
//            startActivity(intent);
        }
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            cartCountData();
        }
    };

    private void hideItem() {
        /*Menu nav_Menu = navigationView.getMenu();

        nav_Menu.findItem(R.id.my_bookings).setVisible(false);
        nav_Menu.findItem(R.id.profile).setVisible(false);
        nav_Menu.findItem(R.id.wish_list).setVisible(false);
        nav_Menu.findItem(R.id.change_password).setVisible(false);
        nav_Menu.findItem(R.id.logout).setVisible(false);*/


    }

    private void applyFontToMenuItem(MenuItem mi) {

        Typeface font = Typeface.createFromAsset(getAssets(), "museosanscyrl.ttf");

        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (exit) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                moveTaskToBack(true);
            } else {
                Toast.makeText(getApplicationContext(), "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2 * 1000);
            }
        }
    }

    /* @Override
     public void onClick(View view)
     {
         if(view==location)
         {
             Toast.makeText(getApplicationContext(), "CLICK", Toast.LENGTH_SHORT).show();
             Intent intent = new Intent(MainActivity.this,LocationDialogActivity.class);
                      startActivity(intent);

         }

     }*/
    @Override
    public void onResume() {
        super.onResume();
        session = new UserSessionManager(getApplicationContext());
        HashMap<String, String> userDetails = session.getUserDetails();
        user_id = userDetails.get(UserSessionManager.USER_ID);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        user_email = userDetails.get(UserSessionManager.USER_EMAIL);
        user_phone = userDetails.get(UserSessionManager.USER_MOBILE);
        jwt_key = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("CHANGNNN", user_id + "//" + jwt_key);
        if (jwt_key == null) {
            navigationView.getMenu().findItem(R.id.login).setVisible(true);
            navigationView.getMenu().findItem(R.id.profile).setVisible(false);
            navigationView.getMenu().findItem(R.id.my_orders).setVisible(false);
            navigationView.getMenu().findItem(R.id.wish_list).setVisible(false);
            navigationView.getMenu().findItem(R.id.my_cart).setVisible(true);
            navigationView.getMenu().findItem(R.id.change_password).setVisible(false);
            navigationView.getMenu().findItem(R.id.logout).setVisible(false);
        } else {
            navigationView.getMenu().findItem(R.id.profile).setVisible(true);
            navigationView.getMenu().findItem(R.id.my_orders).setVisible(true);
            navigationView.getMenu().findItem(R.id.wish_list).setVisible(true);
            navigationView.getMenu().findItem(R.id.my_cart).setVisible(true);
            navigationView.getMenu().findItem(R.id.change_password).setVisible(true);
            navigationView.getMenu().findItem(R.id.logout).setVisible(true);
            navigationView.getMenu().findItem(R.id.login).setVisible(false);
        }
        if (jwt_key == null) {
            user_id = "0";
            cartCountData();
        } else {
            user_id = userDetails.get(UserSessionManager.USER_ID);
            cartCountData();
        }
    }

    private void cartCountData() {
        Log.d("RESSSSSCOUNTTTURL:", AppUrls.BASE_URL + AppUrls.GET_CART_COUNT);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.GET_CART_COUNT,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        Log.d("RESSSSSCOUNTTT:", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String status = jsonObject.getString("status");
                            if (status.equals("10100")) {

                                JSONObject jobdata = jsonObject.getJSONObject("data");
                                int coumnt = jobdata.getInt("recordDataCount");
                                if (coumnt == 0) {
                                    cart_item_count = String.valueOf(coumnt);
                                    count_tv.setVisibility(View.GONE);
                                } else {
                                    count_tv.setVisibility(View.VISIBLE);
                                    count_tv.setText("" + coumnt);

                                    count_tv.setTypeface(typeface);


                                    cart_item_count = String.valueOf(coumnt);


                                }


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {


                        } else if (error instanceof AuthFailureError) {
                            //TODO
                        } else if (error instanceof ServerError) {
                            //TODO
                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {
                            //TODO
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put("browserId", device_id);

                Log.d("RESSSSSCOUNTTTPARAM:", params.toString());
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);
    }

}
