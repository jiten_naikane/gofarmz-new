package in.innasoft.gofarmz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemclicklistners.OrderListItemClickListener;

public class OrderListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView order_id_txt, ref_id_txt, price_txt, time_txt, order_status;
    public ImageView order_img;
    //    public Button view_btn,cancel_btn;
//    public View view_border;
    OrderListItemClickListener orderListItemClickListener;

    public OrderListHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        order_id_txt = (TextView) itemView.findViewById(R.id.order_id_txt);
        ref_id_txt = (TextView) itemView.findViewById(R.id.ref_id_txt);
        price_txt = (TextView) itemView.findViewById(R.id.price_txt);
        time_txt = (TextView) itemView.findViewById(R.id.time_txt);
        order_status = (TextView) itemView.findViewById(R.id.status_txt);
        order_img = (ImageView) itemView.findViewById(R.id.order_img);
//        view_btn = (Button) itemView.findViewById(R.id.view_btn);
//        cancel_btn = (Button) itemView.findViewById(R.id.cancel_btn);
//        view_border = (View) itemView.findViewById(R.id.view_border);

//        time_text_title = (TextView) itemView.findViewById(R.id.time_text_title);
//        reference_id_text_title = (TextView) itemView.findViewById(R.id.reference_id_text_title);
//        order_id_text_title = (TextView) itemView.findViewById(R.id.order_id_text_title);
    }

    @Override
    public void onClick(View view) {
        this.orderListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(OrderListItemClickListener ic) {
        this.orderListItemClickListener = ic;
    }
}
