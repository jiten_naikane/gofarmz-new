package in.innasoft.gofarmz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemclicklistners.AreaItemClickListener;

public class AreaHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView area_name_txt;
    AreaItemClickListener areaItemClickListener;

    public AreaHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        area_name_txt = (TextView) itemView.findViewById(R.id.area_name_txt);
    }

    @Override
    public void onClick(View view) {
        this.areaItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AreaItemClickListener ic)
    {
        this.areaItemClickListener =ic;
    }
}
