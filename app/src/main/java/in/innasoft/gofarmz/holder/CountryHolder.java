package in.innasoft.gofarmz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemclicklistners.CountryItemClickListener;

public class CountryHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView country_name_txt;
    CountryItemClickListener countryItemClickListener;

    public CountryHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        country_name_txt = (TextView) itemView.findViewById(R.id.country_name_txt);
    }

    @Override
    public void onClick(View view) {
        this.countryItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CountryItemClickListener ic)
    {
        this.countryItemClickListener =ic;
    }
}
