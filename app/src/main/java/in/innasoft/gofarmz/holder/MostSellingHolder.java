package in.innasoft.gofarmz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemclicklistners.MostSellingItemClickListener;

public class MostSellingHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView name,rating, textAvailable;
    public TextView price,count_no;
    public ImageView image;
    public ImageView new_image,like_image;
    public Button buttonBuy;
    public RelativeLayout product_rl;


    MostSellingItemClickListener mostSellingItemClickListener;

    public MostSellingHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        name = (TextView) itemView.findViewById(R.id.name);
        rating = (TextView) itemView.findViewById(R.id.rating);
        price = (TextView) itemView.findViewById(R.id.price);
        textAvailable = (TextView) itemView.findViewById(R.id.textAvailable);
        count_no = (TextView) itemView.findViewById(R.id.count_no);
        image = (ImageView) itemView.findViewById(R.id.image);
        new_image = (ImageView) itemView.findViewById(R.id.new_image);
        like_image = (ImageView) itemView.findViewById(R.id.like_image);
        buttonBuy = (Button) itemView.findViewById(R.id.buttonBuy);
        product_rl = (RelativeLayout) itemView.findViewById(R.id.product_rl);
    }

    @Override
    public void onClick(View view) {

        this.mostSellingItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MostSellingItemClickListener ic)
    {
        this.mostSellingItemClickListener=ic;
    }
}
