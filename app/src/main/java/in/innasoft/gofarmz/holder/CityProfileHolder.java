package in.innasoft.gofarmz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemclicklistners.CityProfileItemClickListener;

public class CityProfileHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView city_name_txt;
    CityProfileItemClickListener cityProfileItemClickListener;

    public CityProfileHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        city_name_txt = (TextView) itemView.findViewById(R.id.city_name_txt);
    }

    @Override
    public void onClick(View view) {
        this.cityProfileItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CityProfileItemClickListener ic)
    {
        this.cityProfileItemClickListener =ic;
    }
}
