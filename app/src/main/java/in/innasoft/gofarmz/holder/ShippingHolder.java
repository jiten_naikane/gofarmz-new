package in.innasoft.gofarmz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemclicklistners.ShippingItemClickListener;

public class ShippingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView product_name_txt, quantity_txt, price_txt, ship_product_skuid, ship_product_count, discount_txt;
    public ImageView ship_product_img;
    ShippingItemClickListener shippingItemClickListener;

    public ShippingHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        product_name_txt = (TextView) itemView.findViewById(R.id.product_name_txt);
        ship_product_skuid = (TextView) itemView.findViewById(R.id.ship_product_skuid);
        ship_product_count = (TextView) itemView.findViewById(R.id.ship_product_count);
        quantity_txt = (TextView) itemView.findViewById(R.id.quantity_txt);
        price_txt = (TextView) itemView.findViewById(R.id.price_txt);
        discount_txt = (TextView) itemView.findViewById(R.id.discount_txt);
        ship_product_img = (ImageView) itemView.findViewById(R.id.ship_product_img);

    }

    @Override
    public void onClick(View view) {
        this.shippingItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ShippingItemClickListener ic) {
        this.shippingItemClickListener = ic;
    }
}
