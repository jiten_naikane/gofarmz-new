package in.innasoft.gofarmz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemclicklistners.WishListItemClickListener;

public class WishListHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView wishlist_title_text,wishlist_prize_text,wishlist_weight_text;
    public ImageView wishlsit_image_iv;
    public Button wishlsit_remove_btn;


    WishListItemClickListener wishListItemClickListener;

    public WishListHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        wishlist_title_text = (TextView) itemView.findViewById(R.id.wishlist_title_text);
        wishlist_weight_text = (TextView) itemView.findViewById(R.id.wishlist_weight_text);
        wishlist_prize_text = (TextView) itemView.findViewById(R.id.wishlist_prize_text);
        wishlsit_image_iv = (ImageView) itemView.findViewById(R.id.wishlsit_image_iv);
        wishlsit_remove_btn = (Button) itemView.findViewById(R.id.wishlsit_remove_btn);
    }

    @Override
    public void onClick(View view) {

        this.wishListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(WishListItemClickListener ic)
    {
        this.wishListItemClickListener=ic;
    }
}
