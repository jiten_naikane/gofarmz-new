package in.innasoft.gofarmz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemclicklistners.StateItemClickListener;


public class StateHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView state_name_txt;
    StateItemClickListener stateItemClickListener;

    public StateHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        state_name_txt = (TextView) itemView.findViewById(R.id.state_name_txt);
    }

    @Override
    public void onClick(View view) {
        this.stateItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(StateItemClickListener ic)
    {
        this.stateItemClickListener =ic;
    }
}
