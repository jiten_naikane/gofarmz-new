package in.innasoft.gofarmz.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemclicklistners.HomeCityItemClickListener;


public class HomeCityHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{

    public TextView row_home_city_txt;
    public ImageView city_row_img;


    HomeCityItemClickListener homeCityItemClickListener;

    public HomeCityHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        row_home_city_txt = (TextView) itemView.findViewById(R.id.row_home_city_txt);
        city_row_img = (ImageView) itemView.findViewById(R.id.city_row_img);

    }

    @Override
    public void onClick(View view)
    {

        this.homeCityItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(HomeCityItemClickListener ic)
    {
        this.homeCityItemClickListener=ic;
    }
}
