package in.innasoft.gofarmz.itemclicklistners;

import android.view.View;

public interface BlogListItemClickListener
{
    void onItemClick(View v, int pos);
}
