package in.innasoft.gofarmz.itemclicklistners;

import android.view.View;

public interface StateItemClickListener
{
    void onItemClick(View v, int pos);
}
