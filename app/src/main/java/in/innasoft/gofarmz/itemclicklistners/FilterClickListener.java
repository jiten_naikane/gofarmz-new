package in.innasoft.gofarmz.itemclicklistners;

import android.view.View;

public interface FilterClickListener
{
    void onItemClick(View v, int pos);
}
