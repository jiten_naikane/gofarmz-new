package in.innasoft.gofarmz.itemclicklistners;

import android.view.View;

public interface OrderViewItemClickListener
{
    void onItemClick(View v, int pos);
}
