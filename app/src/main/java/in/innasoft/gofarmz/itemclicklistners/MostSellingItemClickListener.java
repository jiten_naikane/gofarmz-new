package in.innasoft.gofarmz.itemclicklistners;

import android.view.View;


public interface MostSellingItemClickListener
{
    void onItemClick(View v, int pos);
}

