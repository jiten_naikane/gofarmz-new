package in.innasoft.gofarmz.itemclicklistners;

import android.view.View;

public interface AreaItemClickListener
{
    void onItemClick(View v, int pos);
}
