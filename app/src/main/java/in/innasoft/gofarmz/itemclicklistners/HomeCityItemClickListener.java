package in.innasoft.gofarmz.itemclicklistners;

import android.view.View;

public interface HomeCityItemClickListener
{
   void onItemClick(View v, int pos);
}
