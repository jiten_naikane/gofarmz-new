package in.innasoft.gofarmz.itemclicklistners;

import android.view.View;

public interface TestimonialItemClickListener
{
    void onItemClick(View v, int pos);
}
